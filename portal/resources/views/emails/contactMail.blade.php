@component('mail::message')

    <strong>Good days<br> A client with details below has contacted you. </strong><br><br>
    <strong>Client Details:</strong><br>
    <strong>Name: </strong> <span> {{$data['name']}}</span><br>
    <strong>Phone Number: </strong><span> {{ $data['phoneNumber'] }}</span><br>
    <strong>Email: </strong><span> {{ $data['email'] }}</span><br>
    <strong>Message: </strong><span> {{ $data['message'] }}</span><br>

@endcomponent
