@component('mail::message')

    <p> Thank you for submitting your application. We are reviewing it, we will get back at you soon. Login to track your application.</p>

<diV>
     @component('mail::button', ['url' => 'http://localhost/atlantic_portal_latest/atlanticfuneralpolicyportal/portal/login'])
       Login
    @endcomponent
</diV>


@endcomponent
