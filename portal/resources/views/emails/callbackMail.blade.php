@component('mail::message')

    <strong>Good days<br> A client with details below is kindly requesting for a call me back. </strong><br>
    <strong>Client Details:</strong><br>
    <strong>Name: </strong> <span> {{$data['name']}}</span><br>
    <strong>Phone Number: </strong><span> {{ $data['phoneNumber'] }}</span>

@endcomponent
