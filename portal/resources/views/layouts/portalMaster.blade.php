<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Atlantic Portal | Client Portal</title>
    <link rel="icon" type="image/png" href="{{asset('public/frontend/img/atlantic_logo.png')}}">
    <link rel="stylesheet" href="{{asset('public/portal/assets/plugins/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/portal/assets/plugins/fontawesome/css/fontawesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/portal/assets/plugins/fontawesome/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/portal/assets/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/portal/assets/css/admin.css')}}">


    {{--    //for toasters--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-
     alpha/css/bootstrap.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

{{--    <style>--}}

{{--        html {--}}
{{--            scroll-behavior: smooth;--}}
{{--        }--}}

{{--        .modal-login {--}}
{{--            color: #636363;--}}
{{--            width: 650px;--}}
{{--        }--}}
{{--        .modal-login .modal-content {--}}
{{--            padding: 20px;--}}
{{--            border-radius: 5px;--}}
{{--            border: none;--}}
{{--        }--}}
{{--        .modal-login .modal-header {--}}
{{--            border-bottom: none;--}}
{{--            position: relative;--}}
{{--            justify-content: center;--}}
{{--        }--}}
{{--        .modal-login h4 {--}}
{{--            text-align: center;--}}
{{--            font-size: 26px;--}}
{{--            margin: 30px 0 -15px;--}}
{{--        }--}}
{{--        .modal-login .form-control:focus {--}}
{{--            border-color: #70c5c0;--}}
{{--        }--}}
{{--        .modal-login .form-control, .modal-login .btn {--}}
{{--            min-height: 40px;--}}
{{--            border-radius: 3px;--}}
{{--        }--}}
{{--        .modal-login .close {--}}
{{--            position: absolute;--}}
{{--            top: -5px;--}}
{{--            right: -5px;--}}
{{--        }--}}
{{--        .modal-login .modal-footer {--}}
{{--            background: #ecf0f1;--}}
{{--            border-color: #dee4e7;--}}
{{--            text-align: center;--}}
{{--            justify-content: center;--}}
{{--            margin: 0 -20px -20px;--}}
{{--            border-radius: 5px;--}}
{{--            font-size: 13px;--}}
{{--        }--}}
{{--        .modal-login .modal-footer a {--}}
{{--            color: #999;--}}
{{--        }--}}
{{--        .modal-login .avatar {--}}
{{--            position: absolute;--}}
{{--            margin: 0 auto;--}}
{{--            left: 0;--}}
{{--            right: 0;--}}
{{--            top: -70px;--}}
{{--            width: 95px;--}}
{{--            height: 95px;--}}
{{--            border-radius: 50%;--}}
{{--            z-index: 9;--}}
{{--            background: #60c7c1;--}}
{{--            padding: 15px;--}}
{{--            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);--}}
{{--        }--}}
{{--        .modal-login .avatar img {--}}
{{--            width: 100%;--}}
{{--        }--}}
{{--        .modal-login.modal-dialog {--}}
{{--            margin-top: 80px;--}}
{{--        }--}}
{{--        .modal-login .btn, .modal-login .btn:active {--}}
{{--            color: #fff;--}}
{{--            border-radius: 4px;--}}
{{--            background: #60c7c1 !important;--}}
{{--            text-decoration: none;--}}
{{--            transition: all 0.4s;--}}
{{--            line-height: normal;--}}
{{--            border: none;--}}
{{--        }--}}
{{--        .modal-login .btn:hover, .modal-login .btn:focus {--}}
{{--            background: #45aba6 !important;--}}
{{--            outline: none;--}}
{{--        }--}}
{{--        .trigger-btn {--}}
{{--            display: inline-block;--}}
{{--            margin: 100px auto;--}}
{{--        }--}}

{{--        #regForm {--}}
{{--            background-color: #ffffff;--}}
{{--            margin: 100px auto;--}}
{{--            font-family: Raleway;--}}
{{--            padding: 40px;--}}
{{--            width: 70%;--}}
{{--            min-width: 300px;--}}
{{--        }--}}
{{--        h1 {--}}
{{--            text-align: center;--}}
{{--        }--}}
{{--        input {--}}
{{--            padding: 10px;--}}
{{--            width: 100%;--}}
{{--            font-size: 17px;--}}
{{--            font-family: Raleway;--}}
{{--            border: 1px solid #aaaaaa;--}}
{{--        }--}}
{{--        /* Mark input boxes that gets an error on validation: */--}}
{{--        input.invalid {--}}
{{--            background-color: #ffdddd;--}}
{{--        }--}}
{{--        /* Hide all steps by default: */--}}
{{--        .tab {--}}
{{--            display: none;--}}
{{--        }--}}
{{--        button {--}}
{{--            background-color: #04AA6D;--}}
{{--            color: #ffffff;--}}
{{--            border: none;--}}
{{--            padding: 10px 20px;--}}
{{--            font-size: 17px;--}}
{{--            font-family: Raleway;--}}
{{--            cursor: pointer;--}}
{{--        }--}}
{{--        button:hover {--}}
{{--            opacity: 0.8;--}}
{{--        }--}}
{{--        #prevBtn {--}}
{{--            background-color: #bbbbbb;--}}
{{--        }--}}
{{--        /* Make circles that indicate the steps of the form: */--}}
{{--        .step {--}}
{{--            height: 15px;--}}
{{--            width: 15px;--}}
{{--            margin: 0 2px;--}}
{{--            background-color: #bbbbbb;--}}
{{--            border: none;--}}
{{--            border-radius: 50%;--}}
{{--            display: inline-block;--}}
{{--            opacity: 0.5;--}}
{{--        }--}}
{{--        .step.active {--}}
{{--            opacity: 1;--}}
{{--        }--}}
{{--        /* Mark the steps that are finished and valid: */--}}
{{--        .step.finish {--}}
{{--            background-color: #04AA6D;--}}
{{--        }--}}
{{--    </style>--}}
</head>
<body onload="hd()">
@include("patials.portalHeader")

<!-- Modal HTML -->
<div id="addVidTuts" class="modal fade">
    <div class="modal-dialog modal-login">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0C0457;color:white">
                <h5 class="modal-title">Upload a video</h5><br>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form class="needs-validation" method="POST" action="{{ url('upload-video') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="form-group col">
                            <input type="file" class="form-control" id="file" name="file" required>
                        </div>
                        <div class="form-group col">
                            <input id="vidTitle" name="vidTitle"  class="form-control" required  placeholder="Title" >
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col">
                            <textarea  id="vidDescript" name="vidDescript" class="form-control" required placeholder="Video Description..."></textarea>
                        </div>
                    </div>

            <div class="modal-footer">
                <div class="book-now-btn">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Save Video</button>
                </div>
            </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="policy_plans" class="modal fade">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <div class="avatar">
                    <img src="{{asset('public/frontend/img/atlantic_logo.png')}}" alt="" class="img-fluid">
                </div>
                <h4 class="modal-title">Upload Policy Plans</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form class="p-3" method="POST" action="{{ url('capturePlans') }}" >
                    @csrf
                    <div class="row">
                        <div class="form-group col">

                            <select class="form-control" name="planType" id="seeAnotherFieldGroup">
                                <option value="Standard Policy">Standard Policy</option>
                                <option value="Societal Policy">Societal Policy</option>
                            </select>
                        </div>
                        <div class="form-group col">
                            <select name="maritalPlanStatus" class="form-control" required>
                                <option value="">Select Marital Status</option>
                                <option value="Single">Single</option>
                                <option value="Married">Married</option>
                            </select>
                        </div>

                    </div>

{{--                    <div class="form-group">--}}
{{--                        <label for="seeAnotherFieldGroup">Do You Want To See Another Group of Fields?</label>--}}
{{--                        <select class="form-control" id="seeAnotherFieldGroup">--}}
{{--                            <option value="Standard Policy">Standard Policy</option>--}}
{{--                            <option value="Societal Policy">Societal Policy</option>--}}
{{--                        </select>--}}
{{--                    </div>--}}

                    <div class="form-group" id="otherFieldGroupDiv">
                        <div class="row">
                            <div class="col">

{{--                                <select id="otherField1" name="planName" class="form-control" required>--}}
{{--                                    <option value="">Select Societal Plan</option>--}}
{{--                                    <option value="Plan A">Plan A</option>--}}
{{--                                    <option value="Plan B">Plan B</option>--}}
{{--                                    <option value="Plan C">Plan C</option>--}}


{{--                                </select>--}}
                                <input type="text" name="planName" class="form-control w-100" id="otherField1" placeholder="Plan Name eg: Plan A">
                            </div>
                            <div class="col">

                                <input type="text" name="groupSizeRange" class="form-control w-100" id="otherField2" placeholder="Group Size eg: 1-5">
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group col">
                            <input  name="ageRange"  class="form-control" required  placeholder="Age range eg: 18-64" >
                        </div>

                        <div class="form-group col">
                            <input  name="coverAmount"  class="form-control" required  placeholder="Cover Amount" >
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col">
                            <input  name="premium"  class="form-control" required  placeholder="Premium" >
                        </div>
                        <div class="form-group col">
                            <input  name="entryFee"  class="form-control" required  placeholder="Entry Fee" >
                        </div>

                    </div>





                    <div class="modal-footer">
                        <div class="book-now-btn">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save Plan</button>
                        </div>
                    </div>
                </form>


            </div>

        </div>
    </div>
</div>

@yield('content')


@include("patials.portalFooter")

    <script src="{{asset('public/portal/assets/js/jquery-3.5.0.min.js')}}"></script>

    <script src="{{asset('public/portal/assets/js/popper.min.js')}}"></script>
    <script src="{{asset('public/portal/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

    <script src="{{asset('public/portal/assets/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

    <script src="{{asset('public/portal/assets/js/admin.js')}}"></script>
    <script>


        $("#seeAnotherFieldGroup").change(function() {
            if ($(this).val() == "Societal Policy") {
                $('#otherFieldGroupDiv').show();
                // $('#otherField1').attr('required', '');
                // $('#otherField1').attr('data-error', 'This field is required.');
                // $('#otherField2').attr('required', '');
                // $('#otherField2').attr('data-error', 'This field is required.');
            } else {
                $('#otherFieldGroupDiv').hide();
                // $('#otherField1').removeAttr('required');
                // $('#otherField1').removeAttr('data-error');
                // $('#otherField2').removeAttr('required');
                // $('#otherField2').removeAttr('data-error');
            }
        });
        $("#seeAnotherFieldGroup").trigger("change");


        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

{{--for toasters--}}
<script>
    @if(Session::has('message'))
        toastr.options =
        {
            "closeButton" : true,
            "progressBar" : true
        }
    toastr.success("{{ session('message') }}");
    @endif

        @if(Session::has('error'))
        toastr.options =
        {
            "closeButton" : true,
            "progressBar" : true
        }
    toastr.error("{{ session('error') }}");
    @endif

        @if(Session::has('info'))
        toastr.options =
        {
            "closeButton" : true,
            "progressBar" : true
        }
    toastr.info("{{ session('info') }}");
    @endif

        @if(Session::has('warning'))
        toastr.options =
        {
            "closeButton" : true,
            "progressBar" : true
        }
    toastr.warning("{{ session('warning') }}");
    @endif
</script>
<script>
    $(document).ready(function() {
        toastr.options.timeOut = 10000;
        @if (Session::has('error'))
        toastr.error('{{ Session::get('error') }}');
        @elseif(Session::has('success'))
        toastr.success('{{ Session::get('success') }}');
        @endif
    });

</script>



</body>


</html>
