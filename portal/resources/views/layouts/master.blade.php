<!doctype html>
<html lang="zxx">
<head>

    <link rel="icon" type="image/png" href="{{asset('public/frontend/img/atlantic_logo.png')}}">

    <link rel="stylesheet" href="{{asset('public/portal/assets/css/admin.css')}}">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="{{asset('public/frontend/css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/animate.min.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/fontawesome.min.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/flaticon.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/owl.carousel.min.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/slick.min.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/meanmenu.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/magnific-popup.min.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/odometer.min.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/nice-select.min.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/style.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/responsive.css')}}">
    <title>Atlantic Funerals - Funeral Policy Company</title>
    <link rel="icon" type="image/png" href="{{asset('public/frontend/img/atlantic_logo.png')}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

    <!-- Import bootstrap cdn -->
    <link rel="stylesheet" href=
    "https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity=
          "sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
          crossorigin="anonymous">

    <!-- Import jquery cdn -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity=
            "sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous">
    </script>

    <script src=
            "https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
            integrity=
            "sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
            crossorigin="anonymous">
    </script>

{{--    //for toasters--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-
     alpha/css/bootstrap.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
{{--    <style>--}}

{{--        html {--}}
{{--            scroll-behavior: smooth;--}}
{{--        }--}}

{{--        .modal-login {--}}
{{--            color: #636363;--}}
{{--            width: 650px;--}}
{{--        }--}}
{{--        .modal-login .modal-content {--}}
{{--            padding: 20px;--}}
{{--            border-radius: 5px;--}}
{{--            border: none;--}}
{{--        }--}}
{{--        .modal-login .modal-header {--}}
{{--            border-bottom: none;--}}
{{--            position: relative;--}}
{{--            justify-content: center;--}}
{{--        }--}}
{{--        .modal-login h4 {--}}
{{--            text-align: center;--}}
{{--            font-size: 26px;--}}
{{--            margin: 30px 0 -15px;--}}
{{--        }--}}
{{--        .modal-login .form-control:focus {--}}
{{--            border-color: #70c5c0;--}}
{{--        }--}}
{{--        .modal-login .form-control, .modal-login .btn {--}}
{{--            min-height: 40px;--}}
{{--            border-radius: 3px;--}}
{{--        }--}}
{{--        .modal-login .close {--}}
{{--            position: absolute;--}}
{{--            top: -5px;--}}
{{--            right: -5px;--}}
{{--        }--}}
{{--        .modal-login .modal-footer {--}}
{{--            background: #ecf0f1;--}}
{{--            border-color: #dee4e7;--}}
{{--            text-align: center;--}}
{{--            justify-content: center;--}}
{{--            margin: 0 -20px -20px;--}}
{{--            border-radius: 5px;--}}
{{--            font-size: 13px;--}}
{{--        }--}}
{{--        .modal-login .modal-footer a {--}}
{{--            color: #999;--}}
{{--        }--}}
{{--        .modal-login .avatar {--}}
{{--            position: absolute;--}}
{{--            margin: 0 auto;--}}
{{--            left: 0;--}}
{{--            right: 0;--}}
{{--            top: -70px;--}}
{{--            width: 95px;--}}
{{--            height: 95px;--}}
{{--            border-radius: 50%;--}}
{{--            z-index: 9;--}}
{{--            background: #60c7c1;--}}
{{--            padding: 15px;--}}
{{--            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);--}}
{{--        }--}}
{{--        .modal-login .avatar img {--}}
{{--            width: 100%;--}}
{{--        }--}}
{{--        .modal-login.modal-dialog {--}}
{{--            margin-top: 80px;--}}

{{--        }--}}
{{--        .modal-login .btn, .modal-login .btn:active {--}}
{{--            color: #fff;--}}
{{--            border-radius: 4px;--}}
{{--            background: #60c7c1 !important;--}}
{{--            text-decoration: none;--}}
{{--            transition: all 0.4s;--}}
{{--            line-height: normal;--}}
{{--            border: none;--}}
{{--        }--}}
{{--        .modal-login .btn:hover, .modal-login .btn:focus {--}}
{{--            background: #45aba6 !important;--}}
{{--            outline: none;--}}
{{--        }--}}
{{--        .trigger-btn {--}}
{{--            display: inline-block;--}}
{{--            margin: 100px auto;--}}
{{--        }--}}

{{--        #regForm {--}}
{{--            background-color: #ffffff;--}}
{{--            margin: 100px auto;--}}
{{--            font-family: Raleway;--}}
{{--            padding: 40px;--}}
{{--            width: 70%;--}}
{{--            min-width: 300px;--}}
{{--        }--}}
{{--        h1 {--}}
{{--            text-align: center;--}}
{{--        }--}}
{{--        input {--}}
{{--            padding: 10px;--}}
{{--            width: 100%;--}}
{{--            font-size: 17px;--}}
{{--            font-family: Raleway;--}}
{{--            border: 1px solid #aaaaaa;--}}
{{--        }--}}
{{--        /* Mark input boxes that gets an error on validation: */--}}
{{--        input.invalid {--}}
{{--            background-color: #ffdddd;--}}
{{--        }--}}
{{--        /* Hide all steps by default: */--}}
{{--        .tab {--}}
{{--            display: none;--}}
{{--        }--}}
{{--        button {--}}
{{--            background-color: #04AA6D;--}}
{{--            color: #ffffff;--}}
{{--            border: none;--}}
{{--            padding: 10px 20px;--}}
{{--            font-size: 17px;--}}
{{--            font-family: Raleway;--}}
{{--            cursor: pointer;--}}
{{--        }--}}
{{--        button:hover {--}}
{{--            opacity: 0.8;--}}
{{--        }--}}
{{--        #prevBtn {--}}
{{--            background-color: #bbbbbb;--}}
{{--        }--}}
{{--        /* Make circles that indicate the steps of the form: */--}}
{{--        .step {--}}
{{--            height: 15px;--}}
{{--            width: 15px;--}}
{{--            margin: 0 2px;--}}
{{--            background-color: #bbbbbb;--}}
{{--            border: none;--}}
{{--            border-radius: 50%;--}}
{{--            display: inline-block;--}}
{{--            opacity: 0.5;--}}
{{--        }--}}
{{--        .step.active {--}}
{{--            opacity: 1;--}}
{{--        }--}}
{{--        /* Mark the steps that are finished and valid: */--}}
{{--        .step.finish {--}}
{{--            background-color: #04AA6D;--}}
{{--        }--}}
{{--        /* #submitID{--}}
{{--            background-color: rgb(101, 101, 105);--}}
{{--            color: whitesmoke;--}}
{{--            font-weight: bolder;--}}
{{--            border-radius: 10px;--}}
{{--            font-size: 18px;--}}
{{--            /* cursor: pointer; */--}}

{{--        /* #submitID:hover{--}}
{{--            opacity: .9;--}}
{{--        } */--}}
{{--    </style>--}}
</head>

<body onload="hd()" >



@include("patials.header")


@yield('content')


@include("patials.footer")

<div id="terms" class="modal fade">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                                    <div class="avatar">
                                        <img src="{{asset('public/frontend/img/atlantic_logo.png')}}" alt="" class="img-fluid">
                                    </div>
                                    <h4 class="modal-title">Terms & Conditions</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
{{--                <div class="container">--}}
                    <h3>DECLARATION</h3>
                    <p style="text-align: justify">I understand that it is the duty of the family to notify Atlantic Funeral Services as soon as possible after the death of any of the assured members. I declare that my family and I are in good health and that none of us , to the best of my knowledge ,has an illness that will lead to the event of an early death. I declare that all statements given in this application are true. Should it come to the attention of AFS that any of the above information is not accurate , membership under the policy can be cancelled , premiums paid will be forfeited and no claims under the scheme will be considered  for payment. I accept the terms and conditions of the scheme, and am well aware that natural death cover for all persons are six (6) calendar months from date of inception with AFS.</p>
                    <p style="text-align: justify">I understand that Unnatural / Accidental Death cover will commence after registration and receipt of the first premium by AFS and no death claim resulting from suicide will be considered for payment within the first two(2) years from date of inception of policy. Children of the main member and/or spouse are covered to age 21 next birthday , extended to 25 next birthday if single and a fulltime student/scholar at a registered organization. I realise that the benefits under this policy shall only be paid if AFS is notified by the policyholder within three(3) months of the date of death , and all documentation that AFS may request , are delivered to our office within three(3) months from date of death. In the event of the death of the main member , the spouse will be appointed as the main member and cover will continue as long as the adjusted premium are paid. All monthly premiums must be paid in advance in order to enjoy cover. (AFS- Atlantic Funeral Services ).

                    <hr>
                    <h5>TERMS & CONDITIONS</h5>
                    <ol>
                        <li>Effective 01 October 2001
                            <ol style="list-style:lower-alpha">
                                <li>Family cover is provided to member, his/her spouse and all unmarried children under the age of 21years.</li>
                                <li>All physical or mentally handicapped children dependent on their parents will enjoy cover- medical proof is required</li>
                                <li>Full time students up to 25years will enjoy cover -yearly proof is required of institute attended by student</li>
                                <li>Common law couples with an age gap of 14 years are also accepted</li>
                                <li>Legally adopted children are also accepted – adoption papers required</li>
                            </ol>
                        </li><br>

                        <li><ol style="list-style:lower-alpha">
                                <li>Singles and single parents are also accepted.</li>
                                <li>Children dependent on their parents don’t pay till they have their own policy.</li>
                                <li>Grandchildren are only accepted if they are legally adopted.</li>
                            </ol>
                        </li><br>

                        <li><ol style="list-style:lower-alpha">
                                <li>No medical examination is necessary. The company rely on the member’s total honesty.</li>
                                <li>Unnatural Causes (causes other than illnesses ) will have a one ( 1 ) month waiting period.</li>
                            </ol>
                        </li><br>

                        <li><ol style="list-style:lower-alpha">
                                <li>Full cover after 6 months for all new members up to 85 years of age including T.B., Cancer, Heart Failures, Kidney Failures, Pneumonia, HIV/Aids.</li>
                                <li>Suicides will have a 24 months waiting period.</li>
                            </ol>
                        </li><br>


                        <li><ol style="list-style:lower-alpha">
                                <li>No promises made by the contract/agent are binding to ATLANTIC FUNERALS.</li>
                            </ol>
                        </li><br>

                        <li><ol style="list-style:lower-alpha">
                                <li>All premiums (moneys), new applications, copies of I.D. documents/birth certificates, marriage certificates, amendments to policies and information regarding new members such as newborns, etc. to be handed in on or before the 7th of each month.</li>
                            </ol>
                        </li><br>

                        <li><ol style="list-style:lower-alpha">
                                <li>An entry fee of R40.00 is payable.</li>
                                <li>Applications will only be accepted if the necessary documents are furnished with full details and signed by the applicant.</li>
                                <li>In order for the application to be processed the entry fee and the first premium must accompany the application form.</li>
                            </ol>
                        </li><br>

                        <li><ol style="list-style:lower-alpha">
                                <li>Funeral undertakings & cover will be provided at reasonable rates.</li>
                            </ol>
                        </li><br>

                        <li><ol style="list-style:lower-alpha">
                                <li>Cover will cease within two months if member stops paying premiums.</li>
                            </ol>
                        </li>
                        <br>

                        <li><ol style="list-style:lower-alpha">
                                <li>In the event of the death of the main member , the spouse will be appointed as the main member.</li>
                                <li>In the event of the death of a single parent, the policy must be paid continuously in order for the other members to enjoy cover.</li>
                            </ol>
                        </li>
                        <br>

                        <li><ol style="list-style:lower-alpha">
                                <li>In the event of death the office must be notified immediately.</li>
                            </ol>
                        </li>
                        <br>

                        <li>
                            The Funeral Undertaker , Mr Selwyn De Vries is on call 24 / 7 / 365 at the following numbers : <ol style="list-style:lower-alpha">
                                <li>021 593 3708</li>
                                <li>84 4478850</li>
                                <li>021 593 0515 ( Fax )</li>
                            </ol>
                        </li>
                    </ol>

            </div>

        </div>
    </div>
</div>
<div id="callMeBack" class="modal fade">
    <div class="modal-dialog modal-login">
        <div class="modal-content">
            <div class="modal-header">

                                    <h4 class="modal-title">Call me back request</h4>

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form class="p-3" method="POST" action="{{ url('call_back') }}" >
                    @csrf
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" required placeholder="Enter your Name">
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" name="phoneNumber" required placeholder="Enter your Phone">
                    </div>

                    <button type="submit" class="default-btn">CALL ME BACK <span></span></button>
                </form>
                </div>
            </div>

        </div>
    </div>
<div id="commingSoon" class="modal fade">
    <div class="modal-dialog modal-login">
        <div class="modal-content">
            <div class="modal-header">

{{--                                    <h4 class="modal-title">Call me back request</h4>--}}

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <img src="{{asset('public/frontend/img/coming_soon.png')}}" alt="" class="img-fluid">
                </div>
            </div>

        </div>
    </div>
</div>
<div class="go-top"><i class="fas fa-chevron-up"></i><i class="fas fa-chevron-up"></i></div>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="{{asset('public/frontend/js/jquery.min.js')}}"></script>

<script src="{{asset('public/frontend/js/popper.min.js')}}"></script>

<script src="{{asset('public/frontend/js/bootstrap.min.js')}}"></script>

<script src="{{asset('public/frontend/js/parallax.min.js')}}"></script>

<script src="{{asset('public/frontend/js/owl.carousel.min.js')}}"></script>

<script src="{{asset('public/frontend/js/slick.min.js')}}"></script>

<script src="{{asset('public/frontend/js/jquery.meanmenu.js')}}"></script>

<script src="{{asset('public/frontend/js/jquery.appear.min.js')}}"></script>

<script src="{{asset('public/frontend/js/odometer.min.js')}}"></script>

<script src="{{asset('public/frontend/js/jquery.nice-select.min.js')}}"></script>

<script src="{{asset('public/frontend/js/jquery.magnific-popup.min.js')}}"></script>

<script src="{{asset('public/frontend/js/wow.min.js')}}"></script>

<script src="{{asset('public/frontend/js/jquery.ajaxchimp.min.js')}}"></script>

<script src="{{asset('public/frontend/js/form-validator.min.js')}}"></script>

<script src="{{asset('public/frontend/js/contact.blade.php-form-script.js')}}"></script>

<script src="{{asset('public/frontend/js/main.js')}}"></script>

{{----}}
{{--<script src="{{asset('public/portal/assets/js/jquery-3.5.0.min.js')}}"></script>--}}

{{--<script src="{{asset('public/portal/assets/js/popper.min.js')}}"></script>--}}
{{--<script src="{{asset('public/portal/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>--}}

{{--<script src="{{asset('public/portal/assets/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>--}}

{{--<script src="{{asset('public/portal/assets/js/admin.js')}}"></script>--}}



<script>

    function hd(){
        document.getElementById('subBtn').style.display = "none";
        // document.getElementById("submitID").disabled=true;
    }
    function enableButton(){
        document.getElementById("submitID").disabled=false;
        // document.getElementById("submitID").style.backgroundColor = "rgb(32, 32, 139)";
        }

    function focus(){
        let email = document.getElementById('emailContact').value;
        document.getElementById('mail').innerText = email;
       // alert(email);
    }

    function planCover(){
            var ageGroup = document.getElementById("ageGroup").value;
            var coverAmount = document.getElementById("coverAmount").value;
            var ageGroup = document.getElementById("ageGroup").value;
            var status = document.querySelector('input[name="marital"]:checked').value;
            if(ageGroup == "18 - 64 yrs" && coverAmount == "5000" && status == "Single"){
                document.getElementById("premium").value = "50"
            }
            else if(ageGroup == "65 - 74 yrs" && coverAmount == "5000" && status == "Single"){
                document.getElementById("premium").value = "75"
            }
            else if(ageGroup == "75 - 85 yrs" && coverAmount == "5000" && status == "Single"){
                document.getElementById("premium").value = "105"
            }
            else if(ageGroup == "18 - 64 yrs" && coverAmount == "7000" && status == "Single"){
                document.getElementById("premium").value = "55"
            }
            else if(ageGroup == "65 - 74 yrs" && coverAmount == "7000" && status == "Single"){
                document.getElementById("premium").value = "90"
            }
            else if(ageGroup == "75 - 85 yrs" && coverAmount == "7000" && status == "Single"){
                document.getElementById("premium").value = "117"
            }
            else if(ageGroup == "18 - 64 yrs" && coverAmount == "10000" && status == "Single"){
                document.getElementById("premium").value = "80"
            }
            else if(ageGroup == "65 - 74 yrs" && coverAmount == "10000" && status == "Single"){
                document.getElementById("premium").value = "140"
            }
            else if(ageGroup == "75 - 85 yrs" && coverAmount == "10000" && status == "Single"){
                document.getElementById("premium").value = "200"
            }
            else if(ageGroup == "18 - 64 yrs" && coverAmount == "15000" && status == "Single"){
                document.getElementById("premium").value = "125"
            }
            else if(ageGroup == "65 - 74 yrs" && coverAmount == "15000" && status == "Single"){
                document.getElementById("premium").value = "200"
            }
            else if(ageGroup == "75 - 85 yrs" && coverAmount == "15000" && status == "Single"){
                document.getElementById("premium").value = "245"
            }

            else if(ageGroup == "18 - 64 yrs" && coverAmount == "20000" && status == "Single"){
                document.getElementById("premium").value = "180"
            }

            else if(ageGroup == "18 - 64 yrs" && coverAmount == "25000" && status == "Single"){
                document.getElementById("premium").value = "205"
            }

            else if(ageGroup == "18 - 64 yrs" && coverAmount == "30000" && status == "Single"){
                document.getElementById("premium").value = "255"
            }


            else if(ageGroup == "18 - 64 yrs" && coverAmount == "5000" && status == "Married"){
                document.getElementById("premium").value = "55"
            }
            else if(ageGroup == "65 - 74 yrs" && coverAmount == "5000" && status == "Married"){
                document.getElementById("premium").value = "115"
            }
            else if(ageGroup == "75 - 85 yrs" && coverAmount == "5000" && status == "Married"){
                document.getElementById("premium").value = "175"
            }
            else if(ageGroup == "18 - 64 yrs" && coverAmount == "7000" && status == "Married"){
                document.getElementById("premium").value = "63"
            }
            else if(ageGroup == "65 - 74 yrs" && coverAmount == "7000" && status == "Married"){
                document.getElementById("premium").value = "140"
            }
            else if(ageGroup == "75 - 85 yrs" && coverAmount == "7000" && status == "Married"){
                document.getElementById("premium").value = "187"
            }
            else if(ageGroup == "18 - 64 yrs" && coverAmount == "10000" && status == "Married"){
                document.getElementById("premium").value = "90"
            }
            else if(ageGroup == "65 - 74 yrs" && coverAmount == "10000" && status == "Married"){
                document.getElementById("premium").value = "220"
            }
            else if(ageGroup == "75 - 85 yrs" && coverAmount == "10000" && status == "Married"){
                document.getElementById("premium").value = "240"
            }
            else if(ageGroup == "18 - 64 yrs" && coverAmount == "15000" && status == "Married"){
                document.getElementById("premium").value = "140"
            }
            else if(ageGroup == "65 - 74 yrs" && coverAmount == "15000" && status == "Married"){
                document.getElementById("premium").value = "265"
            }
            else if(ageGroup == "75 - 85 yrs" && coverAmount == "15000" && status == "Married"){
                document.getElementById("premium").value = "281"
            }

            else if(ageGroup == "18 - 64 yrs" && coverAmount == "20000" && status == "Married"){
                document.getElementById("premium").value = "200"
            }

            else if(ageGroup == "18 - 64 yrs" && coverAmount == "25000" && status == "Married"){
                document.getElementById("premium").value = "235"
            }

            else if(ageGroup == "18 - 64 yrs" && coverAmount == "30000" && status == "Married"){
                document.getElementById("premium").value = "285"
            }
            else{
                document.getElementById("premium").value = ""
            }

        }

    $( function () {
        $("#gmpolicy").click(function () {
            var text = $("#generalMember").text();
           // alert(text);
            if (typeof(Storage) !== "undefined") {
                // Store
                localStorage.setItem("policy", text);
            }
            $("#modal_body").val('text');
        });

        $("#empolicy").click(function () {
            var text = $("#executiveMember").text();
           // alert(text);
            if (typeof(Storage) !== "undefined") {
                // Store
                localStorage.setItem("policy", text);
            }
            $("#modal_body").val('text');
        });

        $("#append").click( function(e) {
            e.preventDefault();
            $(".inc").append('<div class="controls">\
                <div class="row" style="text-align: left">\
                <div class="col">\
                <label>Last Name</label><br>\
            <input name="depfname" class="form-control">\
            </div>\
            <div class="col">\
                <label>First Name</label><br>\
                <input name="deplname"  class="form-control">\
            </div>\
        </div><br>\
        <div class="row" style="text-align: left">\
                    <div class="col">\
                        <label>Date Of Birth</label><br>\
                        <input  type="date" id="dep1dob"  class="form-control">\
                    </div>\
                    <div class="col">\
                        <label>Birth Certificate Copy</label><br>\
                        <input  type="file"  class="form-control">\
                    </div>\
                </div><br>\
                <hr>\
            </div>');
            return false;
        });

        jQuery(document).on('click', '.remove_this', function() {
            jQuery(this).parent().remove();
            return false;
        });
        $("input[type=submit]").click(function(e) {
            e.preventDefault();
            $(this).next("[name=textbox]")
                .val(
                    $.map($(".inc :text"), function(el) {
                        return el.value
                    }).join(",\n")
                )
        })
    });

    var currentTab = 0; // Current tab is set to be the first tab (0)
    showTab(currentTab); // Display the current tab
    function showTab(n) {
        // This function will display the specified tab of the form...
        var x = document.getElementsByClassName("tab");
        x[n].style.display = "block";
        //... and fix the Previous/Next buttons:
        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }
        if (n == (x.length - 1)) {
            document.getElementById('subBtn').style.display = "inline";
            document.getElementById('nextBtn').style.display = "none";
        } else {
            document.getElementById("nextBtn").innerHTML = "Next";
        }
        //... and run a function that will display the correct step indicator:
        fixStepIndicator(n)
    }
    function nextPrev(n) {
        // This function will figure out which tab to display
        var x = document.getElementsByClassName("tab");
        // Exit the function if any field in the current tab is invalid:
        if (n == 1 && !validateForm()) return false;
        // Hide the current tab:
        x[currentTab].style.display = "none";
        // Increase or decrease the current tab by 1:
        currentTab = currentTab + n;
        // if you have reached the end of the form...
        if (currentTab >= x.length) {
            // ... the form gets submitted:
            document.getElementById("regForm").submit();
            return false;
        }
        // Otherwise, display the correct tab:
        showTab(currentTab);
    }
    function validateForm() {
        // This function deals with validation of the form fields
        var x, y, i, valid = true;
        x = document.getElementsByClassName("tab");
        y = x[currentTab].getElementsByTagName("input");
        // A loop that checks every input field in the current tab:
        for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
                // add an "invalid" class to the field:
                y[i].className += " invalid";
                // and set the current valid status to false
                valid = false;
            }
        }
        // If the valid status is true, mark the step as finished and valid:
        if (valid) {
            document.getElementsByClassName("step")[currentTab].className += " finish";
        }
        return valid; // return the valid status
    }
    function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        //... and adds the "active" class on the current step:
        x[n].className += " active";
    }


</script>

{{--for toasters--}}
<script>
    @if(Session::has('message'))
        toastr.options =
        {
            "closeButton" : true,
            "progressBar" : true
        }
    toastr.success("{{ session('message') }}");
    @endif

        @if(Session::has('error'))
        toastr.options =
        {
            "closeButton" : true,
            "progressBar" : true
        }
    toastr.error("{{ session('error') }}");
    @endif

        @if(Session::has('info'))
        toastr.options =
        {
            "closeButton" : true,
            "progressBar" : true
        }
    toastr.info("{{ session('info') }}");
    @endif

        @if(Session::has('warning'))
        toastr.options =
        {
            "closeButton" : true,
            "progressBar" : true
        }
    toastr.warning("{{ session('warning') }}");
    @endif
</script>
<script>
    $(document).ready(function() {
        toastr.options.timeOut = 10000;
        @if (Session::has('error'))
        toastr.error('{{ Session::get('error') }}');
        @elseif(Session::has('success'))
        toastr.success('{{ Session::get('success') }}');
        @endif
    });

</script>

</body>


</html>
