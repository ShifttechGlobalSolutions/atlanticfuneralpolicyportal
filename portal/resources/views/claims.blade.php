@extends("layouts.master")
@section("content")



    <div class="page-title-area page-title-bg1">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2>Claims</h2>
                        <ul>
                            <li><a href="{{ url('/') }}">Home</a></li>
                            <li>Claims</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>






    <section class="services-area ptb-100 pb-70 ">
        <div class="container">
            <div class="section-title">
                <h3>Keeping the claims process as simple as possible</h3>
                {{--                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}
            </div>
            <div class="row">

                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-services-box" style="height: 419px;">
                        <div class="icon">
                            <i class="flaticon-insurance"></i>
                            <div class="icon-bg">
                                <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">
                                <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">
                            </div>
                        </div>
                        <h3><a  href="{{url('/login')}}">Where to begin</a></h3>
                        <p>Start your claim via our app or client portal. Or simply call us on 0844478850. We're ready to help out.</p>
                        <a  href="{{url('/login')}}" class="read-more-btn">Submit Claim</a><br>
                        <a href="{{url('/login')}}" class="read-more-btn">TRACK CLAIM <span></span></a>
                        <div class="box-shape">
                            <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">
                            <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-services-box" style="height: 419px;">
                        <div class="icon">
                            <i class="flaticon-health-insurance"></i>
                            <div class="icon-bg">
                                <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">
                                <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">
                            </div>
                        </div>
                        <h3><a  href="{{url('/login')}}">Stay up to date</a></h3>
                        <p>Thanks to our claims tracker, you can stay on top of the entire process via our app or client portal. Your allocated claims advisor can also be your point of contact.</p>
                        <a href="{{url('/login')}}" class="read-more-btn">Update Policy</a>
                        <div class="box-shape">
                            <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">
                            <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-services-box" style="height: 419px;">
                        <div class="icon">
                            <i class="flaticon-home-insurance"></i>
                            <div class="icon-bg">
                                <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">
                                <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">
                            </div>
                        </div>
                        <h3><a  href="{{url('/login')}}">Done and dusted</a></h3>
                        <p>We'll get your life back to normal as soon as we can, thanks to our hand-picked service providers.</p>
                        <a  href="{{url('/login')}}" class="read-more-btn">Find A Document</a>
                        <div class="box-shape">
                            <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">
                            <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </section>

{{--    <section class="services-area ptb-100 pb-70 bg-f8f8f8">--}}
{{--    <div class="container">--}}
{{--        <div class="section-title">--}}
{{--            <h3>Need to make a claim? Here's where to begin</h3>--}}
{{--            --}}{{--                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}
{{--        </div>--}}
{{--        <div class="row">--}}

{{--            <div class="col-lg-4 col-md-6 col-sm-6">--}}
{{--                <div class="single-services-box" style="height: 412px;">--}}
{{--                    <div class="icon">--}}
{{--                        <i class="flaticon-insurance"></i>--}}
{{--                        <div class="icon-bg">--}}
{{--                            <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">--}}
{{--                            <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <h3><a href="business-insurance.html">Client Portal</a></h3>--}}
{{--                    <p>Click below. We'll need your ID number, or if you already have an AtlanticID, simply log in.</p>--}}
{{--                    <div class="btn-box">--}}
{{--                        <a href="pricing.html" class="default-btn">SUBMIT A CLAIM <span></span></a><br>--}}
{{--                        <a href="contact.html" class="optional-btn">TRACK CLAIM <span></span></a>--}}
{{--                    </div>--}}

{{--                    <div class="box-shape">--}}
{{--                        <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">--}}
{{--                        <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-lg-4 col-md-6 col-sm-6">--}}
{{--                <div class="single-services-box" style="height: 412px;">--}}
{{--                    <div class="icon">--}}
{{--                        <i class="flaticon-health-insurance"></i>--}}
{{--                        <div class="icon-bg">--}}
{{--                            <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">--}}
{{--                            <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <h3><a href="health-insurance.html">The App</a></h3>--}}
{{--                    <p>Submit and track your claim via the Atlantic app. Download it now.</p>--}}
{{--                    <div class="btn-box">--}}
{{--                        <a href="pricing.html" class="default-btn">App Store <span></span></a><br>--}}
{{--                        <a href="contact.html" class="optional-btn">Google play <span></span></a>--}}
{{--                    </div>--}}
{{--                    <div class="box-shape">--}}
{{--                        <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">--}}
{{--                        <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-lg-4 col-md-6 col-sm-6">--}}
{{--                <div class="single-services-box" style="height: 412px;">--}}
{{--                    <div class="icon">--}}
{{--                        <i class="flaticon-home-insurance"></i>--}}
{{--                        <div class="icon-bg">--}}
{{--                            <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">--}}
{{--                            <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <h3><a href="home-insurance.html">Speak to a human</a></h3>--}}
{{--                    <p>Call our claims centre and speak to a friendly claims advisor who will help you out.</p>--}}
{{--                    <a href="home-insurance.html" class="read-more-btn">0844478850</a>--}}
{{--                    <div class="box-shape">--}}
{{--                        <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">--}}
{{--                        <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--        </div>--}}


{{--    </div>--}}
{{--</section>--}}







@endsection
