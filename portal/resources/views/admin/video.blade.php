@extends('layouts.portalMaster')

@section('content')
    <div class="page-wrapper">
        <div class="content container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Video Tutorials</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Video Tutorials</li>
                        </ul>
                    </div>
                    <div class="col-auto text-right">
                        <a class="btn btn-light filter-btn" href="{{ url('admin/clear-search') }}" data-toggle="tooltip" title="Clear Search">
                            <i class="fas fa-times"></i>
                        </a>
                        <a class="btn btn-white filter-btn" href="javascript:void(0);" id="filter_search" data-toggle="tooltip" title="Search">
                            <i class="fas fa-filter"></i>
                        </a>
                        <a href="{{url('admin/addvideo')}}" class="btn btn-primary add-button ml-3" data-toggle="tooltip" title="Add New Video">
                            <i class="fas fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>

            <div class="card filter-card" id="filter_inputs">
                <div class="card-body pb-0">
                    <form method="POST" action="{{ route('admin/search-video')}}"  enctype="multipart/form-data"/>
                    @CSRF
                    <div class="row filter-row" >

                        <div class="col-sm-12">
                        <div class="input-group">
                            <div class="form-outline">
                              <input type="search" id="form1" class="form-control" name="searchItem" style="width:300px" placeholder="Enter Video title..."/><br>
                              {{-- <label class="form-label" for="form1">Search</label> --}}
                            </div>
                            <button type="submit" class="btn btn-primary" style="width: 100px">
                              <i class="fas fa-search"></i>
                            </button>
                          </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            {!! $video->links() !!}
                            <div class="tab-content profile-tab-cont">
                                <div class="table-responsive tab-pane show active" id="all_polices">
                                    <table class="table table-hover table-center mb-0 datatable">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Video</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                @foreach($video as $v)
                                    <tr>
                                        <td>{{ $v->id}}</td>
                                        <td>
                                            <video  width="70" height="70" controlsList="nodownload">
                                                <source src="{{ asset('storage/app/public/video/'.$v->video) }}" type="video/mp4">
                                            </video>
                                        </td>

                                        <td>{{ $v->title }}</td>
                                        <td>{{ $v->description }}</td>
                                        <td>
                                            <a href="{{ url('admin/show-video/'.$v->id) }}" class="btn btn-info">
                                                <i class="far fa-eye mr-1"></i> View
                                            </a>
                                            <a href="{{ url('admin/delete-video/'.$v->id) }}" class="btn btn-danger">
                                                <i class="far fa-trash-alt"></i> Delete
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div><br>
                            {!! $video->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
