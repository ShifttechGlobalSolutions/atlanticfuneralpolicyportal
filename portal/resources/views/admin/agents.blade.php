@extends('layouts.portalMaster')

@section('content')
    <div class="page-wrapper">

        <div class="content container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Agents</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href=". url('/home') .">Dashboard</a></li>
                            <li class="breadcrumb-item active">Agents</li>
                        </ul>
                    </div>
                    <div class="col-auto text-right">
                        <a class="btn btn-light filter-btn" href="{{ url('clear-search') }}" data-toggle="tooltip" title="Clear Search">
                            <i class="fas fa-times"></i>
                        </a>
                        <a class="btn btn-white filter-btn" href="javascript:void(0);" id="filter_search" data-toggle="tooltip" title="Search Agent">
                            <i class="fas fa-filter"></i>
                        </a>
                        <a href="{{ route('users.create') }}" class="btn btn-primary add-button ml-3" data-toggle="tooltip" title="Add New Agent">
                            <i class="fas fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>

            <div class="card filter-card" id="filter_inputs">
                <div class="card-body pb-0">
                    <form method="POST" action="{{route('agent-search') }}"  enctype="multipart/form-data"/>
                    @CSRF
                        <div class="row filter-row">
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label">Search for an Agent(s)</label>
                                    <input class="form-control" name="agents" type="text">
                                </div>
                            </div>

                            <div class="col-sm-6 col-md-3">

                                <div class="form-group">
                                    <button class="btn btn-primary btn-block" type="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <ul class="nav nav-tabs menu-tabs">
                <li class="nav-item active">
                    <a class="nav-link" data-toggle="tab" href="#all_agents">All Agents <span class="badge badge-primary">{{ $allagents1}}</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#active_agents">Active Agents <span class="badge badge-primary">{{ $activeAgents}}</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#inactive_agents">InActive Agents <span class="badge badge-primary">{{ $inactiveAgents1}}</span></a>
                </li>


            </ul>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content profile-tab-cont">
                                <div class="table-responsive tab-pane show active" id="all_agents">
                                    <table class="table table-hover table-center mb-0 datatable">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Image</th>
                                            <th>Fullname</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($agents as $Agents)
                                        <tr>
                                            <td>{{ $Agents->id }}</td>
                                            <td>
                                                <a href="service-details.html">
                                                    <img class="rounded service-img mr-1" src="asset('storage/app/public/images/avatars/'.$Agents->image" alt="">
                                                </a>
                                            </td>
                                            <td>{{ $Agents->name }}</td>
                                            <td>{{ $Agents->email }}</td>
                                            <td>{{ $Agents->mobile}}</td>
                                            <td class="text">{{ $Agents->status }}
                                                {{-- // .-- <div class="status-toggle">
                                                //     <input id="service_1" class="check" type="checkbox">
                                                //     <label for="service_1" class="checktoggle">. $Agents->status.</label>
                                                // </div> --. --}}
                                            </td>
                                            <td>
                                                <a href="{{ url('admin/view-agent/'.$Agents->id) }}" class="btn btn-info ">
                                                    <i class="far fa-eye mr-1"></i> View
                                                </a>
                                                <a href="{{ url('admin/delete-agent/'.$Agents->id) }}" class="btn btn-danger">
                                                <i class="far fa-trash-alt"></i> Delete
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        {!! $agents->links() !!}

                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-responsive tab-pane fade" id="active_agents">
                                    <table class="table table-hover table-center mb-0 datatable">
                                        <thead>
                                       <tr>
                                            <th>#</th>
                                            <th>Image</th>
                                            <th>Fullname</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($activeAgents1 as $activeagents1)
                                        <tr>
                                            <td>{{ $activeagents1->id }}</td>
                                            <td>
                                                <a href="service-details.html">
                                                    <img class="rounded service-img mr-1" src="asset('storage/app/public/images/avatars/'.$activeagents1->image" alt="">
                                                </a>
                                            </td>
                                            <td>{{ $activeagents1->name }}</td>
                                            <td>{{ $activeagents1->email }}</td>
                                            <td>{{ $activeagents1->mobile}}</td>
                                            <td class="text">{{ $activeagents1->status }}
                                                {{-- // .-- <div class="status-toggle">
                                                //     <input id="service_1" class="check" type="checkbox">
                                                //     <label for="service_1" class="checktoggle">. $Agents->status.</label>
                                                // </div> --. --}}
                                            </td>
                                            <td>
                                                <a href=". url('admin/claims/view-claims/'.$claims->id) ." class="btn btn-info ">
                                                    <i class="far fa-eye mr-1"></i> View
                                                </a>
                                                <a href=". url('admin/claims/view-claims/'.$claims->id) ." class="btn btn-danger">
                                                <i class="far fa-trash-alt"></i> Delete
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        {!! $activeAgents1->links() !!}

                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-responsive tab-pane fade" id="inactive_agents">
                                    <table class="table table-hover table-center mb-0 datatable">
                                        <thead>
                                       <tr>
                                            <th>#</th>
                                            <th>Image</th>
                                            <th>Fullname</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($inactiveAgents as $inactiveagents1)
                                        <tr>
                                            <td>{{ $inactiveagents1->id }}</td>
                                            <td>
                                                <a href="service-details.html">
                                                    <img class="rounded service-img mr-1" src="asset('storage/app/public/images/avatars/'.$inactiveagents1->image" alt="">
                                                </a>
                                            </td>
                                            <td>{{ $inactiveagents1->name }}</td>
                                            <td>{{ $inactiveagents1->email }}</td>
                                            <td>{{ $inactiveagents1->mobile}}</td>
                                            <td class="text">{{ $inactiveagents1->status }}
                                                {{-- // .-- <div class="status-toggle">
                                                //     <input id="service_1" class="check" type="checkbox">
                                                //     <label for="service_1" class="checktoggle">. $Agents->status.</label>
                                                // </div> --. --}}
                                            </td>
                                            <td>
                                                <a href=". url('admin/claims/view-claims/'.$claims->id) ." class="btn btn-info ">
                                                    <i class="far fa-eye mr-1"></i> View
                                                </a>
                                                <a href=". url('admin/claims/view-claims/'.$claims->id) ." class="btn btn-danger">
                                                <i class="far fa-trash-alt"></i> Delete
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        {!! $inactiveAgents->links() !!}

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
