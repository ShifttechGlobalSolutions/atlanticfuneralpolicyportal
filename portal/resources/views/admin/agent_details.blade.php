@extends('layouts.portalMaster')

@section('content')
    <div class="page-wrapper">

        <div class="content container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">View Agent Details</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active"><a href="{{ url('admin/agents') }}">Agents</a></li>
                            <li class="breadcrumb-item active">View Agent</li>
                        </ul>
                    </div>
                    <form action="" method="POST">
                    @foreach($agentInfo as $AgentInfo)
                    <div class="col-auto text-right">
                        <div class="input-group mb-6">
                            <label class="input-group-text" for="inputGroupSelect01">Status</label>
                                <select class="form-select" id="sel">
                                    <option value="{{ $AgentInfo->status }}">{{ $AgentInfo->status }}</option>
                                    <option value="Approved">Active</option>
                                    <option value="Pending">InActive</option>

                                </select>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

            {{-- test --}}

                  <!-- /Breadcrumb -->
                 @foreach ($agentInfo as $Agent)
                {{-- <div class="container">
                <div class="main-body"> --}}
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="card">
                                <div class="card-body">
                                <form method="POST" action="" enctype="multipart/form-data">
                                    @csrf
                                    <div class="d-flex flex-column align-items-center text-center">
                                        <img src="{{ asset('storage/app/public/images/avatars/'.$Agent->image) }}" alt="Admin" class="rounded-circle p-1 bg-primary" name="avatar" width="110" height='130'>
                                        <br><span><strong>{{ $Agent->name}}</strong></span>
                                        <span><a href="mailto: {{$Agent->email}}">{{ $Agent->email}}</a></span>
                                        <span><a href="tel: {{$Agent->mobile}}">{{ $Agent->mobile}}</a></span>
                                        <span>{{ $Agent->address.", "}} {{ $Agent->suburb.", "}} {{ $Agent->city.", " }} {{ $Agent->country.", " }} {{ $Agent->postalCode }}</span>
                                    </div>
                                    <hr class="my-4">
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                            <h6 class="mb-0">All Applications</h6>
                                            <span class="text-secondary">{{ $appl }}</span>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                            <h6 class="mb-0">Current Month Applications</h6>
                                            <span class="text-secondary">{{ $applCur }}</span>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                            <h6 class="mb-0">All Claims</h6>
                                            <span class="text-secondary">{{ $cl }}</span>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                            <h6 class="mb-0">Current Month Claims</h6>
                                            <span class="text-secondary">{{ $clCur}}</span>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                            <h6 class="mb-0">Commission</h6>
                                            <span class="text-secondary">R100</span>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                            <h6 class="mb-0">Current Month Commission</h6>
                                            <span class="text-secondary">R100</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">


                            <div class="card">
                                <div class="card-body">
                                    <div class="row mb-3">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">Description</h6>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            <p>{{ $Agent->description }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="card">
                                <div class="card-body">
                                    <div class="row mb-3">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">Full Name</h6>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            <input type="text" class="form-control" name="name" value="{{ $Agent->name}}">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">Email</h6>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            <input type="text" class="form-control" readonly name="email" value="{{ $Agent->email}}">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">Phone</h6>
                                        </div>
                                        {{-- <div class="col-sm-9 text-secondary">
                                            <input type="text" class="form-control" name="phone" value="{{ $Agent->phone}}">
                                        </div> --}}
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">Mobile</h6>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            <input type="text" class="form-control" name="mobile"  value="{{ $Agent->mobile}}">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">Address</h6>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            <input type="text" class="form-control" name="address" value="{{ $Agent->address}}">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">Suburb</h6>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            <input type="text" class="form-control" name="suburb" value="{{ $Agent->suburb}}">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">City</h6>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            <input type="text" class="form-control" name="city" value="{{ $Agent->city}}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9 text-secondary">
                                            <input type="submit" class="btn btn-primary px-4" value="Save Changes">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    {{-- </div>
                </div> --}}
            </div>
            @endforeach


            {{-- end test --}}



            {{-- <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content profile-tab-cont">
                                <div class="row">
                                    <div class="col">
                                        <h5>Policy Holder</h5>
                                    </div>
                                    <div class="col-auto text-right" data-toggle="tooltip" title="Update">
                                        <a href="#"><i class='fas fa-edit' style='color: blue'></i></a>
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Policy ID</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->policyHolderId }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Surname</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->policyHolderSurname }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Firstname</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->policyHolderFullName }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">D.O.B</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->policyHolderDateOfBirth }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Email</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->policyHolderEmail }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Contact</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->policyHolderContactNumber }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">ID No.</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->policyHolderIdNumber }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Postal Address</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->policyHolderPostalAddress }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Residential Address</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->policyHolderResidentialAddress }}">
                                    </div>
                                  </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content profile-tab-cont">
                                <div class="row">
                                    <div class="col">
                                        <h5>Deceased Information</h5>
                                    </div>
                                    <div class="col-auto text-right" data-toggle="tooltip" title="Update">
                                        <a href="#"><i class='fas fa-edit' style='color: blue'></i></a>
                                    </div>
                                </div>
                                <hr>

                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Surname</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->deceasedSurname }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Firstname</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->deceasedFullName }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">D.O.B</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->deceasedDateOfBirth }}">
                                    </div>
                                  </div>

                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Contact</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->deceasedContactNumber }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Cause of Death</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->deceasedCauseOfDeath }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Last Address</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->deceasedLastAddress }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Death Certificate Serial No.</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->deceasedDeathCertificateSerialNo }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">BI Serial No.</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->deceasedBiSerialNo }}">
                                    </div>
                                  </div>
                                    <div class="row">
                                        <label for="staticEmail" class="col-sm-5 col-form-label">Residential Address</label>
                                        <div class="col-sm-7">
                                          <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->deceasedResidentialAddress }}">
                                        </div>
                                  </div>
                            </div>
                        </div>
                    </div>
                </div>
 --}}

            {{-- ggg --}}
            {{-- <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content profile-tab-cont">

                                <div class="row">
                                    <div class="col">
                                        <h5>Claimant Information</h5>
                                    </div>
                                    <div class="col-auto text-right" data-toggle="tooltip" title="Update">
                                        <a href="#"><i class='fas fa-edit' style='color: blue'></i></a>
                                    </div>
                                </div>
                                <hr>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Surname</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->claimantSurname }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Firstname</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->claimantFullName }}">
                                    </div>
                                  </div>

                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Email</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->claimantContactNumber }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Contact</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->claimantEmail }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">ID No.</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->claimantIdNumber }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Relationship</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->claimantRelationship }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Postal Address</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->claimantPostalAddress }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Residential Address</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->claimantResidentialAddress }}">
                                    </div>
                                  </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content profile-tab-cont">

                                <div class="row">
                                    <div class="col">
                                        <h5>Beneficiary Information</h5>
                                    </div>
                                    <div class="col-auto text-right" data-toggle="tooltip" title="Update">
                                        <a href="#"><i class='fas fa-edit' style='color: blue'></i></a>
                                    </div>
                                </div>
                                <hr>

                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Full Name</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->beneficiaryName }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">ID No.</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->beneficiaryIdNumber }}">
                                    </div>
                                  </div>

                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Bank Name</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->beneficiaryBankName }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Branch Name</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->beneficiaryBranch }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Account No.</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->beneficiaryAccountNumber }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Branch Code</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->beneficiaryBranchCode }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Account Type</label>
                                    <div class="col-sm-7">
                                      <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->beneficiaryAccountType }}">
                                    </div>
                                  </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content profile-tab-cont">

                                <div class="row">
                                    <div class="col">
                                        <h5>Parlour Information</h5>
                                    </div>
                                    <div class="col-auto text-right" data-toggle="tooltip" title="Update">
                                        <a href="#"><i class='fas fa-edit' style='color: blue'></i></a>
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Parlour Name</label>
                                    <div class="col-sm-7">
                                        <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->funeralParlourName }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Contact</label>
                                    <div class="col-sm-7">
                                        <input type="text"  class="form-control-plaintext border-top-0 border-dark" id="staticEmail" value="{{ $AgentInfo->funeralParlourPhone }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}

                {{-- <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content profile-tab-cont">

                                <div class="row" style="text-align: center">
                                    <div class="col-sm-6">
                                    <button type="button" class="btn btn-secondary btn-lg">Back</button>
                                    </div>
                                    <div class="col-sm-6">
                                        <button type="button" class="btn btn-primary btn-lg">Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>

    </div>

</div>





</div>

</div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(function(){
        var da = $("#sel").html()
        // alert(da)
        // da != "Approved" ? $(".text").css("color", "green") :  $(".text").css("color", "yellow")
        // // $(".text").css("color", "green")
        // switch(da){
        //     case "Approved": $(".text").css("color", "green")
        //     break;
        //     case "Pending" : $(".text").css("color", "yellow")
        //     break;
        //     case "Declined" : $(".text").css("color", "red")
        //     break;
        // }
        if (da != "Pending" && da != "Declined"){
            $("#sel").css("color", "green")
        }
        else if(da != "Approved" && da != "Declined"){
            $("#sel").css("color", "yellow")
        }
        else if(da == "Approved" && da != "Pending"){
            $("#sel").css("color", "red")
        }
    })
</script>
@endsection


