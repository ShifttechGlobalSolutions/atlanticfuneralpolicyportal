@extends('layouts.portalMaster')

@section('content')


    <div class="page-wrapper">
        <div class="content container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Add a Tutorial</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ url('admin/tutorials') }}">Video Tutorials</a></li>
                            <li class="breadcrumb-item active">Add Tutorials</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">

                    <div class="profile-menu">
                    <form method="post" action="{{ route('admin/add-video') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="hidden"  class="form-control" id="id" name="id" placeholder="Video Title" style="background-color: #F5F5F5;color: black">
                            <input type="text"  class="form-control" id="title" name="title" placeholder="Video Title" style="background-color: #F5F5F5;color: black">
                        </div>

                        <div class="form-group">
                            <label for="file">Video</label>
                            <input type="file" class="form-control" id="file" name="file"   style="background-color: #F5F5F5;color: black">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description" id="description" placeholder="Video Description"></textarea>
                        </div>

                        <div class="form-group">

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
