@extends('layouts.portalMaster')

@section('content')


    <div class="page-wrapper">
        <div class="content container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Claim Details</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Claim Details</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">

                    <div class="profile-menu">
                        <ul class="nav nav-tabs nav-tabs-solid">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#policy">Policy Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#deceased">Deceased Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#claimant">Claimant Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#payment">Payment Options</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#beneficiary">Documents</a>
                            </li>


                        </ul>
                    </div>
                    <form  enctype="multipart/form-data" id="upload-file" >
                        @CSRF
                        <div class="tab-content profile-tab-cont">
                            @foreach ($claimInfo as $claims)
                            @endforeach
                            <div id="policy" class="tab-pane fade show active">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Policy Holder Details</h5>
                                        <div class="row">
                                            <div class="col-md-4 ">

                                                <div class="form-group">
                                                    <label>Policy No</label>
                                                    <input type="text" name="policyHolderId" class="form-control" value="{{$claims->policyHolderId}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Surname</label>
                                                    <input type="text" name="policyHolderSurname" class="form-control" value="{{$claims->policyHolderSurname}}"readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>First Name</label>
                                                    <input type="text" name="policyHolderFullName" class="form-control" value="{{$claims->policyHolderFullName}}"readonly>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-md-4">

                                                <div class="form-group">
                                                    <label>Date of Birth</label>
                                                    <input type="text" name="policyHolderDateOfBirth"
                                                           class="form-control" value="{{$claims->policyHolderDateOfBirth}}"readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Email Address</label>
                                                    <input type="text" name="policyHolderEmail" class="form-control" value="{{$claims->policyHolderEmail}}"readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="tel" name="policyHolderContactNumber"
                                                           class="form-control" value="{{$claims->policyHolderContactNumber}}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>ID Number/ Passport</label>
                                                    <input type="text" name="policyHolderIdNumber" class="form-control" value="{{$claims->policyHolderIdNumber}}"readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Postal Address</label>
                                                    <input type="text" name="policyHolderPostalAddress"
                                                           class="form-control" value="{{$claims->policyHolderPostalAddress}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Residential Address</label>
                                                    <input type="text" name="policyHolderResidentialAddress"
                                                           class="form-control" value="{{$claims->policyHolderResidentialAddress}}" readonly>
                                                </div>

                                            </div>

                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div id="deceased" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Deceased Details</h5>
                                        <div class="row">


                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Surname</label>
                                                    <input type="text" name="deceasedSurname" class="form-control" value="{{$claims->deceasedSurname}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>FullName</label>
                                                    <input type="text" name="deceasedFullName" class="form-control" value="{{$claims->deceasedFullName}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="tel" name="deceasedContactNumber" class="form-control" value="{{$claims->deceasedContactNumber}}" readonly>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">


                                            <div class="col-md-4">

                                                <div class="form-group">
                                                    <label>Last Known Address</label>
                                                    <input type="text" name="deceasedLastAddress" class="form-control" value="{{$claims->deceasedLastAddress}}" readonly>
                                                </div>

                                            </div>

                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Date of Birth</label>
                                                    <input type="text" name="deceasedDateOfBirth" class="form-control" value="{{$claims->deceasedDateOfBirth}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Cause of Death</label>
                                                    <input type="text" name="deceasedCauseOfDeath" class="form-control" value="{{$claims->deceasedCauseOfDeath}}" readonly>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Death Certificate Serial No</label>
                                                    <input type="text" name="deceasedDeathCertificateSerialNo"
                                                           class="form-control" value="{{$claims->deceasedDeathCertificateSerialNo}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>BI-1663 Serial No</label>
                                                    <input type="text" name="deceasedBiSerialNo" class="form-control" value="{{$claims->deceasedBiSerialNo}}" readonly>
                                                </div>

                                            </div>

                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Residential Address</label>
                                                    <input type="text" name="deceasedResidentialAddress"
                                                           class="form-control" value="{{$claims->deceasedResidentialAddress}}" readonly>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div id="claimant" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Claimant Details</h5>
                                        <div class="row">
                                            <div class="col-md-4 ">

                                                <div class="form-group">
                                                    <label>Surname</label>
                                                    <input type="text" name="claimantSurname" class="form-control" value="{{$claims->claimantSurname}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" name="claimantFullName" class="form-control" value="{{$claims->claimantFullName}}" readonly>
                                                </div>
                                            </div>
{{--                                            <div class="col-md-4 ">--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label>Date of Birth</label>--}}
{{--                                                    <input type="date" name="claimantDateOfBirth" class="form-control" value="{{$claims->claimantDateOfBirth}}">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="tel" name="claimantContactNumber"
                                                           class="form-control" value="{{$claims->claimantContactNumber}}" readonly>
                                                </div>

                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Email Address</label>
                                                    <input type="email" name="claimantEmail" class="form-control" value="{{$claims->claimantEmail}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>ID Number</label>
                                                    <input type="text" name="claimantIdNumber" class="form-control" value="{{$claims->claimantIdNumber}}" readonly>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Relationship</label>
                                                    <input type="text" name="claimantRelationship"
                                                           class="form-control" value="{{$claims->claimantRelationship}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Postal Address</label>
                                                    <input type="text" name="claimantPostalAddress"
                                                           class="form-control" value="{{$claims->claimantPostalAddress}}" readonly>
                                                </div>

                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Residential Address</label>
                                                    <input type="text" name="claimantResidentialAddress"
                                                           class="form-control" value="{{$claims->claimantResidentialAddress}}" readonly>
                                                </div>
                                            </div>
                                        </div>




                                    </div>
                                </div>
                            </div>
                            <div id="payment" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Benefit Payment Options</h5>
                                        <div class="row">


                                            <div class="form-group col-md-6 ">
                                                <label>Parlour Name</label>
                                                <input type="text" name="funeralParlourName" class="form-control" value="{{$claims->funeralParlourName}}" readonly>
                                            </div>
                                            <div class="form-group col-md-6 ">
                                                <label>Contact Number</label>
                                                <input type="text" name="funeralParlourPhone" class="form-control" value="{{$claims->funeralParlourPhone}}" readonly>
                                            </div>
                                            <hr>

                                        </div>
                                       <hr>
                                        <h5 class="card-title">Nominated Beneficiary</h5>
                                        <div class="row">
                                            <div class="form-group col">
                                                <label>Beneficiary Name</label>
                                                <input name="beneficiaryName" type="text" class="form-control" value="{{$claims->beneficiaryName}}">
                                            </div>
                                            <div class="form-group col">
                                                <label>Beneficiary Id Number</label>
                                                <input name="beneficiaryIdNumber" type="text" class="form-control" value="{{$claims->beneficiaryIdNumber}}">
                                            </div>
                                            <div class="form-group col">
                                                <label>Beneficiary Bank Name</label>
                                                <input name="beneficiaryBankName" type="text" class="form-control" value="{{$claims->beneficiaryBankName}}">
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="form-group col">
                                                <label>Beneficiary Bank Branch</label>
                                                <input name="beneficiaryBranch" class="form-control " type="text" value="{{$claims->beneficiaryBranch}}">
                                            </div>
                                            <div class="form-group col">
                                                <label>Beneficiary Account Number</label>
                                                <input name="beneficiaryAccountNumber" type="text" class="form-control" value="{{$claims->beneficiaryAccountNumber}}">
                                            </div>
                                            <div class="form-group col">
                                                <label>Beneficiary Branch Code</label>
                                                <input name="beneficiaryBranchCode" class="form-control " type="text" value="{{$claims->beneficiaryBranchCode}}">
                                            </div>
                                        </div>


                                        <label for="email"><strong>Account type:</strong></label>

                                        <div class="row">
                                            <div class="form-group col-md-4">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio"
                                                                               name="beneficiaryAccountType"
                                                                               value="Current" checked>&nbsp;<span>Current (cheque)</span><br>
                                            </div>
                                            <div class="form-group col-md-4">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio"
                                                                               name="beneficiaryAccountType"
                                                                               value="Savings">&nbsp;<span>Savings</span><br>
                                            </div>
                                            <div class="form-group col-md-4">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio"
                                                                               name="beneficiaryAccountType"
                                                                               value="Transmission">&nbsp;<span>Transmission</span>
                                            </div>

                                        </div>
                                        <div class="row">

                                    </div>
                                </div>
                                </div>
                            </div>
                            <div id="beneficiary" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                        <div class="form-group col-md-4 ">
                                            {{-- <p> <a href="{{URL::to('/')}}/{{$claims->deceasedIdCopyPath}}" > --}}
                                                    <i class="fa fa-download"></i>
                                                </a>Deceased Id Copy

                                            </p>
                                         </div>
                                            <div class="form-group col-md-4 ">
                                                <p><a href="{{ url('') }}" >
                                                        <i class="fa fa-download"></i>
                                                    </a>Claimant Id Copy

                                                </p>
                                             </div>

                                            <div class="form-group col-md-4 ">
                                                <p><a href="{{ url('') }}" >
                                                        <i class="fa fa-download"></i>
                                                    </a>Bank Statement Copy

                                                </p>
                                            </div>

                                        </div>

                                          <div class="row">

                                            <div class="form-group col-md-4 ">
                                                <p><a href="{{ url('') }}" >
                                                        <i class="fa fa-download"></i>
                                                    </a>Police Report Copy

                                                </p>

                                            </div>
                                              <div class="form-group col-md-6 ">
                                                  <p><a href="{{ url('') }}" >
                                                          <i class="fa fa-download"></i>
                                                      </a>Certified BI Copy

                                                  </p>
                                              </div>
                                        </div>

                                        <div style="text-align: right">
                                            <button type="submit" class="btn btn-primary">Approve Claim</button>
                                        </div>


                                    </div>


                                </div>
                            </div>

                            </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
