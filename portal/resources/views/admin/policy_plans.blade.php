@extends('layouts.portalMaster')

@section('content')
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Policy Plans</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Policy Plans</li>
                        </ul>
                    </div>
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{Session::get('success')}}
                        </div>
                    @endif
                    <div class="col-auto text-right">
                        <a class="btn btn-light filter-btn" href="{{ url('clear-policy-search') }}" data-toggle="tooltip" title="Clear Search">
                            <i class="fas fa-times"></i>
                        </a>
                        <a class="btn btn-white filter-btn" href="javascript:void(0);" id="filter_search" data-toggle="tooltip" title="Search Policy Plan">
                            <i class="fas fa-filter"></i>
                        </a>
                        <a href="#policy_plans"  data-toggle="modal" class="btn btn-primary add-button ml-3" title="Upload Package">
                            <i class="fas fa-plus"></i>
                        </a>
                    </div>
                </div>
            </div>


            <div class="card filter-card" id="filter_inputs">
                <div class="card-body pb-0">
                    <form method="POST" action="{{ route('policy-filter')}}"  enctype="multipart/form-data"/>
                    @CSRF
                    <div class="row filter-row">
                        <div class="col-sm-6 col-md-5">
                            <div class="form-group">
                                <label class="col-form-label">From Date</label>
                                {{--                                    <div class="cal-icon">--}}
                                <input class="form-control datetimepicker" name="startDate" type="date">
                                {{--                                    </div>--}}
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-5">
                            <div class="form-group">
                                <label class="col-form-label">To Date</label>
                                {{--                                    <div class="cal-icon">--}}
                                <input class="form-control datetimepicker" name="endDate" type="date">
                                {{--                                    </div>--}}
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-2">

                            <div class="form-group">
                                <button class="btn btn-primary btn-block" type="submit">Submit</button>
                            </div>
                        </div>
                    </div>
                    </form>





                </div>
            </div>

            <ul class="nav nav-tabs menu-tabs">

                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#standard">Standard Policy Plans  <span class="badge badge-primary">{{$total_standard_plans}}</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#societal">Society Policy Plans <span class="badge badge-primary">{{$total_societal_plans}}</span></a>
                </li>

            </ul>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                            <div class="tab-content profile-tab-cont">
                                <div class="table-responsive tab-pane show active" id="standard">
                                    <table class="table table-hover table-center mb-0 datatable">
                                        <thead>
                                        <tr>

{{--                                            <th>PlanType</th>--}}
                                            <th>MaritalStatus</th>
{{--                                            <th>PlanName</th>--}}
                                            <th>AgeRange</th>
{{--                                            <th>GroupSize</th>--}}
                                            <th>Cover</th>
                                            <th>Premium</th>
                                            <th>EntryFee</th>

                                            <th style="text-align:center">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($standard_plans as $standard)
                                            <tr>

                                                <td>{{ $standard->maritalPlanStatus}}</td>
{{--                                                <td>{{ $standard->planName }}</td>--}}
                                                <td>{{ $standard->ageRange }}</td>
{{--                                                <td>{{ $standard->groupSizeRange }}</td>--}}
                                                <td>{{ $standard->coverAmount }}</td>
                                                <td>{{ $standard->premium }}</td>
                                                <td>{{ $standard->entryFee }}</td>

                                                <td>

                                                    <a   data-toggle="modal" data-target="#view_policy_plan-{{$standard->id}}"    class="btn btn-info view">
                                                        <i class="far fa-edit mr-1"></i>
                                                    </a>
                                                    <a href="{{ url('admin/delete_plan/'.$standard->id) }}" class="btn btn-danger">
                                                        <i class="far fa-trash-alt"></i>
                                                    </a>
                                                </td>
                                            </tr>

                                            <div id="view_policy_plan-{{$standard->id}}" class="modal fade" tabindex="-1">
                                                <div class="modal-dialog" >
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <div class="avatar">
                                                                <img src="{{asset('public/frontend/img/atlantic_logo.png')}}" alt="" class="img-fluid">
                                                            </div>
                                                            <h4 class="modal-title">Update Policy Plan</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form class="p-3" method="POST" action="{{ url('/admin/update_plan') }}" >
                                                                @csrf
                                                                <div class="row">
                                                                    <div class="form-group col">
                                                                        <input type="hidden" name="id" value="{{$standard->id}}">
                                                                        <label>Plan Type:</label>
                                                                        <input type="text" name="planType" value="{{$standard->planType}}" class="form-control w-100" readonly>
{{--                                                                        <select class="form-control" name="planType"  id="seeAnotherFieldGroup" readonly>--}}
{{--                                                                            <option value="{{$standard->planType}}">{{$standard->planType}}</option>--}}
{{--                                                                            <option value="Standard Policy">Standard Policy</option>--}}
{{--                                                                            <option value="Societal Policy">Societal Policy</option>--}}
{{--                                                                        </select>--}}
                                                                    </div>
                                                                    <div class="form-group col">
                                                                        <label>Marital Status:</label>
                                                                        <select name="maritalPlanStatus"  class="form-control" required>
                                                                            <option value="{{$standard->maritalPlanStatus}}">{{$standard->maritalPlanStatus}}</option>
                                                                            <option value="Single">Single</option>
                                                                            <option value="Married">Married</option>
                                                                        </select>
                                                                    </div>

                                                                </div>


                                                               @if($standard->planType=='Societal Policy')
                                                                <div class="form-group" id="otherFieldGroupDiv" show>
                                                                    <div class="row">
                                                                        <div class="col">

                                                                            <input type="text" name="planName" class="form-control w-100" id="otherField1" placeholder="Plan Name eg: Plan A">
                                                                        </div>
                                                                        <div class="col">

                                                                            <input type="text" name="groupSizeRange" class="form-control w-100" id="otherField2" placeholder="Group Size eg: 1-5">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @endif


                                                                <div class="row">
                                                                    <div class="form-group col">
                                                                        <label>Age Range:</label>
                                                                        <input  name="ageRange"  class="form-control" required value="{{$standard->ageRange}}"  placeholder="Age range eg: 18-64" >
                                                                    </div>

                                                                    <div class="form-group col">
                                                                        <label>Cover Amount:</label>
                                                                        <input  name="coverAmount"  value="{{$standard->coverAmount}}" class="form-control" required  placeholder="Cover Amount" >
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-group col">
                                                                        <label>Premium:</label>
                                                                        <input  name="premium" value="{{$standard->premium}}"  class="form-control" required  placeholder="Premium" >
                                                                    </div>
                                                                    <div class="form-group col">
                                                                        <label>Entry Fee:</label>
                                                                        <input  name="entryFee" value="{{$standard->entryFee}}"  class="form-control" required  placeholder="Entry Fee" >
                                                                    </div>

                                                                </div>





                                                                <div class="modal-footer">
                                                                    <div class="book-now-btn">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                        <button type="submit" class="btn btn-primary">Update Plan</button>
                                                                    </div>
                                                                </div>
                                                            </form>


                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach

                                        </tbody>

                                    </table>
                                    {!! $standard_plans->links() !!}

                                </div>

                                <div class="table-responsive tab-pane fade" id="societal">
                                    <table class="table table-hover table-center mb-0 datatable">
                                        <thead>
                                        <tr>
{{--                                            <th>PlanType</th>--}}
                                            <th>MaritalStatus</th>
                                            <th>PlanName</th>
                                            <th>AgeRange</th>
                                            <th>GroupSize</th>
                                            <th>Cover</th>
                                            <th>Premium</th>
                                            <th>EntryFee</th>

                                            <th style="text-align:center">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($societal_plans as $societal)
                                            <tr>
{{--                                                <td>{{ $societal->id}}</td>--}}

{{--                                                <td>{{ $societal->planType }}</td>--}}
                                                <td>{{ $societal->maritalPlanStatus}}</td>
                                                <td>{{ $societal->planName }}</td>
                                                <td>{{ $societal->ageRange }}</td>
                                                <td>{{ $societal->groupSizeRange }}</td>
                                                <td>{{ $societal->coverAmount }}</td>
                                                <td>{{ $societal->premium }}</td>
                                                <td>{{ $societal->entryFee }}</td>

                                                <td>
                                                    <a data-toggle="modal" data-target="#view_policy_plan-{{$societal->id}}" class="btn btn-info">
                                                        <i class="far fa-eye mr-1"></i>
                                                    </a>
                                                    {{--                                                                                                <a href="{{ url('admin/edit/'.$standard->id) }}" class="btn btn-primary">--}}
                                                    {{--                                                                                                    <i class="far fa-edit mr-1"></i> Edit--}}
                                                    {{--                                                                                                </a>--}}
                                                    <a href="{{ url('admin/delete_plan/'.$societal->id) }}" class="btn btn-danger">
                                                        <i class="far fa-trash-alt"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <div id="view_policy_plan-{{$societal->id}}" class="modal fade" tabindex="-1">
                                                <div class="modal-dialog" >
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <div class="avatar">
                                                                <img src="{{asset('public/frontend/img/atlantic_logo.png')}}" alt="" class="img-fluid">
                                                            </div>
                                                            <h4 class="modal-title">Update Policy Plan</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form class="p-3" method="POST" action="{{ url('/admin/update_plan') }}" >
                                                                @csrf
                                                                <div class="row">
                                                                    <div class="form-group col">
                                                                        <input type="hidden" name="id" value="{{$societal->id}}">
                                                                        <label>Policy Type:</label>
                                                                        <input type="text" name="planType" value="{{$societal->planType}}" class="form-control w-100" readonly>

{{--                                                                        <select class="form-control" name="planType"  id="seeAnotherFieldGroup" hidden>--}}
{{--                                                                            <option value="{{$societal->planType}}">{{$societal->planType}}</option>--}}
{{--                                                                            <option value="Standard Policy">Standard Policy</option>--}}
{{--                                                                            <option value="Societal Policy">Societal Policy</option>--}}
{{--                                                                        </select>--}}
                                                                    </div>
                                                                    <div class="form-group col">
                                                                        <label>Marital Status:</label>
                                                                        <select name="maritalPlanStatus"  class="form-control" required>
                                                                            <option value="{{$societal->maritalPlanStatus}}">{{$societal->maritalPlanStatus}}</option>
                                                                            <option value="Single">Single</option>
                                                                            <option value="Married">Married</option>
                                                                        </select>
                                                                    </div>

                                                                </div>



                                                                    <div class="form-group" id="otherFieldGroupDiv" show>
                                                                        <div class="row">
                                                                            <div class="col">
                                                                                <label>Plan Type:</label>
                                                                                <input type="text" name="planName"  value="{{$societal->planName}}" class="form-control w-100" id="otherField1" placeholder="Plan Name eg: Plan A">
                                                                            </div>
                                                                            <div class="col">
                                                                                <label>Group Size:</label>
                                                                                <input type="text" name="groupSizeRange" value="{{$societal->groupSizeRange}}" class="form-control w-100" id="otherField2" placeholder="Group Size eg: 1-5">
                                                                            </div>
                                                                        </div>
                                                                    </div>



                                                                <div class="row">
                                                                    <div class="form-group col">
                                                                        <label>Age Range:</label>
                                                                        <input  name="ageRange"  class="form-control" required value="{{$societal->ageRange}}"  placeholder="Age range eg: 18-64" >
                                                                    </div>

                                                                    <div class="form-group col">
                                                                        <label>Cover Amount:</label>
                                                                        <input  name="coverAmount"  value="{{$societal->coverAmount}}" class="form-control" required  placeholder="Cover Amount" >
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-group col">
                                                                        <label>Premium:</label>
                                                                        <input  name="premium" value="{{$societal->premium}}"  class="form-control" required  placeholder="Premium" >
                                                                    </div>
                                                                    <div class="form-group col">
                                                                        <label>Entry Fee:</label>
                                                                        <input  name="entryFee" value="{{$societal->entryFee}}"  class="form-control" required  placeholder="Entry Fee" >
                                                                    </div>

                                                                </div>





                                                                <div class="modal-footer">
                                                                    <div class="book-now-btn">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                        <button type="submit" class="btn btn-primary">Update Plan</button>
                                                                    </div>
                                                                </div>
                                                            </form>


                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
{{--                                        {!! $societal->links() !!}--}}
                                        </tbody>

                                    </table>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

{{--    <script>--}}
{{--        $(document).on('click', '.view', function(e) {--}}

{{--            e.preventDefault();--}}

{{--            // const id = $(this).data('id');--}}
{{--            const url = $(this).data('url');--}}

{{--           // $('#deleteFormClient').attr('action', url);--}}
{{--            alert(url);--}}
{{--            --}}{{--$('#deleteFormClient').attr('action', '{{ route(destroy.clients, '+ id + ')}}');--}}
{{--        })--}}
{{--    </script>--}}
@endsection
