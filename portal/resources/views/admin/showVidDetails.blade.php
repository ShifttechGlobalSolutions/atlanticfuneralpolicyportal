@extends('layouts.portalMaster')

@section('content')
    <div class="page-wrapper">
        <div class="content container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Video Details</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ url('admin/tutorials') }}">Video Tutorials</a></li>
                            <li class="breadcrumb-item active">Edit Tutorials</li>
                        </ul>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="card" style="width: 100%">
                    <div class="card-body">
                        <div class="tab-content profile-tab-cont">
                            <div class="table-responsive tab-pane show active" id="all_polices">
                                <div class="row align-items-start">
                                <div class="col">
                                    @foreach($video as $v)
                                        <div style="text-align:center; margin-left: 6%">
                                            <video  controls width="450" height="350" controlsList="nodownload">
                                                <source src="{{ asset('storage/app/public/video/'.$v->video) }}" type="video/mp4">
                                            </video><br><br>
                                        </div>
                                </div>
                                <div class="col">
                                    <div style="text-align:left; margin-left: 6%">
                                        <br><br><h3>{{ $v->title }}</h3><br><hr>
                                        <p>{{ $v->description }}</p><hr><br>
                                            <a href="" class="btn btn-primary" data-toggle="modal" data-target="#updateVid">Edit Video</a>
                                        </div>
                                    @endforeach
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    <!-- Modal -->
    <div class="modal fade" id="updateVid" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #0C0457;color:white">
                    <h5 class="modal-title" id="exampleModalLabel">Add Update Video</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="background-color: #ffffff">
                    @foreach($video as $vid)
                    <form method="post" action="{{ route('admin/update-video') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="hidden" value="{{ $vid->id }}" class="form-control" id="id" name="id" placeholder="Video Title" style="background-color: #F5F5F5;color: black">
                            <input type="text" value="{{ $vid->title }}" class="form-control" id="title" name="title" placeholder="Video Title" style="background-color: #F5F5F5;color: black">
                        </div>

                        <div class="form-group">
                            <label for="file">Video</label>
                            <input type="file" class="form-control" id="file" name="file" value="{{ $vid->video }}"  style="background-color: #F5F5F5;color: black">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description" id="description" placeholder="Video Description">{{ $vid->description }}</textarea>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @endsection
