@extends('layouts.portalMaster')
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet" href="{{asset('public/frontend/css/bootstrap.min.css')}}">

<link rel="stylesheet" href="{{asset('public/frontend/css/animate.min.css')}}">

<link rel="stylesheet" href="{{asset('public/frontend/css/fontawesome.min.css')}}">

<link rel="stylesheet" href="{{asset('public/frontend/css/flaticon.css')}}">

<link rel="stylesheet" href="{{asset('public/frontend/css/owl.carousel.min.css')}}">

<link rel="stylesheet" href="{{asset('public/frontend/css/slick.min.css')}}">

<link rel="stylesheet" href="{{asset('public/frontend/css/meanmenu.css')}}">

<link rel="stylesheet" href="{{asset('public/frontend/css/magnific-popup.min.css')}}">

<link rel="stylesheet" href="{{asset('public/frontend/css/odometer.min.css')}}">

<link rel="stylesheet" href="{{asset('public/frontend/css/nice-select.min.css')}}">

<link rel="stylesheet" href="{{asset('public/frontend/css/style.css')}}">

<link rel="stylesheet" href="{{asset('public/frontend/css/responsive.css')}}">
<title>Atlantic Funerals - Funeral Policy Company</title>
<link rel="icon" type="image/png" href="{{asset('public/frontend/img/atlantic_logo.png')}}">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<style>

    .modal-login {
        color: #636363;
        width: 650px;
    }
    .modal-login .modal-content {
        padding: 20px;
        border-radius: 5px;
        border: none;
    }
    .modal-login .modal-header {
        border-bottom: none;
        position: relative;
        justify-content: center;
    }
    .modal-login h4 {
        text-align: center;
        font-size: 26px;
        margin: 30px 0 -15px;
    }
    .modal-login .form-control:focus {
        border-color: #70c5c0;
    }
    .modal-login .form-control, .modal-login .btn {
        min-height: 40px;
        border-radius: 3px;
    }
    .modal-login .close {
        position: absolute;
        top: -5px;
        right: -5px;
    }
    .modal-login .modal-footer {
        background: #ecf0f1;
        border-color: #dee4e7;
        text-align: center;
        justify-content: center;
        margin: 0 -20px -20px;
        border-radius: 5px;
        font-size: 13px;
    }
    .modal-login .modal-footer a {
        color: #999;
    }
    .modal-login .avatar {
        position: absolute;
        margin: 0 auto;
        left: 0;
        right: 0;
        top: -70px;
        width: 95px;
        height: 95px;
        border-radius: 50%;
        z-index: 9;
        background: #60c7c1;
        padding: 15px;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
    }
    .modal-login .avatar img {
        width: 100%;
    }
    .modal-login.modal-dialog {
        margin-top: 80px;
    }
    .modal-login .btn, .modal-login .btn:active {
        color: #fff;
        border-radius: 4px;
        background: #60c7c1 !important;
        text-decoration: none;
        transition: all 0.4s;
        line-height: normal;
        border: none;
    }
    .modal-login .btn:hover, .modal-login .btn:focus {
        background: #45aba6 !important;
        outline: none;
    }
    .trigger-btn {
        display: inline-block;
        margin: 100px auto;
    }

    #regForm {
        background-color: #ffffff;
        margin: 100px auto;
        font-family: Raleway;
        padding: 40px;
        width: 70%;
        min-width: 300px;
    }
    h1 {
        text-align: center;
    }
    input {
        padding: 10px;
        width: 100%;
        font-size: 17px;
        font-family: Raleway;
        border: 1px solid #aaaaaa;
    }
    /* Mark input boxes that gets an error on validation: */
    input.invalid {
        background-color: #ffdddd;
    }
    /* Hide all steps by default: */
    .tab {
        display: none;
    }
    button {
        background-color: #04AA6D;
        color: #ffffff;
        border: none;
        padding: 10px 20px;
        font-size: 17px;
        font-family: Raleway;
        cursor: pointer;
    }
    button:hover {
        opacity: 0.8;
    }
    #prevBtn {
        background-color: #bbbbbb;
    }
    /* Make circles that indicate the steps of the form: */
    .step {
        height: 15px;
        width: 15px;
        margin: 0 2px;
        background-color: #bbbbbb;
        border: none;
        border-radius: 50%;
        display: inline-block;
        opacity: 0.5;
    }
    .step.active {
        opacity: 1;
    }
    /* Mark the steps that are finished and valid: */
    .step.finish {
        background-color: #04AA6D;
    }
</style>
</head>

@section('content')

    @foreach($member as $mem)
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Edit Member</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('admin/policies') }}"> Back</a>
                    </div>
                </div>
            </div><br>

            <div class="row">
                <div class="col-sm-6">
                    <div class="single-services-box">

                        <form id="regForm" method="POST" action="{{ route('admin-mail')}}"  enctype="multipart/form-data" >
                        @CSRF
                        <!-- One "tab" for each step in the form: -->
                            <div class="tab"><h6>Policy Holder Details:</h6>
                                <div class="row">
                                    <div class="form-group col">

                                        <input id="firstName" class="form-control " type="text"  name="fname"  required  value="{{ $mem->client_firstname }}" placeholder=" Firstname">


                                    </div>
                                    <div class="form-group col">
                                        <input id="lastName" class="form-control " type="text"  name="lname"  required value="{{ $mem->client_lastname }}"  placeholder=" Lastname">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col">
                                        <input id="dob1" name="dob1" type="date" class="form-control" required value="{{ $mem->client_dob }}"  >
                                    </div>
                                    <div class="form-group col">
                                        <input id="memID" name="memID" class="form-control " type="text" value="{{ $mem->client_id_number}}"  required   placeholder="ID. Number">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="custom-file">
                                        <input name="idCopy" type="file" class="form-control" id="customFile" placeholder="Copy of ID" value="{{ $mem->client_idCopy }}">
                                    </div>
                                </div><br>

                                <div class="row">
                                    <div class="form-group col">
                                        <input id="strName" name="strName" class="form-control" type="text" required value="{{ $mem->strName }}"  placeholder="Street Name">
                                    </div>
                                    <div class="form-group col">
                                        <input id="surburb" name="surburb" class="form-control " type="text"    required value="{{ $mem->surburb }}"  placeholder="Surburb">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col">
                                        <input id="pcode" name="pcode" class="form-control " type="text"  required  value="{{ $mem->postalCode }}" placeholder="Postal Code">
                                    </div>
                                    <div class="form-group col">
                                        <input id="home" name="homeContact" class="form-control " placeholder="Home: (000) 000 0000" required value="{{ $mem->home }}" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col">
                                        <input id="work" name="workContact" class="form-control " type="text"  required value="{{ $mem->work }}"  placeholder="Work: (000) 000 0000">
                                    </div>
                                    <div class="form-group col">
                                        <input id="cell" name="cellContact" class="form-control " placeholder="cell: (000) 000 0000" required value="{{ $mem->cell }}"  >
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col">
                                        <input id="email" name="emailContact" placeholder="example@gmail.com" required value="{{ $mem->email }}"  class="form-control">
                                    </div>
                                </div>

                                <div class="row" style="text-align: left">
                                    <div class="form-group col">
                                        <label for="email"><strong>Gender:</strong></label><br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="genderRadio" id="gridRadios1" value="Male" checked>&nbsp;<span>Male</span><br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="genderRadio" id="gridRadios2" value="Female">&nbsp;<span>Female</span>

                                    </div>
                                </div>
                            </div>
                            <div class="tab"><h6>Spouse Details:</h6>
                                <div class="row">
                                    <div class="form-group col">
                                        <input id="sfname" name="sfname" type="text" class="form-control" required   placeholder="Firstname">
                                    </div>
                                    <div class="form-group col">
                                        <input id="slname" name="slname" class="form-control " type="text"   required   placeholder="Lastname">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col">
                                        <input id="sId" name="sId" type="text" class="form-control" required   placeholder="ID. Number">
                                    </div>
                                    <div class="form-group col">
                                        <input id="sdob" name="sdob" class="form-control " type="date"   required  >
                                    </div>
                                </div>
                            </div>

                            <div class="tab"><h6>Dependants Details:</h6>
                                <div class="inc">
                                    <div class="row" style="text-align: left">
                                        <div class="col">
                                            <label>Last Name</label><br>
                                            <input  name="dep_fname" class="form-control">
                                        </div>
                                        <div class="col">
                                            <label>First Name</label><br>
                                            <input name="dep_lname"  class="form-control">
                                        </div>
                                    </div><br>

                                    <div class="row" style="text-align: left">
                                        <div class="col">
                                            <label>Date Of Birth</label><br>
                                            <input  type="date" name="dep_dob" class="form-control">
                                        </div>
                                        <div class="col">
                                            <label>Birth Certificate Copy</label><br>
                                            <input  type="file" name="depIdCody" id="depIdCody"  class="form-control">
                                        </div>
                                    </div><br>

                                    <hr>
                                </div>
                                <div class="row" style="text-align: right">
                                    <div class="col">
                                        <button  class="btn btn-info" type="submit" id="append" name="append"><i class="fa fa-plus" aria-hidden="true"></i>  Dependent</button>
                                    </div>
                                </div><br>
                            </div>

                            <div class="tab"><h6>Beneficiary Details:</h6>
                                <div class="row">
                                    <div class="form-group col">
                                        <input id="benfname" name="benfname" type="text" class="form-control" required   placeholder="Firstname">
                                    </div>
                                    <div class="form-group col">
                                        <input id="benlname" name="benlname" class="form-control " type="text"   required   placeholder="Lastname">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col">
                                        <input id="benId" name="benId" type="text" class="form-control" required   placeholder="ID. Number">
                                    </div>
                                    <div class="form-group col">
                                        <input id="bdob" name="bdob" class="form-control " type="date"   required  >
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col">
                                        <input id="benconNumber" name="benconNumber" required   placeholder="Call: (000) 000 0000" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="tab"><h6>Account Details:</h6>
                                <div class="row">
                                    <div class="form-group col">
                                        <input id="bankName" name="bankName" type="text" class="form-control" required   placeholder="Name of bank">
                                    </div>
                                    <div class="form-group col">
                                        <input id="branchName" name="branchName" class="form-control " type="text"   required   placeholder="Name of branch">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col">
                                        <input id="accNumber" name="accNumber" type="text" class="form-control" required   placeholder="Account number">
                                    </div>
                                    <div class="form-group col">
                                        <input id="branchCode" name="branchCode" class="form-control " type="text"   required   placeholder="Branch code
">
                                    </div>
                                </div>

                                <div class="row" style="text-align: left">
                                    <div class="form-group col">
                                        <label for="email"><strong>Account type:</strong></label><br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="accType" id="accType1" value="Current" checked>&nbsp;<span>Current (cheque)</span><br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="accType" id="accType2" value="Savings">&nbsp;<span>Savings</span><br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="accType" id="accType3" value="Transmission">&nbsp;<span>Transmission</span>
                                    </div>

                                </div>
                            </div>

                            <div style="overflow:auto;">
                                <div style="float:right;">
                                    <button type="button" class="btn btn-secondary" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                                    <button type="button" class="btn btn-primary" id="nextBtn" onclick="nextPrev(1)">Next</button>
                                    <button type="submit" class="btn btn-primary"  >Submit</button>

                                </div>
                            </div>
                            <!-- Circles which indicates the steps of the form: -->
                            <div style="text-align:center;margin-top:40px;">
                                <span class="step"></span>
                                <span class="step"></span>
                                <span class="step"></span>
                                <span class="step"></span>
                            </div>
                        </form>
                        <div class="box-shape">
                            <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">
                            <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="{{asset('public/frontend/js/jquery.min.js')}}"></script>

    <script src="{{asset('public/frontend/js/popper.min.js')}}"></script>

    <script src="{{asset('public/frontend/js/bootstrap.min.js')}}"></script>

    <script src="{{asset('public/frontend/js/parallax.min.js')}}"></script>

    <script src="{{asset('public/frontend/js/owl.carousel.min.js')}}"></script>

    <script src="{{asset('public/frontend/js/slick.min.js')}}"></script>

    <script src="{{asset('public/frontend/js/jquery.meanmenu.js')}}"></script>

    <script src="{{asset('public/frontend/js/jquery.appear.min.js')}}"></script>

    <script src="{{asset('public/frontend/js/odometer.min.js')}}"></script>

    <script src="{{asset('public/frontend/js/jquery.nice-select.min.js')}}"></script>

    <script src="{{asset('public/frontend/js/jquery.magnific-popup.min.js')}}"></script>

    <script src="{{asset('public/frontend/js/wow.min.js')}}"></script>

    <script src="{{asset('public/frontend/js/jquery.ajaxchimp.min.js')}}"></script>

    <script src="{{asset('public/frontend/js/form-validator.min.js')}}"></script>

    <script src="{{asset('public/frontend/js/contact.blade.php-form-script.js')}}"></script>

    <script src="{{asset('public/frontend/js/main.js')}}"></script>

    <script>
        jQuery(document).ready( function () {
            $("#append").click( function(e) {
                e.preventDefault();
                $(".inc").append('<div class="controls">\
                <div class="row" style="text-align: left">\
                <div class="col">\
                <label>Last Name</label><br>\
            <input name="depfname" class="form-control">\
            </div>\
            <div class="col">\
                <label>First Name</label><br>\
                <input name="deplname"  class="form-control">\
            </div>\
        </div><br>\
        <div class="row" style="text-align: left">\
                    <div class="col">\
                        <label>Date Of Birth</label><br>\
                        <input  type="date" id="dep1dob"  class="form-control">\
                    </div>\
                    <div class="col">\
                        <label>Birth Certificate Copy</label><br>\
                        <input  type="file"  class="form-control">\
                    </div>\
                </div><br>\
                <hr>\
            </div>');
                return false;
            });

            jQuery(document).on('click', '.remove_this', function() {
                jQuery(this).parent().remove();
                return false;
            });
            $("input[type=submit]").click(function(e) {
                e.preventDefault();
                $(this).next("[name=textbox]")
                    .val(
                        $.map($(".inc :text"), function(el) {
                            return el.value
                        }).join(",\n")
                    )
            })
        });

        var currentTab = 0; // Current tab is set to be the first tab (0)
        showTab(currentTab); // Display the current tab
        function showTab(n) {
            // This function will display the specified tab of the form...
            var x = document.getElementsByClassName("tab");
            x[n].style.display = "block";
            //... and fix the Previous/Next buttons:
            if (n == 0) {
                document.getElementById("prevBtn").style.display = "none";
            } else {
                document.getElementById("prevBtn").style.display = "inline";
            }
            if (n == (x.length - 1)) {
                document.getElementById("nextBtn").innerHTML = "Submit";
            } else {
                document.getElementById("nextBtn").innerHTML = "Next";
            }
            //... and run a function that will display the correct step indicator:
            fixStepIndicator(n)
        }
        function nextPrev(n) {
            // This function will figure out which tab to display
            var x = document.getElementsByClassName("tab");
            // Exit the function if any field in the current tab is invalid:
            if (n == 1 && !validateForm()) return false;
            // Hide the current tab:
            x[currentTab].style.display = "none";
            // Increase or decrease the current tab by 1:
            currentTab = currentTab + n;
            // if you have reached the end of the form...
            if (currentTab >= x.length) {
                // ... the form gets submitted:
                document.getElementById("regForm").submit();
                return false;
            }
            // Otherwise, display the correct tab:
            showTab(currentTab);
        }
        function validateForm() {
            // This function deals with validation of the form fields
            var x, y, i, valid = true;
            x = document.getElementsByClassName("tab");
            y = x[currentTab].getElementsByTagName("input");
            // A loop that checks every input field in the current tab:
            for (i = 0; i < y.length; i++) {
                // If a field is empty...
                if (y[i].value == "") {
                    // add an "invalid" class to the field:
                    y[i].className += " invalid";
                    // and set the current valid status to false
                    valid = false;
                }
            }
            // If the valid status is true, mark the step as finished and valid:
            if (valid) {
                document.getElementsByClassName("step")[currentTab].className += " finish";
            }
            return valid; // return the valid status
        }
        function fixStepIndicator(n) {
            // This function removes the "active" class of all steps...
            var i, x = document.getElementsByClassName("step");
            for (i = 0; i < x.length; i++) {
                x[i].className = x[i].className.replace(" active", "");
            }
            //... and adds the "active" class on the current step:
            x[n].className += " active";
        }
    </script>
@endforeach
