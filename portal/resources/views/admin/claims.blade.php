@extends('layouts.portalMaster')

@section('content')
    <div class="page-wrapper">
        <div class="content container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Claims List</h3>
                    </div>
                    <div class="col-auto text-right">
                        <a class="btn btn-light filter-btn" href="{{ url('clear-claim-search') }}" data-toggle="tooltip" title="Clear Search">
                            <i class="fas fa-times"></i>
                        </a>
                        <a class="btn btn-white filter-btn" href="javascript:void(0);" id="filter_search" data-toggle="tooltip" title="Search Claim">
                            <i class="fas fa-filter"></i>
                        </a>

                    </div>
                </div>
            </div>


            <div class="card filter-card" id="filter_inputs">
                <div class="card-body pb-0">
                    <form method="POST" action="{{ route('claim-filter')}}"  enctype="multipart/form-data"/>
                    @CSRF
                        <div class="row filter-row">
                            <div class="col-sm-6 col-md-5">
                                <div class="form-group">
                                    <label class="col-form-label">From Date</label>
{{--                                    <div class="cal-icon">--}}
                                        <input class="form-control datetimepicker" name="startDate" type="date">
{{--                                    </div>--}}
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-5">
                                <div class="form-group">
                                    <label class="col-form-label">To Date</label>
{{--                                    <div class="cal-icon">--}}
                                        <input class="form-control datetimepicker" name="endDate" type="date">
{{--                                    </div>--}}
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-2">

                                <div class="form-group">
                                    <button class="btn btn-primary btn-block" type="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <ul class="nav nav-tabs menu-tabs">
                <li class="nav-item active">
                    <a class="nav-link" data-toggle="tab" href="#all_claims">All Claims <span class="badge badge-primary">{{ $allClaimsCount }}</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#approved_claims">Approved Claims <span class="badge badge-primary">{{ $active_claims }}</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#pending_claims">Pending Claims <span class="badge badge-primary">{{ $dormant_claims }}</span></a>
                </li>
{{--                <li class="nav-item">--}}
{{--                    <a class="nav-link" data-toggle="tab" href="#cancelled_claims">Cancelled Claims <span class="badge badge-primary">1</span></a>--}}
{{--                </li>--}}


            </ul>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content profile-tab-cont">
                              <div class="table-responsive tab-pane show active" id="all_claims">
                                <table class="table table-hover table-center mb-0 datatable">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Policy Number</th>
                                            <th>Policy Holder Name</th>
                                            <th>Policy Holder Email</th>

                                            <th>Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($allClaims as $claims)
                                        <tr>
                                            <td> {{ $claims->id }}</td>
                                            <td> {{ $claims->policyHolderId }}</td>
                                            <td>
                                                {{ $claims->policyHolderFullName }}
                                            </td>
                                            <td> {{ $claims->policyHolderEmail }}</td>

                                            <td>{{ $claims->created_at }}</td>

                                            <td>
                                                {{ $claims->status }}
                                            </td>
                                            <td>
                                                <a href="{{ url('admin/claims/view-claims/'.$claims->id) }}" class="btn btn-sm bg-info-light">
                                                    <i class="far fa-eye mr-1"></i> View
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach

                                        </tbody>
                                        {{-- {!! $allClaims->links() !!} --}}
                                    </table>
                              </div>


                                {{-- Approved Tab --}}

                                <div class="table-responsive tab-pane show active" id="approved_claims">
                                    <table class="table table-hover table-center mb-0 datatable">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Policy Number</th>
                                            <th>Policy Holder Name</th>
                                            <th>Policy Holder Email</th>

                                            <th>Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($active_claims1 as $claims1)
                                        <tr>
                                            <td> {{ $claims1->id }}</td>
                                            <td> {{ $claims1->policyHolderId }}</td>
                                            <td>
                                                {{ $claims1->policyHolderFullName }}
                                            </td>
                                            <td> {{ $claims1->policyHolderEmail }}</td>

                                            <td>{{ $claims1->created_at }}</td>

                                            <td>
                                                <div class="status-toggle">
                                                    <input id="service_1" class="check" type="checkbox">
                                                    <label for="service_1" class="checktoggle">checkbox</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="{{ url('admin/claims/view-claims/'.$claims1->id) }}" class="btn btn-sm bg-info-light">
                                                    <i class="far fa-eye mr-1"></i> View
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach

                                        </tbody>
                                        {{-- {!! $active_claims1->links() !!} --}}
                                    </table>
                                </div>


                                {{-- Pending Tab --}}

                                <div class="table-responsive tab-pane show active" id="pending_claims">
                                    <table class="table table-hover table-center mb-0 datatable">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Policy Number</th>
                                            <th>Policy Holder Name</th>
                                            <th>Policy Holder Email</th>

                                            <th>Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($dormant_claims1 as $claims2)
                                        <tr>
                                            <td> {{ $claims2->id }}</td>
                                            <td> {{ $claims2->policyHolderId }}</td>
                                            <td>
                                                {{ $claims2->policyHolderFullName }}
                                            </td>
                                            <td> {{ $claims2->policyHolderEmail }}</td>

                                            <td>{{ $claims2->created_at }}</td>

                                            <td>
                                                <div class="status-toggle">
                                                    <input id="service_1" class="check" type="checkbox">
                                                    <label for="service_1" class="checktoggle">checkbox</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="{{ url('admin/claims/view-claims/'.$claims2->id) }}" class="btn btn-sm bg-info-light">
                                                    <i class="far fa-eye mr-1"></i> View
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach

                                        </tbody>
                                        {{-- {!! $dormant_claims1->links() !!} --}}
                                    </table>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
