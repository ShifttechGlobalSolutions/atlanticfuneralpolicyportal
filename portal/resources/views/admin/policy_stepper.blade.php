@extends('layouts.portalMaster')

@section('content')


    <div class="page-wrapper">
        <div class="content container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">New Policy</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{url('admin/policies')}}">Policy</a></li>
                            <li class="breadcrumb-item active">New Policy</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <div class="profile-menu">
                        <ul class="nav nav-tabs nav-tabs-solid">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#policy">Policy Holder Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#package">Package Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#spouse">Spouse Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#dependants">Dependants Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#beneficiary">Beneficiary Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#account">Account Details</a>
                            </li>

                        </ul>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                    </div>
                    <form  method="POST" action="{{ route('admin-mail')}}"  enctype="multipart/form-data" >
                        @CSRF
                        <div class="tab-content profile-tab-cont">
{{--                            <div class="tab-content">--}}
                            <div id="policy" class="tab-pane fade show active" >
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Policy Holder Details</h5>
                                        <div class="row">
                                            <div class="col-md-6 ">

                                                <div class="form-group">
                                                    <label>FirstName</label>
                                                    <input class="form-control " type="text"  name="fname">

                                                    @error('client_firstname')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Date of Birth</label>
                                                    <input name="dob1" type="date" class="form-control">
                                                     @error('client_dob')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label>LastName</label>
                                                    <input class="form-control " type="text"  name="lname">
                                                     @error('client_lastname')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>ID Number</label>
                                                    <input name="memID" class="form-control " type="text"   >
                                                     @error('client_id_number')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12 ">
                                                <div class="form-group">
                                                    <label>Copy of ID</label>
                                                    <input name="idCopy" type="file" class="form-control" >
                                                     @error('image')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label>Address</label>
                                                    <input name="strName" class="form-control" type="text" >
                                                     @error('strName')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Postal Code</label>
                                                    <input name="pcode" class="form-control " type="text"  >
                                                     @error('postalCode')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror

                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label>Suburb</label>
                                                    <input name="surburb" class="form-control " type="text">
                                                     @error('surburb')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Tel Number (Home)</label>
                                                    <input name="homeContact" class="form-control ">
                                                     @error('home')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label>Tel Number (Work)</label>
                                                    <input name="workContact" class="form-control " type="text">
                                                     @error('work')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Email Address</label>
                                                    <input name="emailContact"    class="form-control">
                                                     @error('email')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>

                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label>Cellphone</label>
                                                    <input name="cellContact" class="form-control">
                                                     @error('cell')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label for="email"><strong>Gender:</strong></label><br>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="genderRadio" id="gridRadios1" value="Male" checked>&nbsp;<span>Male</span><br>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="genderRadio" id="gridRadios2" value="Female">&nbsp;<span>Female</span>
                                                     @error('gender')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                            </div>

                                        </div>

                                        <div style="text-align: left">
                                            <button data-toggle="tab" href="#package" class="btn btn-primary"  >Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div id="package" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title" id="modal_body"></h5>
                                        <div class="row">


                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label>Package</label>
                                                     <input type="hidden" name="agent"  value="Admin"  class="form-control">
                                                    <select name="package" class="form-control" >
                                                        <option>Select Package</option>
                                                        <option value="Basic">Basic</option>
                                                        <option value="Executive">Executive</option>
                                                    </select>
                                                     @error('package')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Age Group</label>
                                                    {{-- <input id="modal_body1" class="form-control " type="hidden"  name="agent"> --}}
                                                        <select name="ageGroup" class="form-control" >
                                                            <option>Select Age Group</option>
                                                            <option value="14 - 64 yrs">14 - 64 yrs</option>
                                                            <option value="65 - 74 yrs">65 - 74 yrs</option>
                                                            <option value="75 - 85 yrs">75 - 85  yrs</option>
                                                        </select>
                                                         @error('ageGroup')
                                                            <span class="text-danger">{{ $message }}</span>
                                                        @enderror
                                                </div>

                                            </div>

                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label>Cover Amount</label>
                                                    <select name="coverAmount" class="form-control" >
                                                        <option>Select Cover Amount</option>
                                                        <option value="5000">R 5 000</option>
                                                        <option value="7000">R 7 000</option>
                                                        <option value="10000">R 10 000</option>
                                                        <option value="13000">R 13 000</option>
                                                    </select>
                                                     @error('coverAmount')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>

                                                <div class="form-group">
                                                    <label for="email"><strong>Status:</strong></label><br>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="marital" id="marital1" value="Single" checked>&nbsp;<span>Single</span><br>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="marital" id="marital2" value="Married">&nbsp;<span>Married</span>
                                                     @error('marital')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>

                                        </div>
                                        <BR>
                                        <div style="text-align: left">
                                            <button data-toggle="tab" href="#spouse" class="btn btn-primary"  >Next</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="spouse" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Spouse Details</h5>
                                        <div class="row">
                                            <div class="col-md-6 ">

                                                <div class="form-group">
                                                    <label>FirstName</label>
                                                    <input name="sfname" type="text" class="form-control"  >
                                                     @error('spouse_firstname')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>ID Number</label>
                                                    <input name="sId" type="text" class="form-control" >
                                                     @error('spouse_idNumber')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label>LastName</label>
                                                        <input name="slname" class="form-control " type="text">
                                                         @error('spouse_lastname')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                    </div>
                                                    <label>Date of Birth</label>
                                                    <input name="sdob" class="form-control " type="date">
                                                     @error('spouse_dob')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
{{--                                                <div class="form-group">--}}
{{--                                                    <label>Contact Number</label>--}}
{{--                                                    <input type="tel" name="claimantContactNumber" class="form-control">--}}
{{--                                                </div>--}}
                                            </div>
{{--                                            <div class="col-md-6 ">--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label>Email Address</label>--}}
{{--                                                    <input type="email" name="claimantEmail" class="form-control">--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label>ID Number</label>--}}
{{--                                                    <input type="text" name="claimantIdNumber" class="form-control">--}}
{{--                                                </div>--}}


{{--                                            </div>--}}
{{--                                            <div class="col-md-6 ">--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label>Relationship</label>--}}
{{--                                                    <input type="text" name="claimantRelationship" class="form-control">--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label>Postal Address</label>--}}
{{--                                                    <input type="text" name="claimantPostalAddress" class="form-control">--}}
{{--                                                </div>--}}

{{--                                            </div>--}}
{{--                                            <div class="col-md-6 ">--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label>Residential Address</label>--}}
{{--                                                    <input type="text" name="claimantResidentialAddress" class="form-control">--}}
{{--                                                </div>--}}

{{--                                                <div class=" form-group ">--}}
{{--                                                    <label>Claimant Copy of Id</label>--}}
{{--                                                    <input name="claimantIdCopy" type="file" class="form-control"  >--}}
{{--                                                </div>--}}


{{--                                            </div>--}}

{{--                                            <div class="col-md-6">--}}
{{--                                                <div class=" form-group">--}}
{{--                                                    <label>Police Report Copy</label>--}}
{{--                                                    <input name="policeReportCopy" type="file" class="form-control" >--}}
{{--                                                </div>--}}
{{--                                            </div>--}}





                                        </div>

                                        <div style="text-align: left">
                                            <button data-toggle="tab" href="#dependants" class="btn btn-primary"  >Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="dependants" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Dependant(s) Options</h5>
                                        <div class="inc">
                                        <div class="row">
                                            <div class="form-group col-md-6 ">
                                                <label>FirstName</label>
                                                <input  name="dep_fname" class="form-control">
                                                 @error('dep_fname')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6 ">
                                                <label>LastName</label>
                                                <input name="dep_lname"  class="form-control">
                                                @error('dep_lname')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6 ">
                                                <label>Date Of Birth</label>
                                                <input  type="date" name="dep_dob" class="form-control">
                                                @error('dep_dob')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6 ">
                                                <label>Birth Certificate Copy</label>
                                                <input  type="file" name="depIdCody" id="depIdCody"  class="form-control">
                                                @error('dep_copiesofid')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <hr>
                                        </div>
                                    <div class="row" style="text-align: right">
                                        <div class="col">
                                            <button  class="btn btn-info" type="submit" id="append" name="append"><i class="fa fa-plus" aria-hidden="true"></i>  Dependent</button>
                                        </div>
                                    </div><br>
                                        <div>
                                            <div style="text-align: left">
                                                <button data-toggle="tab" href="#beneficiary" class="btn btn-primary"  >Next</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div id="beneficiary" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Beneficiary Details</h5>
                                        <div class="row">
                                            <div class="form-group col">
                                                <label>FirstName</label>
                                                <input name="benfname" type="text" class="form-control" >
                                                @error('ben_firstname')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col">
                                                <label>LastName</label>
                                                <input name="benlname" class="form-control " type="text"   >
                                                @error('ben_lastname')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col">
                                                <label>ID Number</label>
                                                <input id="benId" name="benId" type="text" class="form-control"  >
                                                @error('ben_idNumber')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col">
                                                <label>Data of Birth</label>
                                                <input id="bdob" name="bdob" class="form-control " type="date"     >
                                                @error('ben_dob')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col">
                                                <label>Contact Number</label>
                                                <input id="benconNumber" name="benconNumber" required class="form-control">
                                                @error('ben_contact_number')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
{{--                                            <div class="form-group col">--}}
{{--                                                <label>Beneficiary Branch Code</label>--}}
{{--                                                <input  name="beneficiaryBranchCode" class="form-control " type="text"    >--}}
{{--                                            </div>--}}
                                        </div>
                                        <br>
                                        <div>
                                            <div style="text-align: left">
                                                <button data-toggle="tab" href="#account" class="btn btn-primary"  >Next</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="account" class="tab-pane fade">
                                 <div class="card">
                                     <div class="card-body">
                                          <h5 class="card-title">Account Details</h5>
                                          <div class="row">
                                              <div class="form-group col">
                                                  <label>Bank Name</label>
                                                  <input name="bankName" type="text" class="form-control" >
                                                  @error('bankName')
                                                    <span class="text-danger">{{ $message }}</span>
                                                  @enderror
                                              </div>
                                              <div class="form-group col">
                                                  <label>Branch Name</label>
                                                  <input name="branchName" class="form-control " type="text"   >
                                                  @error('branchName')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="form-group col">
                                                   <label>Account Number</label>
                                                  <input name="accNumber" type="text" class="form-control"  >
                                                  @error('accNumber')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                              </div>
                                              <div class="form-group col">
                                                   <label>Branch code</label>
                                                  <input name="branchCode" class="form-control " type="text"   >
                                                  @error('branchCode')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                              </div>
                                          </div>

                                          <br>

                                        <label for="email"><strong>Account type:</strong></label>

                                        <div class="row" >
                                            <div class="form-group col-md-4">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="accType"   value="Current" checked>&nbsp;<span>Current (cheque)</span><br>
                                            </div>
                                            <div class="form-group col-md-4">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="accType"   value="Savings">&nbsp;<span>Savings</span><br>
                                            </div>
                                            <div class="form-group col-md-4">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="accType"   value="Transmission">&nbsp;<span>Transmission</span>
                                            </div>

                                        </div>
{{--                                        <div class="row" >--}}
{{--                                            <div class="custom-file col ">--}}
{{--                                                <label>Bank Statement Copy</label>--}}
{{--                                                <input name="bankStatementCopy" type="file" class="form-control" id="customFile">--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                         <br><br>
                                        <div style="text-align: left">
                                            <button type="submit" class="btn btn-primary"  >Submit Policy</button>
                                        </div>


                                    </div>
                                 </div>

                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
