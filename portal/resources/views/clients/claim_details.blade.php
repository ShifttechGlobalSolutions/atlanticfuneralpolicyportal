@extends('layouts.portalMaster')

@section('content')


    <div class="page-wrapper">
        <div class="content container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Claim Details</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Claim Details</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">

                    <div class="profile-menu">
                        <ul class="nav nav-tabs nav-tabs-solid">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#policy">Policy Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#deceased">Deceased Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#claimant">Claimant Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#payment">Payment Options</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#beneficiary">Nominated Beneficiary</a>
                            </li>


                        </ul>
                    </div>
                    <form  enctype="multipart/form-data" id="upload-file" >
                        @CSRF
                        <div class="tab-content profile-tab-cont">
                            @foreach ($claim_details as $claims)

                            <div id="policy" class="tab-pane fade show active">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Policy Holder Details</h5>
                                        <div class="row">
                                            <div class="col-md-4 ">

                                                <div class="form-group">
                                                    <label>Policy No</label>
                                                    <input type="text" name="policyHolderId" class="form-control" value="{{$claims->policyHolderId}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Surname</label>
                                                    <input type="text" name="policyHolderSurname" class="form-control" value="{{$claims->policyHolderSurname}}"readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>First Name</label>
                                                    <input type="text" name="policyHolderFullName" class="form-control" value="{{$claims->policyHolderFullName}}"readonly>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-md-4">

                                                <div class="form-group">
                                                    <label>Date of Birth</label>
                                                    <input type="text" name="policyHolderDateOfBirth"
                                                           class="form-control" value="{{$claims->policyHolderDateOfBirth}}"readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Email Address</label>
                                                    <input type="text" name="policyHolderEmail" class="form-control" value="{{$claims->policyHolderEmail}}"readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="tel" name="policyHolderContactNumber"
                                                           class="form-control" value="{{$claims->policyHolderContactNumber}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>ID Number/ Passport</label>
                                                    <input type="text" name="policyHolderIdNumber" class="form-control" value="{{$claims->policyHolderIdNumber}}"readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Postal Address</label>
                                                    <input type="text" name="policyHolderPostalAddress"
                                                           class="form-control" value="{{$claims->policyHolderPostalAddress}}">
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Residential Address</label>
                                                    <input type="text" name="policyHolderResidentialAddress"
                                                           class="form-control" value="{{$claims->policyHolderResidentialAddress}}">
                                                </div>

                                            </div>

                                        </div>

                                        <div style="text-align: right">
                                            <button data-toggle="tab" href="#deceased" class="btn btn-primary">Next
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="deceased" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Deceased Details</h5>
                                        <div class="row">


                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Surname</label>
                                                    <input type="text" name="deceasedSurname" class="form-control" value="{{$claims->deceasedSurname}}">
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>FullName</label>
                                                    <input type="text" name="deceasedFullName" class="form-control" value="{{$claims->deceasedFullName}}">
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="tel" name="deceasedContactNumber" class="form-control" value="{{$claims->deceasedContactNumber}}">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">


                                            <div class="col-md-4">

                                                <div class="form-group">
                                                    <label>Last Known Address</label>
                                                    <input type="text" name="deceasedLastAddress" class="form-control" value="{{$claims->deceasedLastAddress}}">
                                                </div>

                                            </div>

                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Date of Birth</label>
                                                    <input type="date" name="deceasedDateOfBirth" class="form-control" value="{{$claims->deceasedDateOfBirth}}">
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Cause of Death</label>
                                                    <input type="text" name="deceasedCauseOfDeath" class="form-control" value="{{$claims->deceasedCauseOfDeath}}">
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Death Certificate Serial No</label>
                                                    <input type="text" name="deceasedDeathCertificateSerialNo"
                                                           class="form-control" value="{{$claims->deceasedDeathCertificateSerialNo}}">
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>BI-1663 Serial No</label>
                                                    <input type="text" name="deceasedBiSerialNo" class="form-control" value="{{$claims->deceasedBiSerialNo}}">
                                                </div>

                                            </div>

                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Residential Address</label>
                                                    <input type="text" name="deceasedResidentialAddress"
                                                           class="form-control" value="{{$claims->deceasedResidentialAddress}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <div class="custom-file col">
                                                    <label>Deceased Copy of Id</label>
                                                    <input name="deceasedIdCopy" type="file" class="form-control"
                                                           id="deceasedIdCopy" value="{{$claims->deceasedIdCopy}}">
                                                    @error('deceasedIdCopy')
                                                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>


                                            <div class="col-md-6 ">

                                                <div class="custom-file ">
                                                    <label>Certified Copy of BI-1663/DHA-1663/BI-1680</label>
                                                    <input name="certifiedBICopy" type="file" class="form-control"
                                                           id="certifiedBICopy" value="{{$claims->certifiedBICopy}}">
                                                    @error('certifiedBICopy')
                                                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                                    @enderror
                                                </div>


                                            </div>


                                        </div>
                                        <BR>
                                        <div style="text-align: right">
                                            <button data-toggle="tab" href="#claimant" class="btn btn-primary">Next
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="claimant" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Claimant Details</h5>
                                        <div class="row">
                                            <div class="col-md-4 ">

                                                <div class="form-group">
                                                    <label>Surname</label>
                                                    <input type="text" name="claimantSurname" class="form-control" value="{{$claims->claimantSurname}}">
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" name="claimantFullName" class="form-control" value="{{$claims->claimantFullName}}">
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Date of Birth</label>
                                                    <input type="date" name="claimantDateOfBirth" class="form-control" value="{{$claims->claimantDateOfBirth}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="tel" name="claimantContactNumber"
                                                           class="form-control" value="{{$claims->claimantContactNumber}}">
                                                </div>

                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Email Address</label>
                                                    <input type="email" name="claimantEmail" class="form-control" value="{{$claims->claimantEmail}}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>ID Number</label>
                                                    <input type="text" name="claimantIdNumber" class="form-control" value="{{$claims->claimantIdNumber}}">
                                                </div>


                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Relationship</label>
                                                    <input type="text" name="claimantRelationship"
                                                           class="form-control" value="{{$claims->claimantRelationship}}">
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Postal Address</label>
                                                    <input type="text" name="claimantPostalAddress"
                                                           class="form-control" value="{{$claims->claimantPostalAddress}}">
                                                </div>

                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Residential Address</label>
                                                    <input type="text" name="claimantResidentialAddress"
                                                           class="form-control" value="{{$claims->claimantResidentialAddress}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 ">

                                                <div class=" form-group ">
                                                    <label>Claimant Copy of Id</label>
                                                    <input name="claimantIdCopy" type="file" class="form-control" value="{{$claims->claimantIdCopy}}">
                                                    @error('claimantIdCopy')
                                                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                                    @enderror
                                                </div>

                                            </div>

                                            <div class="col-md-6">
                                                <div class=" form-group">
                                                    <label>Police Report Copy</label>
                                                    <input name="policeReportCopy" type="file" class="form-control" value="{{$claims->policeReportCopy}}">
                                                    @error('policeReportCopy')
                                                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>




                                        <div style="text-align: right">
                                            <button data-toggle="tab" href="#payment" class="btn btn-primary">Next
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div id="payment" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Benefit Payment Options</h5>
                                        <div class="row">


                                            <div class="form-group col-md-6 ">
                                                <label>Parlour Name</label>
                                                <input type="text" name="funeralParlourName" class="form-control" value="{{$claims->funeralParlourName}}">
                                            </div>
                                            <div class="form-group col-md-6 ">
                                                <label>Contact Number</label>
                                                <input type="text" name="funeralParlourPhone" class="form-control" value="{{$claims->funeralParlourPhone}}">
                                            </div>
                                            <hr>

                                        </div>
                                        <div>
                                            <div style="text-align: right">
                                                <button data-toggle="tab" href="#beneficiary" class="btn btn-primary">
                                                    Next
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div id="beneficiary" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Nominated Beneficiary</h5>
                                        <div class="row">
                                            <div class="form-group col">
                                                <label>Beneficiary Name</label>
                                                <input name="beneficiaryName" type="text" class="form-control" value="{{$claims->beneficiaryName}}">
                                            </div>
                                            <div class="form-group col">
                                                <label>Beneficiary Id Number</label>
                                                <input name="beneficiaryIdNumber" type="text" class="form-control" value="{{$claims->beneficiaryIdNumber}}">
                                            </div>
                                            <div class="form-group col">
                                                <label>Beneficiary Bank Name</label>
                                                <input name="beneficiaryBankName" type="text" class="form-control" value="{{$claims->beneficiaryBankName}}">
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="form-group col">
                                                <label>Beneficiary Bank Branch</label>
                                                <input name="beneficiaryBranch" class="form-control " type="text" value="{{$claims->beneficiaryBranch}}">
                                            </div>
                                            <div class="form-group col">
                                                <label>Beneficiary Account Number</label>
                                                <input name="beneficiaryAccountNumber" type="text" class="form-control" value="{{$claims->beneficiaryAccountNumber}}">
                                            </div>
                                            <div class="form-group col">
                                                <label>Beneficiary Branch Code</label>
                                                <input name="beneficiaryBranchCode" class="form-control " type="text" value="{{$claims->beneficiaryBranchCode}}">
                                            </div>
                                        </div>


                                        <label for="email"><strong>Account type:</strong></label>

                                        <div class="row">
                                            <div class="form-group col-md-4">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio"
                                                                               name="beneficiaryAccountType"
                                                                               value="Current" checked>&nbsp;<span>Current (cheque)</span><br>
                                            </div>
                                            <div class="form-group col-md-4">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio"
                                                                               name="beneficiaryAccountType"
                                                                               value="Savings">&nbsp;<span>Savings</span><br>
                                            </div>
                                            <div class="form-group col-md-4">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio"
                                                                               name="beneficiaryAccountType"
                                                                               value="Transmission">&nbsp;<span>Transmission</span>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="custom-file col ">
                                                <label>Bank Statement Copy</label>
                                                <input name="bankStatementCopy" type="file" class="form-control"
                                                       id="bankStatementCopy" value="{{$claims->bankStatementCopy}}">
                                                @error('bankStatementCopy')
                                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <br><br>
                                        <div style="text-align: right">
                                            <button type="submit" class="btn btn-primary">Update Claim</button>
                                        </div>


                                    </div>


                                </div>
                            </div>
                            @endforeach
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
