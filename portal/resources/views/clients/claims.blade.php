@extends('layouts.portalMaster')

@section('content')


    <div class="page-wrapper">
    <div class="content container-fluid">

        <div class="page-header">
            <div class="row">
                <div class="col">
                    <h3 class="page-title">Claims</h3>
                </div>
                <div class="col-auto text-right">
                    <a class="btn btn-white filter-btn" href="javascript:void(0);" id="filter_search">
                        <i class="fas fa-filter"></i>
                    </a>

                    <a href="{{url('clients/claimForm')}}" class="btn btn-primary add-button ml-3" data-toggle="tooltip" title="Create Claim">
                        <i class="fas fa-plus"></i>
                    </a>
                </div>
            </div>
        </div>


        <form action="#" method="post" id="filter_inputs">
            <div class="card filter-card">
                <div class="card-body pb-0">
                    <div class="row filter-row">
                        <div class="col-sm-6 col-md-3">
                            <div class="form-group">
                                <label>Service</label>
                                <input class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="form-group">
                                <label>From Date</label>
                                <div class="cal-icon">
                                    <input class="form-control datetimepicker" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="form-group">
                                <label>To Date</label>
                                <div class="cal-icon">
                                    <input class="form-control datetimepicker" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="form-group">
                                <button class="btn btn-primary btn-block" type="submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-center mb-0 datatable">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Policy Id</th>
                                    <th>Deceased Name</th>
                                    <th>Claimant Name</th>
                                    <th>Parlour Name</th>
                                    <th>Beneficiary Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($claims as $claim)
                                <tr>
                                    <td>{{ ++$i }}</td>

                                    <td>{{ $claim->policyHolderId }}</td>
                                    <td>{{ $claim->deceasedFullName }}</td>
                                    <td>{{ $claim->claimantFullName }}</td>
                                    <td>{{ $claim->funeralParlourName }}</td>
                                    <td>{{ $claim->beneficiaryName }}</td>
                                    <td>{{ $claim->status }}</td>


                                    <td>
                                        <a href="{{ url('clients/claim_details/'.$claim->id) }}" class="btn btn-sm bg-info-light">
                                            <i class="far fa-edit mr-1"></i> Edit
                                        </a>
                                    </td>
                                </tr>


                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
