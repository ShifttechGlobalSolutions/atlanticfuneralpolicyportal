@extends('layouts.portalMaster')

@section('content')


    <div class="page-wrapper">
        <div class="content container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Policy Profile</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                            <li class="breadcrumb-item active">Policy Profile</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="profile-header">
                        <div class="row align-items-center">
                            <div class="col-auto profile-image">
                                <a href="#">
                                    <img class="rounded-circle" alt="User Image" src="{{asset('public/frontend/img/log.png')}}">
                                </a>
                            </div>
                            <div class="col ml-md-n2 profile-user-info">
                                <h4 class="user-name mb-0">{{ Auth::user()->name }}</h4>
                                <h6 class="text-muted">{{ Auth::user()->email }}</h6>
{{--                                <div class="user-Location"><i class="fas fa-map-marker-alt"></i>{{ Auth::user()->address }}</div>--}}
{{--                                <div class="about-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>--}}
                            </div>
                            <div class="col-auto profile-btn">

                                <a href="{{url('clients/profile')}}" class="btn btn-primary">
                                    Profile
                                </a>
                            </div>
                        </div>
                    </div>
{{--                    <div class="profile-menu">--}}
{{--                        <ul class="nav nav-tabs nav-tabs-solid">--}}
{{--                            <li class="nav-item">--}}
{{--                                <a class="nav-link active" data-toggle="tab" href="#per_details_tab">Personal Details</a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a class="nav-link" data-toggle="tab" href="#password_tab">Password</a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
                    <div class="tab-content profile-tab-cont">

                        <div class="tab-pane fade show active" id="per_details_tab">

                            <div class="row">
                                <div class="col-lg-12">
                                    @foreach ($policy_details as $user)
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title d-flex justify-content-between">
                                                <span>Policy Details</span>
                                                <a class="edit-link" data-toggle="modal" href="#edit_personal_details"><i class="far fa-eye mr-1"></i></i>Personal Details</a>
                                            </h5>
                                            <div class="row">
                                                <div class="col-md-6">

                                                <p class="col-sm-6"><strong>Policy Number:</strong>  {{ $user->policy_no}}</p>


                                                <p class="col-sm-9"><strong>Policy Status:</strong> {{$user->status}}</p>


                                                <p class="col-sm-9"><strong>Package:</strong> {{ $user->package}}</p>
                                            </div>

                                                <div class="col-md-6">

                                                <p class="col-sm-6"><strong>Cover Amount:</strong> {{$user->coverAmount}}</p>


                                                <p class="col-sm-12 mb-0"><strong>Age Group:</strong> {{$user->ageGroup}}
                                                   </p>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- @endforeach --}}

                                    <div class="modal fade" id="edit_personal_details" aria-hidden="true" role="dialog">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header" style="background-color: #000046; color:white">
                                                    <h5 class="modal-title">Personal Details</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="post" enctype="multipart/form-data" action="{{ route('clients/update_holder') }}">
                                                        @CSRF
                                                        <div class="row form-row">
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>First Name</label>
                                                                    <input type="text" class="form-control" name="client_firstname" value="{{ $user->client_firstname }}">
                                                                    <input type="hidden" class="form-control" name="id" value="{{ $user->id }}">
                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>Last Name</label>
                                                                    <input type="text" class="form-control" name="client_lastname" value="{{$user->client_lastname}}">
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <label>Date of Birth</label>
                                                                    <div class="cal-icon">
                                                                        <input type="text" class="form-control" name="client_dob" value="{{$user->client_dob}}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>Email ID</label>
                                                                    <input type="email" class="form-control" name="email" value="{{$user->email}}">
                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>Mobile</label>
                                                                    <input type="text" value="{{$user->cell}}" name="cell" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <h5 class="form-title"><span>Address</span></h5>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <label>Address</label>
                                                                    <input type="text" class="form-control" name="strName" value="{{$user->strName }}">
                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>City</label>
                                                                    <input type="text" class="form-control" name="surburb" value="{{$user->surburb}}">
                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>State</label>
                                                                    <input type="text" class="form-control" name="state" value="">
                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>Zip Code</label>
                                                                    <input type="text" class="form-control" name="postalCode" value="{{$user->postalCode}}">
                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>Country</label>
                                                                    <input type="text" class="form-control" value="South Africa">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button type="submit"  class="btn btn-primary btn-block">Save Changes</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                </div>
                            </div>

                        </div>
@endforeach

                    </div>
                    <div class="tab-content profile-tab-cont">

                        <div class="tab-pane fade show active" id="per_details_tab">

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <h5 class="card-title d-flex justify-content-between">
                                                    <span style="line-height: 30px">Beneficiary Details</span>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <a style="width: 25px; height:25px" data-toggle="modal" href="#add_beneficiary_details" class="btn btn-primary add-button ml-3" data-toggle="tooltip" title="Add New Beneficiary">
                                                            <i class="fas fa-plus"></i>
                                                        </a>


                                                </h5>

                                            </div>
                                            <div class="table-responsive" id="filter_search">
                                                <table class="table table-hover table-center mb-0 datatable">
                                                    <thead>
                                                    <tr>

                                                        <th>#</th>
                                                        <th>Full Name</th>
                                                        <th>Phone</th>


                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($beneficiaries as $beneficiary)
                                                        <tr>

                                                            <td> {{ $ben_counter++}}</td>
                                                            <td>{{ $beneficiary->ben_firstname }}</td>
                                                            <td>{{ $beneficiary->ben_contact_number }}</td>
                                                            <td>
                                                                <a href="{{url('clients/beneficiary_details/'.$beneficiary->id) }}}}" class="btn btn-sm bg-info-light">
                                                                    <i class="far fa-eye mr-1"></i> View
                                                                </a>
                                                                <a href="{{url('clients/delete_beneficiary/'.$beneficiary->id) }}}}" class="btn btn-sm bg-danger-light">
                                                                    <i class="fa fa-trash mr-1"></i> Delete
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal fade" id="add_beneficiary_details" aria-hidden="true" role="dialog">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header" style="background-color: #000046; color:white">
                                                    <h5 class="modal-title">Add New Benefiary</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="post" enctype="multipart/form-data" action="{{ route('clients/add-beneficiary') }}">
                                                        @CSRF
                                                        <div class="row form-row">
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>First Name</label>
                                                                    <input type="hidden" class="form-control" name="policy_no" value="{{$policy_no}}">
                                                                    <input type="text" class="form-control" name="ben_firstname">
                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>Last Name</label>
                                                                    <input type="text" class="form-control" name="ben_lastname">
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <label>Date of Birth</label>
                                                                    <input type="date" class="form-control" name="ben_dob">
                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>ID / Passport Number</label>
                                                                    <input type="text" class="form-control" name="ben_id_number">
                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>Mobile</label>
                                                                    <input type="text" name="ben_mobile" class="form-control">
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <button type="submit" class="btn btn-primary btn-block">Submit</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                {{-- @endforeach --}}

                                <div class="col-lg-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title d-flex justify-content-between">
                                                <span>Spouse Details</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <div class="col-auto text-right">
                                                    <a style="width: 25px; height:25px" data-toggle="modal" href="#add_spouse_details" class="btn btn-primary add-button ml-3" data-toggle="tooltip" title="Add New Spouse">
                                                        <i class="fas fa-plus"></i>
                                                    </a>
                                                </div>

                                            </h5>
                                            <div class="table-responsive" id="filter_search">
                                                <table class="table table-hover table-center mb-0 datatable">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Full Name</th>
                                                        <th>ID Number</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($spouses as $spouse)
                                                    <tr>
                                                        <td> {{ $spouse_counter++}}</td>
                                                        <td>{{ $spouse->spouse_firstname }}</td>
                                                        <td>{{ $spouse->spouse_idNumber }}</td>

                                                        <td>
                                                            <a href="{{url('clients/spouse_details/'.$spouse->id) }}}}" class="btn btn-sm bg-info-light">
                                                                <i class="far fa-eye mr-1"></i> View
                                                            </a>
                                                            <a href="{{url('clients/delete_spouse/'.$spouse->id) }}}}" class="btn btn-sm bg-danger-light">
                                                                <i class="fa fa-trash mr-1"></i> Delete
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal fade" id="add_spouse_details" aria-hidden="true" role="dialog">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header" style="background-color: #000046; color:white">
                                                    <h5 class="modal-title">Add New Spouse</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="post" enctype="multipart/form-data" action="{{ route('clients/add-spouse') }}">
                                                        @CSRF
                                                        <div class="row form-row">
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>First Name</label>
                                                                    <input type="hidden" class="form-control" name="policy_no" value="{{$policy_no}}">
                                                                    <input type="text" class="form-control" name="spouse_firstname">
                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>Last Name</label>
                                                                    <input type="text" class="form-control" name="spouse_lastname">
                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>Date of Birth</label>
                                                                    <input type="date" class="form-control" name="spouse_dob">
                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>ID / Passport Number</label>
                                                                    <input type="text" class="form-control" name="spouse_id_number">
                                                                </div>
                                                            </div>

                                                        <button type="submit" class="btn btn-primary btn-block">Submit</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>




                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
