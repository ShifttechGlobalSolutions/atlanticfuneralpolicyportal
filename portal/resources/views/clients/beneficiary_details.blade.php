@extends('layouts.portalMaster')

@section('content')


    <div class="page-wrapper">
        <div class="content container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Beneficiary Details</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="index.html">Policy Profile</a></li>
                            <li class="breadcrumb-item active">Beneficiary Details</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="profile-header">
                        <div class="row align-items-center">
                            <div class="col-auto profile-image">
                                <a href="#">
                                    <img class="rounded-circle" alt="User Image" src="{{asset('public/frontend/img/log.png')}}">
                                </a>
                            </div>
                            <div class="col ml-md-n2 profile-user-info">
                                <h4 class="user-name mb-0">{{ Auth::user()->name }}</h4>
                                <h6 class="text-muted">{{ Auth::user()->email }}</h6>
{{--                                <div class="user-Location"><i class="fas fa-map-marker-alt"></i>{{ Auth::user()->address }}</div>--}}
{{--                                <div class="about-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>--}}
                            </div>
                            <div class="col-auto profile-btn">

                                <a href="{{url('clients/profile')}}" class="btn btn-primary">
                                    Profile
                                </a>
                            </div>
                        </div>
                    </div>


                    <div class="tab-content profile-tab-cont">

                        {{-- <div id="payment" class="tab-pane fade"> --}}
                            @foreach ($beneficiary as $beneficiary_detail)

                            @endforeach
                            <div class="card">
                                <div class="card-body">
                                    <form method="post" enctype="multipart/form-data" action="{{ route('clients/update-beneficiary') }}">
                                        @CSRF
                                    {{-- <h5 class="card-title">Benefit Payment Options</h5> --}}
                                    <div class="row">


                                        <div class="form-group col-md-6 ">
                                            <label>Policy No</label>
                                            <input type="hidden" name="id" class="form-control" value="{{$beneficiary_detail->id}}" readonly>
                                            <input type="text" name="policy_no" class="form-control" value="{{$beneficiary_detail->policy_no}}" readonly>
                                        </div>
                                        {{-- <div class="form-group col-md-6 ">
                                            <label>Contact Number</label>
                                            <input type="text" name="funeralParlourPhone" class="form-control" value="" readonly>
                                        </div> --}}
                                        <hr>

                                    </div>
                                   <hr>
                                    {{-- <h5 class="card-title">Nominated Beneficiary</h5> --}}
                                    <div class="row">
                                        <div class="form-group col">
                                            <label>First Name</label>
                                            <input name="ben_firstname" type="text" class="form-control" value="{{$beneficiary_detail->ben_firstname}}">
                                        </div>
                                        <div class="form-group col">
                                            <label>Last Name</label>
                                            <input name="ben_lastname" type="text" class="form-control" value="{{$beneficiary_detail->ben_lastname}}">
                                        </div>
                                        <div class="form-group col">
                                            <label>ID / Passport Number</label>
                                            <input name="ben_id_number" type="text" class="form-control" value="{{$beneficiary_detail->ben_idNumber}}">
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="form-group col">
                                            <label>Date Of Birth</label>
                                            <input name="ben_dob" class="form-control " type="text" value="{{$beneficiary_detail->ben_dob}}">
                                        </div>
                                        <div class="form-group col">
                                            <label>Contact Number</label>
                                            <input name="ben_contact_number" type="text" class="form-control" value="{{$beneficiary_detail->ben_contact_number}}">
                                        </div>
                                        {{-- <div class="form-group col">
                                            <label>Beneficiary Branch Code</label>
                                            <input name="beneficiaryBranchCode" class="form-control " type="text" value="">
                                        </div> --}}
                                    </div>

{{-- @endforeach --}}


                                    <div class="row">
                                        <div class="col-auto profile-btn">
                                            <a href="{{url('clients/policy')}}" class="btn btn-secondary">
                                                Back
                                            </a>

                                            <button type="submit" class="btn btn-primary">Update</button>

                                            {{-- <a href="{{url('clients/update-beneficiary')}}" class="btn btn-primary">
                                                Update
                                            </a> --}}
                                        </div>
                                    <div class="row">

                                </div>
                            </form>
                            </div>

                        </div>

                    </div>



                    @endsection
