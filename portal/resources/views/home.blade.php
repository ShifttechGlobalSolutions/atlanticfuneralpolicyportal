@extends('layouts.portalMaster')

@section('content')

<div class="page-wrapper">
    <div class="content container-fluid">

        <div class="page-header">
            <div class="row">
                <div class="col-12">
                    <h3 class="page-title">Welcome   {{ Auth::user()->name }}</h3>
                </div>
            </div>
        </div>
        @if(Auth::user()->hasRole('Admin'))

        <div class="row">
            <div class="col-xl-4 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon bg-primary">
                            <i class="far fa-credit-card"></i>
                            </span>
                            <div class="dash-widget-info">

                                <h3>{{ $all_policies_count }}</h3>

                                <h6 class="text-muted">Policies</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon bg-primary">
                            <i class="fas fa-user-shield"></i>
                            </span>
                            <div class="dash-widget-info">
                                <h3>{{ $claims }}</h3>
                                <h6 class="text-muted">Claims</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon bg-primary">
                            <i class="fas fa-users"></i>
                            </span>
                            <div class="dash-widget-info">
                                <h3>{{ $allAgents_count }}</h3>
                                <h6 class="text-muted">Agents</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-md-6 d-flex">

                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">Recent Policies</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-center">
                                <thead>
                                <tr>

                                    <th>FullName</th>
                                    <th>Phone</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($recent_policies as $policies)
                                    <tr>
                                        <td  hidden>{{ $policies->id}}</td>
                                        <td>
                                            <a href="service-details.html">
                                                <img class="rounded service-img mr-1" src="assets/img/services/service-01.jpg" alt=""> {{ $policies->client_firstname . " " . $policies->client_lastname }}
                                            </a>
                                        </td>

                                        <td>{{ $policies->cell}}</td>


                                        <td>
                                            {{$policies->status}}
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/show/'.$policies->policy_no) }}" class="btn btn-info"  data-toggle="tooltip" title="View Policy">
                                                <i class="far fa-eye mr-1"></i>
                                            </a>
                                            {{--                                            <a href="{{ url('admin/edit/'.$policy_holder->policy_no) }}" class="btn btn-primary">--}}
                                            {{--                                                <i class="far fa-edit mr-1"></i> Edit--}}
                                            {{--                                            </a>--}}
                                            <a href="{{ url('admin/delete/'.$policies->policy_no) }}" class="btn btn-danger"data-toggle="tooltip" title="Delete Policy">
                                                <i class="far fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6 d-flex">

                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">Recent Claims</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-center">
                                <thead>
                                <tr>


                                    <th>Policy Holder Name</th>

                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($recent_claims as $claims)
                                    <tr>

                                        <td>

                                            {{ $claims->policyHolderFullName ." ". $claims->policyHolderSurname  }}
                                        </td>

                                        <td>
                                            {{ $claims->status }}
                                        </td>


                                        <td>
                                            <a href="{{ url('admin/claims/view-claims/'.$claims->id) }}" class="btn btn-info" data-toggle="tooltip" title="View Claim">
                                                <i class="far fa-eye mr-1"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        @endif

        @if(Auth::user()->hasRole('Agent'))
            <div class="row">
                <div class="col-xl-4 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
                            <span class="dash-widget-icon bg-primary">
                            <i class="far fa-user"></i>
                            </span>
                                <div class="dash-widget-info">
                                    <h3>{{$countAgentPolicies}}</h3>
                                    <h6 class="text-muted">Policies</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
                            <span class="dash-widget-icon bg-primary">
                            <i class="fas fa-user-shield"></i>
                            </span>
                                <div class="dash-widget-info">
                                    <h3>{{$videosCount}}</h3>
                                    <h6 class="text-muted">Tutorials</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
                            <span class="dash-widget-icon bg-primary">
                            <i class="far fa-credit-card"></i>
                            </span>
                                <div class="dash-widget-info">
                                    <h3>R0</h3>
                                    <h6 class="text-muted">Commission Earned</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 d-flex">

                    <div class="card card-table flex-fill">
                        <div class="card-header">
                            <h4 class="card-title">Recent Policies</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-center">
                                    <thead>
                                    <tr>

                                        <th>FullName</th>
                                        <th>Phone</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($agentPolicies as $agent_polices)
                                    <tr>


                                        <td> {{ $agent_polices->client_firstname }}</td>
                                        <td>
                                            {{ $agent_polices->cell }}
                                        </td>
                                        <td>
                                            {{ $agent_polices->status }}
                                        </td>
                                        <td>
                                            <a href="{{ url('agents/policy_details/'.$agent_polices->policy_no) }}" class="btn btn-info"  data-toggle="tooltip" title="View Policy">
                                                <i class="far fa-eye mr-1"></i>
                                            </a>
                                            {{--                                            <a href="{{ url('admin/edit/'.$policy_holder->policy_no) }}" class="btn btn-primary">--}}
                                            {{--                                                <i class="far fa-edit mr-1"></i> Edit--}}
                                            {{--                                            </a>--}}
{{--                                            <a href="{{ url('admin/delete/'.$policies->policy_no) }}" class="btn btn-danger"data-toggle="tooltip" title="Delete Policy">--}}
{{--                                                <i class="far fa-trash-alt"></i>--}}
{{--                                            </a>--}}
                                        </td>
                                    </tr>

                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        @endif


         @if(Auth::user()->hasRole('Client'))
            <div class="row">

                <div class="col-xl-6 col-sm-6 col-12">
                    <div class="card">
                        <a href="{{url('clients/beneficiaries')}}">
                        <div class="card-body">
                            <div class="dash-widget-header">
                            <span class="dash-widget-icon bg-primary">
                            <i class="fas fa-users"></i>
                            </span>
                                <div class="dash-widget-info">
                                    <h3>{{$count_beneficiaries }}</h3>
                                    <h6 class="text-muted">Beneficiaries</h6>
                                </div>
                            </div>
                        </div>
                        </a>
                    </div>
                </div>

                <div class="col-xl-6 col-sm-6 col-12">
                    <div class="card">
                        <a href="{{url('clients/payments')}}">
                        <div class="card-body">
                            <div class="dash-widget-header">
                            <span class="dash-widget-icon bg-primary">
                            <i class="far fa-credit-card"></i>
                            </span>
                                <div class="dash-widget-info">
                                    <h3>R0.00</h3>
                                    <h6 class="text-muted">Premiums</h6>
                                </div>
                            </div>
                        </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 d-flex">

                    <div class="card flex-fill">
                        <div class="card-header">
                            <h4 class="card-title">Policy Profile</h4>
                        </div>
                        @foreach ($policy_details as $user)
                        <div class="card-body">
                            <div class="dash-widget-header">
                            <span class="dash-widget-icon bg-primary">
                            <i class="fa fa-user-shield"></i>
                            </span>
                                <div class="dash-widget-info">
                                    <h3> {{  $user->client_firstname }}</h3>
                                    <h6 class="text badge bg-danger inv-badge">Status: {{ $user->status }} </h6>
                                </div>


                            </div><br>
                            <div class="row ">
                                <div class="col">
                                    <h6 class="text">Policy No: {{ $user->policy_no }}</h6>
                                    <h6 class="text">ID No: {{ $user->client_id_number }}</h6>
                                    <h6 class="text">Cell No: {{ $user->cell }}</h6>


                                </div>
                                <div class="col">
                                    <h6 class="text">Package: {{ $user->package }} </h6>
                                    <h6 class="text">Cover Amount:R.{{ $user->coverAmount }} </h6>



                                </div>



                            </div>
                            <a href="{{url('clients/policy')}}" class="btn btn-primary btn-block" >View Policy</a>
                        </div>
                        @endforeach
                    </div>

                </div>
                <div class="col-md-6 d-flex">

                    <div class="card card-table flex-fill">
                        <div class="card-header">
                            <h4 class="card-title">My Premiums</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-center">
                                    <thead>
                                    <tr>

                                        <th>Date</th>
                                        <th>Package</th>
                                        <th>Status</th>
                                        <th>Price</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>

{{--                                        <td class="text-nowrap">9 Sep 2021</td>--}}
{{--                                        <td>Funeral Cover</td>--}}
{{--                                        <td>--}}
{{--                                            <span class="badge bg-success inv-badge">Approved</span>--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            <div class="font-weight-600">R15000</div>--}}
{{--                                        </td>--}}
                                    </tr>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
         @endif
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="addVid" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0C0457;color:white">
                <h5 class="modal-title" id="exampleModalLabel">Add New Video</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="background-color: #ffffff">
                <form method="post" action="{{ route('admin/add-video') }}" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" id="title" name="title" placeholder="Video Title" style="background-color: #F5F5F5;color: black">
                        </div>

                        <div class="form-group">
                            <input type="file" class="form-control" id="file" name="file"  style="background-color: #F5F5F5;color: black">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="description" id="description" placeholder="Video Description"></textarea>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js" integrity="sha512-QSkVNOCYLtj73J4hbmVoOV6KVZuMluZlioC+trLpewV8qMjsWqlIQvkn1KGX2StWvPMdWGBqim1xlC8krl1EKQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
{{-- <script>--}}
{{--    $(function(){--}}
{{--        let date =  new Date().getFullYear();--}}
{{--        var allPol = <?php echo json_encode($allPol); ?>;--}}
{{--        var all_policies_canvas = $("#all_policies");--}}
{{--        var all_policies = new Chart(all_policies_canvas,{--}}
{{--            type: 'bar',--}}
{{--            data:{--}}
{{--                labels:['Jan', 'Feb', 'Mar', 'April', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],--}}
{{--                datasets:[--}}
{{--                    {--}}
{{--                        label: 'All policies ' + date,--}}
{{--                        data: allPol,--}}
{{--                        backgroundColor:['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet', 'purple', 'pink', 'silver', 'gold', 'black']--}}
{{--                    }--}}
{{--                ]--}}
{{--            },--}}
{{--            options:{--}}
{{--                scales:{--}}
{{--                    yAxes:[{--}}
{{--                        ticks:{--}}
{{--                            beginatZero:true--}}
{{--                        }--}}
{{--                    }]--}}
{{--                }--}}
{{--            }--}}
{{--        })--}}
{{--    })--}}

{{--    // Approved Policy--}}
{{--    $(function(){--}}
{{--        let date =  new Date().getFullYear();--}}
{{--        var aprPol = <?php echo json_encode($aprPol); ?>;--}}
{{--        var apr_policies_canvas = $("#apr_policies");--}}
{{--        var apr_policies = new Chart(apr_policies_canvas,{--}}
{{--            type: 'bar',--}}
{{--            data:{--}}
{{--                labels:['Jan', 'Feb', 'Mar', 'April', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],--}}
{{--                datasets:[--}}
{{--                    {--}}
{{--                        label: 'Approved Policies ' + date,--}}
{{--                        data: aprPol,--}}
{{--                        backgroundColor:['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet', 'purple', 'pink', 'silver', 'gold', 'black']--}}
{{--                    }--}}
{{--                ]--}}
{{--            },--}}
{{--            options:{--}}
{{--                scales:{--}}
{{--                    yAxes:[{--}}
{{--                        ticks:{--}}
{{--                            beginatZero:true--}}
{{--                        }--}}
{{--                    }]--}}
{{--                }--}}
{{--            }--}}
{{--        })--}}
{{--    })--}}

{{--     // Declined Policy--}}
{{--     $(function(){--}}
{{--        let date =  new Date().getFullYear();--}}
{{--        var declPol = <?php echo json_encode($declPol); ?>;--}}
{{--        var decl_policies_canvas = $("#decl_policies");--}}
{{--        var decl_policies = new Chart(decl_policies_canvas,{--}}
{{--            type: 'bar',--}}
{{--            data:{--}}
{{--                labels:['Jan', 'Feb', 'Mar', 'April', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],--}}
{{--                datasets:[--}}
{{--                    {--}}
{{--                        label: 'Declined policies ' + date,--}}
{{--                        data: declPol,--}}
{{--                        backgroundColor:['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet', 'purple', 'pink', 'silver', 'gold', 'black']--}}
{{--                    }--}}
{{--                ]--}}
{{--            },--}}
{{--            options:{--}}
{{--                scales:{--}}
{{--                    yAxes:[{--}}
{{--                        ticks:{--}}
{{--                            beginatZero:true--}}
{{--                        }--}}
{{--                    }]--}}
{{--                }--}}
{{--            }--}}
{{--        })--}}
{{--    })--}}


{{--//All Claims--}}
{{--$(function(){--}}
{{--        let date =  new Date().getFullYear();--}}
{{--        var allCl = <?php echo json_encode($allCl); ?>;--}}
{{--        var all_claims_canvas = $("#all_claims");--}}
{{--        var all_claims = new Chart(all_claims_canvas,{--}}
{{--            type: 'line',--}}
{{--            data:{--}}
{{--                labels:['Jan', 'Feb', 'Mar', 'April', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],--}}
{{--                datasets:[--}}
{{--                    {--}}
{{--                        label: 'All Claims ' + date,--}}
{{--                        data: allCl,--}}
{{--                        backgroundColor:['black', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet', 'purple', 'pink', 'silver', 'gold', 'red']--}}
{{--                    }--}}
{{--                ]--}}
{{--            },--}}
{{--            options:{--}}
{{--                scales:{--}}
{{--                    yAxes:[{--}}
{{--                        ticks:{--}}
{{--                            beginatZero:true--}}
{{--                        }--}}
{{--                    }]--}}
{{--                }--}}
{{--            }--}}
{{--        })--}}
{{--    })--}}

{{--    // Approved Claims--}}
{{--    $(function(){--}}
{{--        let date =  new Date().getFullYear();--}}
{{--        var aprCl = <?php echo json_encode($aprCl); ?>;--}}
{{--        var apr_policies_canvas = $("#apr_claims");--}}
{{--        var apr_policies = new Chart(apr_policies_canvas,{--}}
{{--            type: 'line',--}}
{{--            data:{--}}
{{--                labels:['Jan', 'Feb', 'Mar', 'April', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],--}}
{{--                datasets:[--}}
{{--                    {--}}
{{--                        label: 'Approved Claims ' + date,--}}
{{--                        data: aprCl,--}}
{{--                        backgroundColor:['black', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet', 'purple', 'pink', 'silver', 'gold', 'red']--}}
{{--                    }--}}
{{--                ]--}}
{{--            },--}}
{{--            options:{--}}
{{--                scales:{--}}
{{--                    yAxes:[{--}}
{{--                        ticks:{--}}
{{--                            beginatZero:true--}}
{{--                        }--}}
{{--                    }]--}}
{{--                }--}}
{{--            }--}}
{{--        })--}}
{{--    })--}}

{{--     // Declined Claims--}}
{{--     $(function(){--}}
{{--        let date =  new Date().getFullYear();--}}
{{--        var declCl = <?php echo json_encode($declCl); ?>;--}}
{{--        var decl_policies_canvas = $("#decl_claims");--}}
{{--        var decl_policies = new Chart(decl_policies_canvas,{--}}
{{--            type: 'line',--}}
{{--            data:{--}}
{{--                labels:['Jan', 'Feb', 'Mar', 'April', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],--}}
{{--                datasets:[--}}
{{--                    {--}}
{{--                        label: 'Declined Claims ' + date,--}}
{{--                        data: declCl,--}}
{{--                        backgroundColor:['black', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet', 'purple', 'pink', 'silver', 'gold', 'red']--}}
{{--                    }--}}
{{--                ]--}}
{{--            },--}}
{{--            options:{--}}
{{--                scales:{--}}
{{--                    yAxes:[{--}}
{{--                        ticks:{--}}
{{--                            beginatZero:true--}}
{{--                        }--}}
{{--                    }]--}}
{{--                }--}}
{{--            }--}}
{{--        })--}}
   {{-- }) --}}
{{--</script> --}}

@endsection
