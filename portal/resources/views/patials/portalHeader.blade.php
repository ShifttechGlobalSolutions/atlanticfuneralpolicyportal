
    <div class="header">
        <div class="header-left">
            <a href="" class="logo logo-small">
                <img src="{{asset('public/frontend/img/log.jpg')}}" alt="Logo" width="50" height="50">
            </a>
        </div>
        <a href="javascript:void(0);" id="toggle_btn">
            <i class="fas fa-align-left"></i>
        </a>
        <a class="mobile_btn" id="mobile_btn" href="javascript:void(0);">
            <i class="fas fa-align-left"></i>
        </a>
        <ul class="nav user-menu">

            <li class="nav-item dropdown noti-dropdown">
                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                    <i class="far fa-bell"></i> <span class="badge badge-pill"></span>
                </a>
            </li>


            <li class="nav-item dropdown">
                <a href="javascript:void(0)" class="dropdown-toggle user-link  nav-link" data-toggle="dropdown">
<span class="user-img">
<img class="rounded-circle" src="{{asset('public/frontend/img/log.jpg')}}" width="40" alt="Admin">
</span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">


                    @if(Auth::user()->hasRole('Admin'))
                    <a class="dropdown-item" href="{{ route('users.index') }}">Manage Users</a>
                    <a class="dropdown-item" href="{{ route('roles.index') }}">Manage Role</a>


                    <a class="dropdown-item" href="" data-toggle="modal" data-target="#addVid">Upload Video Tutorial</a>
                    @endif



                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>


                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>


                </div>
            </li>

        </ul>
    </div>


    <div class="sidebar" id="sidebar">
        <div class="sidebar-logo">
            <a href="{{url('/home')}}">
                <img src="{{asset('public/frontend/img/log.png')}}" class="img-fluid" alt="">
            </a>
        </div>
        <div class="sidebar-inner slimscroll">
            <div id="sidebar-menu" class="sidebar-menu">
                    @if(Auth::user()->hasRole('Admin'))
                    <ul>
{{--                        <li class="active">--}}
{{--                            <a href="{{url('/home')}}"><i class="fas fa-columns"></i> <span>Dashboard</span></a>--}}
{{--                        </li>--}}
                        <li>
                            <a href="{{url('admin/policies')}}"><i class="fas fa-layer-group"></i> <span>Policies</span></a>
                        </li>
                        <li>
                            <a href="{{url('admin/claims')}}"><i class="fas fa-file-contract"></i> <span>Claims</span></a>
                        </li>
                        <li>
                            <a href="{{url('admin/agents')}}"><i class="fas fa-users"></i> <span> Agents</span></a>
                        </li>

                         <li>
                            <a href="{{url('admin/policy_plans')}}"><i class="fas fa-file-invoice"></i> <span>Policy Plans</span></a>
                        </li>

                        {{-- <li>
                             <a href="{{url('admin/notifications')}}"><i class="fas fa-bell"></i> <span>Notifications</span></a>
                         </li> --}}
                        <li>
                            <a href="{{url('admin/tutorials')}}"><i class="fa fa-video"></i> <span>Video Tutorials</span></a>
                        </li>

                        <li>
                            <a href="{{url('admin/profile')}}"><i class="fas fa-user"></i> <span>Profile</span></a>
                        </li>




                    </ul>
                        @endif
                    @if(Auth::user()->hasRole('Agent'))
                            <ul>
                                <li class="active">
                                    <a href="{{url('/home')}}"><i class="fas fa-columns"></i> <span>Dashboard</span></a>
                                </li>
                                <li>
                                    <a href="{{url('agents/policy')}}"><i class="fas fa-layer-group"></i> <span>My Policies</span></a>
                                </li>
                                <li>
                                    <a href="{{url('agents/commissions')}}"><i class="fas fa-coins"></i> <span>Commissions</span></a>
                                </li>

                                <li>
                                    <a href="{{url('agents/profile')}}"><i class="fas fa-user"></i> <span>Profile</span></a>
                                </li>
                                <li>
                                    <a href="{{url('agents/tutorials')}}"><i class="fa fa-video"></i> <span>Video Tutorials</span></a>
                                </li>


                            </ul>
                        @endif
                    @if(Auth::user()->hasRole('Client'))
                            <ul>
                                <li class="active">
                                    <a href="{{url('/home')}}"><i class="fas fa-columns"></i> <span>Dashboard</span></a>
                                </li>
                                <li>
                                    <a href="{{url('clients/policy')}}"><i class="fas fa-id-card"></i> <span>My Policy</span></a>
                                </li>
                                <li>
                                    <a href="{{url('clients/claims')}}"><i class="fas fa-file-contract"></i> <span>My Claims</span></a>
                                </li>


                                <li>
                                    <a href="{{url('clients/payments')}}"><i class="fas fa-money-bill"></i> <span>My Premiums</span></a>
                                </li>

                                <li>
                                    <a href="{{url('clients/profile')}}"><i class="fas fa-user"></i> <span>Profile</span></a>
                                </li>



                            </ul>
                    @endif
            </div>
        </div>
    </div>

