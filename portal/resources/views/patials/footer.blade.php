<footer class="footer-area">
    <div class="container">

        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <div class="logo">
                        <a href="#"><img src="{{asset('public/frontend/img/log.png')}}" alt="image"></a>

                    </div>
{{--                    <ul class="social">--}}
{{--                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>--}}
{{--                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>--}}
{{--                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>--}}
{{--                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>--}}
{{--                    </ul>--}}
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h3>Quick Links</h3>
                    <ul class="footer-quick-links">
                        <li><a href="{{url('/')}}">Home</a></li>

                        <li><a href="{{url('/')}}">About Us</a></li>
                        <li><a href="{{url('policy_products/funeral_policies')}}">Our Packages</a></li>
                        <li><a href="{{url('/login')}}">My Policy</a></li>
                        <li><a href="{{url('/contact')}}">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-0 offset-sm-3 offset-md-3">
                <div class="single-footer-widget">
                    <h3>Contact Info</h3>
                    <ul class="footer-contact-info">
                        <li><span>Location:</span> 16 Soldaat Plein, Factreton, Cape Town</li>
                        <li><span>Email:</span> <a href="#"><span class="__cf_email__" data-cfemail="29594c485b46694e44484045074a4644">info@atlanticfunerals.co.za</span></a></li>
                        <li><span>Phone:</span> <a href="#">+(27) 629 806 851</a></li>
                      </ul>
                </div>
            </div>
        </div>
        <div class="copyright-area">
            <div class="row align-items-center">
                <div class="col-lg-8 col-sm-8 col-md-8">
                    <p>Copyright ©<script>document.write(new Date().getFullYear());</script>  Atlantic Funerals. All Rights Reserved. Designed by Shifttech Global Solutions</p>

                </div>
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <ul>
                        <li><a href="#terms"  data-toggle="modal">Privacy Policy</a></li>
                        <li><a href="#terms"  data-toggle="modal">Terms & Conditions</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
