<header class="header-area">

    <div class="top-header">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3 col-md-3">
                    <ul class="top-header-nav">
                        <li><a href="https://sacoronavirus.co.za/">COVID-19 Updates</a></li>

{{--                        <li><a href="#callMeBack" data-toggle="modal">Our App</a></li>--}}

                    </ul>
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="top-header-right-side">
                        <ul>
                            <li>
                                <div class="icon">
                                    <i class="flaticon-email"></i>
                                </div>
                                <span>Drop us an email:</span>
                                <a href="#"><span>info@atlantic-funerals.co.za</span></a>
                            </li>
                            <li>
                                <div class="icon">
                                    <i class="flaticon-call"></i>
                                </div>
                                <span>Call us:</span>
                                <a href="#">+27 629 806 851 </a>
                            </li>
                            <li>
                                <a href="{{url('/login')}}" class="default-btn">LOGIN<span></span></a>
                            </li>

                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="navbar-area">
        <div class="pearo-responsive-nav">
            <div class="container">
                <div class="pearo-responsive-menu">
                    <div class="logo">
                        <a href="{{url('/')}}">
                            <img src="{{asset('public/frontend/img/log.png')}}" alt="logo">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="pearo-nav">
            <div class="container">
                <nav class="navbar navbar-expand-md navbar-light">
                    <a class="navbar-brand" href="{{url('/')}}">
                        <img src="{{asset('public/frontend/img/log.png')}}" alt="logo">
                    </a>
                    <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                        <ul class="navbar-nav">



                            <li class="nav-item"><a href="#" class="nav-link">Services <i class="flaticon-down-arrow"></i></a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item"><a href="{{url('/policy_products/funeral_policies')}}" class="nav-link">FUNERAL POLICIES</a></li>
                                    <li class="nav-item"><a href="#callMeBack" data-toggle="modal" class="nav-link">CHURCHES</a></li>
                                    <li class="nav-item"><a href="#callMeBack" data-toggle="modal" class="nav-link">GROUP SCHEMES</a></li>
                                    <li class="nav-item"><a href="#callMeBack" data-toggle="modal" class="nav-link">UNIONS</a></li>
                                    <li class="nav-item"><a href="#callMeBack" data-toggle="modal" class="nav-link">TOMBSTONES</a></li>

                                </ul>
                            </li>
                            <li class="nav-item"><a href="{{url('/claims')}}" class="nav-link">Claims</a></li>

                            <li class="nav-item"><a href="{{url('/myPolicy')}}" class="nav-link">My Policy</a></li>
                            <li class="nav-item"><a href="{{url('/contact')}}" class="nav-link">Contact Us</a></li>

                        </ul>
                        <div class="others-option">

                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>


</header>

