@extends("layouts.authentication")
@section("content")

    <div class="main-wrapper">
        <div class="login-page">
            <div class="login-body container">

                <div class="loginbox">
                    <div class="login-right-wrap">
                        <div class="account-header">
                            <div class="account-logo text-center mb-4">
                                <a href="{{ url('/') }}">
                                    <img src="{{asset('public/frontend/img/atlantic_logo.png')}}" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>
                        <div class="login-header">
                            <h2>Welcome </h2>
                            <p class="text-muted">Login to the Atlantic Funeral portal</p>
                        </div>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group">
                                <label class="control-label">Username</label>
                                <input id="email" class="form-control @error('email') is-invalid @enderror" type="email"  name="email"  value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Enter your email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>



                            <div class="form-group mb-4">
                                <label class="control-label">Password</label>
                                <input class="form-control" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Enter your password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-block account-btn">
                                    {{ __('Login') }}
                                </button>
                            </div>

{{--                            @if (Route::has('password.request'))--}}
{{--                                <a class="btn btn-link" href="{{ route('password.request') }}">--}}
{{--                                    {{ __('Forgot Your Password?') }}--}}
{{--                                </a>--}}
{{--                            @endif--}}
                        </form>



{{--                        <div class="text-center dont-have">Don’t have an account? <a href="{{ route('register') }}">Register</a></div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
