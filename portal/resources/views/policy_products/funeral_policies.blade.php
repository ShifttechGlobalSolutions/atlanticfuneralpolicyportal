@extends("layouts.master")
@section("content")


    <div class="page-title-area page-title-bg3">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2>Funeral Policies</h2>
                        <ul>
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li>Funeral Policies</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section class="insurance-details-area ptb-70">
        <div class="container">

            <div class="insurance-details-desc">
                <h3>Funeral Policy Plans</h3>
                <div class="row ">

                    <div class=" about-content col-lg-4 col-md-4">


                        <p> We are well-known for our extremely competitive rates for funeral cover and funeral insurance. We know how important a dignified funeral
                            is and that every family deserves to grieve without worry. That is why our strategy of regular rate comparisons keeps us ahead of other
                            competitors – ensuring our clients pay less and receive more.   We have a solid reputation as a reliable and trustworthy company to work with. Our clients include:</p>

                        <ul class="features-list">
                            <li><i class="fas fa-check"></i> Employees</li>
                            <li><i class="fas fa-check"></i> Individuals</li>
                            <li><i class="fas fa-check"></i> Church Schemes</li>
                            <li><i class="fas fa-check"></i> Society Groups</li>
                        </ul>
                        <div>
                            <a href="{{url('/contact')}}" class="default-btn">Contact Us <span></span></a>
                        </div>

                    </div>
                    <div class="col-lg-8 col-md-8 ">




                            <div class="row align-items-center">


                                    <div class=" quote-list-tab">


                                            <em style="color: #003a5d">*Competitive rates are available on all Single member, Single parent and Families.</em>
                                        <div class="tab ">
                                            <ul class="tabs">
                                                <li><a href="#">Single Plans</a></li>
                                                <li><a href="#">Married Plans </a></li>


                                            </ul>
                                            <div class="tab_content">

                                                <div class="tabs_item">
                                                    <form  method="POST" action="{{ url('filterCover') }}" >
                                                        @csrf
                                                        <div class="form-group col">
                                                            <select id="search" name="coverAmount" class="form-control" onchange="this.form.submit()">
                                                                <option value="">Select Cover Amount</option>
                                                                <option value="5000">R5000.00</option>
                                                                <option value="7000">R7000.00</option>
                                                                <option value="10000">R10000.00</option>
                                                                <option value="15000">R15000.00</option>
                                                                <option value="20000">R20000.00</option>
                                                                <option value="25000">R25000.00</option>
                                                                <option value="30000">R30000.00</option>
                                                            </select>
                                                        </div>
                                                    </form>

                                                <div class="table-responsive">
                                                    <div class="table-wrapper">
                                                        <table class="table  rates-table ">
                                                            <thead>
                                                            <tr>

                                                                <th>PLAN</th>
                                                                <th>AGE RANGE</th>
                                                                <th>COVER</th>
                                                                <th>PREMIUM</th>
                                                                <th>ENTRY FEE</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($single_standard_plans as $single_standard)
{{--                                                                @if($single_standard->coverAmount =='5000')--}}
                                                            <tr>

                                                                <td>{{$single_standard->planType}}</td>
                                                                <td>{{$single_standard->ageRange}}</td>
                                                                <td>{{$single_standard->coverAmount}}</td>
                                                                <td>{{$single_standard->premium}}</td>
                                                                <td>{{$single_standard->entryFee}}</td>


                                                            </tr>
{{--                                                                @endif--}}



                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                            {!! $single_standard_plans->links() !!}

                                                    </div>
                                                </div>
                                                </div>
                                                <div class="tabs_item">

                                                    <div class="table-responsive">
                                                        <div class="table-wrapper">
                                                            <table class="table  rates-table ">
                                                                <thead>
                                                                <tr>

                                                                    <th>PLAN</th>
                                                                    <th>AGE RANGE</th>
                                                                    <th>COVER</th>
                                                                    <th>PREMIUM</th>
                                                                    <th>ENTRY FEE</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach($married_standard_plans as $married_standard)
                                                                    <tr>

                                                                        <td>{{$married_standard->planType}}</td>
                                                                        <td>{{$married_standard->ageRange}}</td>
                                                                        <td>{{$married_standard->coverAmount}}</td>
                                                                        <td>{{$married_standard->premium}}</td>
                                                                        <td>{{$married_standard->entryFee}}</td>


                                                                    </tr>



                                                                @endforeach
                                                                </tbody>
                                                            </table>

                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>

                                    </div>

                            </div>

                    </div>


                </div><br>

            </div>
        </div>
    </section>
    </script>
{{--    <script type="text/javascript">--}}
{{--        $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });--}}
{{--    </script>--}}
    <script type="text/javascript">
        function getval(sel)
        {
         // alert(sel.value);
            $coverAmount =sel.value;
            console.log($coverAmount)
            alert($coverAmount)
            $.ajax({
                type: 'post',
                url: '{{url("policy_products/funeral_policies")}}',
                data: {'coverAmount': $coverAmount},
                //cache: false,
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },

                success: function(response){ // What to do if we succeed

                       alert($coverAmount);
                       // alert(response);
                    },
                error: function(response){
                    alert('Error'+response);
                }
            });

        }

        {{--$('#c').on('keyup',function() {--}}
        {{--    alert(sel.value);--}}
        {{--    $value = $(this).val();--}}
        {{--    $.ajax({--}}
        {{--        type: 'get',--}}
        {{--        url: '{{URL::to('search')}}',--}}
        {{--        data: {'search': $value},--}}
        {{--        success: function (data) {--}}
        {{--            $('tbody').html(data);--}}
        {{--        }--}}
        {{--    });--}}
        {{--})--}}

    </script>
@endsection
