@extends("layouts.master")
@section("content")


    <div class="page-title-area page-title-bg3">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2>Cash Funerals</h2>
                        <ul>
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li>Cash Funerals</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section class="insurance-details-area ptb-100">
        <div class="container">
            <div class="insurance-details-header">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-12">
                        <div class="content">
                            <h3>Business policy is a multiple-line insurance policy</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. Randomised words which don't slightly believable.</p>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some for.</p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="image text-center">
                            <img src="{{asset('public/frontend/img/services-image/2.jpg')}}" alt="image">
                        </div>
                    </div>
                </div>
            </div>
            <div class="insurance-details-desc">
                <h3>Four major elements that we offer:</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <ul class="features-list">
                            <li><i class="fas fa-check"></i> Scientific Skills For getting a better result</li>
                            <li><i class="fas fa-check"></i> Communication Skills to getting in touch</li>
                            <li><i class="fas fa-check"></i> A Career Overview opportunity Available</li>
                            <li><i class="fas fa-check"></i> A good Work Environment For work</li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <ul class="features-list">
                            <li><i class="fas fa-check"></i> Scientific Skills For getting a better result</li>
                            <li><i class="fas fa-check"></i> Communication Skills to getting in touch</li>
                            <li><i class="fas fa-check"></i> A Career Overview opportunity Available</li>
                            <li><i class="fas fa-check"></i> A good Work Environment For work</li>
                        </ul>
                    </div>
                </div>
                <h3>Setting the mood with incense</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                <ul class="wp-block-gallery columns-3">
                    <li class="blocks-gallery-item">
                        <figure>
                            <img src="{{asset('public/frontend/img/services-image/1.jpg')}}" alt="image">
                        </figure>
                    </li>
                    <li class="blocks-gallery-item">
                        <figure>
                            <img src="{{asset('public/frontend/img/services-image/2.jpg')}}" alt="image">
                        </figure>
                    </li>
                    <li class="blocks-gallery-item">
                        <figure>
                            <img src="{{asset('public/frontend/img/services-image/3.jpg')}}" alt="image">
                        </figure>
                    </li>
                </ul>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                <blockquote class="wp-block-quote">
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                    <cite>Tom Cruise</cite>
                </blockquote>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                <div class="pearo-post-navigation">
                    <div class="prev-link-wrapper">
                        <div class="info-prev-link-wrapper">
                            <a href="#">
<span class="image-prev">
<img src="{{asset('public/frontend/img/services-image/1.jpg')}}" alt="image">
<span class="post-nav-title">Prev</span>
</span>
                                <span class="prev-link-info-wrapper">
<span class="prev-title">Health Insurance</span>
<span class="meta-wrapper">
<span class="date-post">January 21, 2021</span>
</span>
</span>
                            </a>
                        </div>
                    </div>
                    <div class="next-link-wrapper">
                        <div class="info-next-link-wrapper">
                            <a href="#">
<span class="next-link-info-wrapper">
<span class="next-title">Life Insurance</span>
<span class="meta-wrapper">
<span class="date-post">January 21, 2021</span>
</span>
</span>
                                <span class="image-next">
<img src="{{asset('public/frontend/img/services-image/3.jpg')}}" alt="image">
<span class="post-nav-title">Next</span>
</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
