@extends("layouts.master")
@section("content")

    <div class="home-area home-slides-two owl-carousel owl-theme">
        <div class="banner-section item-bg3">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="main-banner-content">
                            <span class="sub-title">Funeral cover</span>
                            <h2 style="color: ghostwhite">Plan ahead and protect the ones you love.</h2>
                            <p>Choosing funeral cover from Atlantic allows you to take control and protect you and your
                                family from additional expenses and challenges while grieving.</p>
                            <div class="btn-box">
                                <a href="{{url('/applications')}}" class="default-btn">Buy Cover Online
                                    <span></span></a>


                                <a href="#callMeBack" data-toggle="modal" class="optional-btn"> Call me back
                                    <span></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <section class="about-area ptb-100 bg-f8f8f8">
        <div class="container">
            <div class="row ">
                <div class="col-lg-6 col-md-12">
                    <div class="about-image">
                        <img class="roundrect" src="{{asset('public/frontend/img/hands.webp')}}" style="height: 350px"
                             alt="image">
                        {{--                        <img src="{{asset('public/frontend/img/about-image/about.png')}}" alt="image">--}}
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="about-content">
                        <span>About Us</span>
                        <h3>Funeral, Undertakers & Financial Service Providers</h3>
                        <p>Atlantic Funeral Service and Burial Fund are an Authorised Financial Services Provider in
                            terms of section
                            8 of the financial advisory and intermediary services act, 2002 (Act No. 37 of 2002) subject
                            to the conditions and restrictions
                            set out in the Annexure. Atlantic Funeral Service has been in existence since 1st August
                            2001 and has over 20 years experience in the funeral industry.</p>
                        <a href="{{url('/contact')}}" class="default-btn">Contact Us <span></span></a>
                    </div>
                </div>
            </div>
            <div class="about-inner-area">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="about-text-box" style="height: 227px">
                            <h3>Our Vision</h3>
                            <p>To be the only choice and unique Funeral Services Provider in our chosen market,
                                rendering a world class service to ALL.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="about-text-box" style="height: 227px">
                            <h3>Our Values</h3>
                            <p>Atlantic Funeral Services brand is your assurance of quality, value, caring service and
                                remarkable customer service and peace of mind.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 offset-lg-0 offset-md-3 offset-sm-3 col-sm-6">
                        <div class="about-text-box" style="height: 227px">
                            <h3>Our Mission</h3>
                            <p>To innovatively provide outstanding service and satisfaction to our clients through the
                                utilisation of professional, courteous staff members as well as technologically advanced
                                systems.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="about-area ptb-70 bg-f8f8f8">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-12">
                    <div class="about-title">

                        <h2>Our Funeral Services</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="about-text">
                        <p>We have 4 funeral products, each with its own unique benefits, which you can choose from to
                            suit your needs: </p>
{{--                        <a href="{{url('/contact')}}" class="read-more-btn">Contact Us <i--}}
{{--                                class="flaticon-right-chevron"></i></a>--}}
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </section>
    <section class="services-area bg-f8f8f8 pbt-70">
        <div class="container">
            <div class="services-slides owl-carousel owl-theme">
                <div class="single-services-box" style="height: 524px">
                    <div class="icon">
                        <i class="flaticon-insurance"></i>
                        <div class="icon-bg">
                            <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">
                            <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">
                        </div>
                    </div>
                    <h3><a href="">Funeral Policies </a></h3>
                    <p>We offer various funeral policies to suit you budget. Our policies are structured to accomodate
                        everyone in need
                        of assurance and would gladly assist you in choosing the right plan for you and your loved
                        ones.</p>
                    <a href="#callMeBack" data-toggle="modal" class="read-more-btn">Learn More</a>
                    <div class="box-shape">
                        <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">
                        <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">
                    </div>
                </div>
                <div class="single-services-box" style="height: 524px">
                    <div class="icon">
                        <i class="flaticon-insurance"></i>
                        <div class="icon-bg">
                            <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">
                            <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">
                        </div>
                    </div>
                    <h3><a href="">Funeral Arrangements</a></h3>
                    <p>We cater for all funeral arrangements for a complete peace of mind.
                        We believe that your loved one should be layed to rest with dignity and compassion.
                        Our experience and expertise puts us up there with the best.
                        Contact us and we will be more than happy to assist you.</p>
                    <a href="#callMeBack" data-toggle="modal" class="read-more-btn">Learn More</a>
                    <div class="box-shape">
                        <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">
                        <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">
                    </div>
                </div>
                <div class="single-services-box" style="height: 524px">
                    <div class="icon">
                        <i class="flaticon-insurance"></i>
                        <div class="icon-bg">
                            <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">
                            <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">
                        </div>
                    </div>
                    <h3><a href="">Cash Funerals</a></h3>
                    <p> For the times when death unexpectingly comes into your life.
                        We offer cash funerals to suit your budget. contact us today to discuss in more detail.</p>
                    <a href="#callMeBack" data-toggle="modal" class="read-more-btn">Learn More</a>
                    <div class="box-shape">
                        <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">
                        <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">
                    </div>
                </div>
                <div class="single-services-box" style="height: 524px">
                    <div class="icon">
                        <i class="flaticon-insurance"></i>
                        <div class="icon-bg">
                            <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">
                            <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">
                        </div>
                    </div>
                    <h3><a href="">Cremations</a></h3>
                    <p>Atlantic Funeral Services offers the option of cremation. When a loved one passes on,
                        we are left with so much decisions to make. Let us assist you in making the right choice.</p>
                    <a href="#callMeBack" data-toggle="modal" class="read-more-btn">Learn More</a>
                </div>
                <div class="single-services-box" style="height: 524px">
                    <div class="icon">
                        <i class="flaticon-insurance"></i>
                        <div class="icon-bg">
                            <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">
                            <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">
                        </div>
                    </div>
                    <h3><a href="">Tombstones</a></h3>
                    <p>We offer tombstones in various sizes and designs. We can design it in accordance with how you
                        would like it.
                        There are also various options to choose from depending on your budget.</p>
                    <a href="#callMeBack" data-toggle="modal" class="read-more-btn">Learn More</a>
                    <div class="box-shape">
                        <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">
                        <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="why-choose-us-area">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-12 col-md-12">
                    <div class="why-choose-us-content">
                        <div class="content">


                            <div class="title ">
                                <span class="sub-title">Your Benefits</span>
                                <h2>Why Choose Us</h2>
                                <p> We Can Guide You Through The Process.</p>
                            </div>
                            <ul class="features-list row">
                                <li>
                                    <div class="icon">
                                        <i class="flaticon-like"></i>
                                    </div>
                                    <span>25 Years of experience</span>
                                    {{--                                   <p style="color: white"> We have cultivated an understanding of the funeral insurance needs for South Africans from all walks of life.</p>--}}

                                </li>
                                <li>
                                    <div class="icon">
                                        <i class="flaticon-customer-service"></i>
                                    </div>
                                    <span>Pesronalised Services</span>
                                    {{--                                   <p style="color: white"> We exceed client expectations by being attentive and proactive</p>--}}

                                </li>
                                <li>
                                    <div class="icon">
                                        <i class="flaticon-care"></i>
                                    </div>
                                    <span>Competitive Rates</span>
                                    {{--                                    <p style="color: white">We are well known in the industry for our extremely competitive rates for funeral services</p>--}}
                                </li>
                                <li>
                                    <div class="icon">
                                        <i class="flaticon-team"></i>
                                    </div>
                                    <span>Clients Focused</span>
                                    {{--                                    <p style="color: white">Our service centres assures you of friendly and efficient service and assistance with your insurance needs.</p>--}}
                                </li>
                                <li>
                                    <div class="icon">
                                        <i class="flaticon-policy"></i>
                                    </div>
                                    <span>Reputation</span>
                                    {{--                                   <p style="color: white"> We are a respected and trusted name in insurance.</p>--}}
                                </li>
                                <li>
                                    <div class="icon">
                                        <i class="flaticon-education"></i>
                                    </div>
                                    <span>Simplicity</span>
                                    {{--                                   <p style="color: white" > We keep things as simple and easy as possible and make buying insurance clear and simple.</p>--}}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="quote-area ptb-100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-12">
                    <div class="quote-content">
                        <h2>Get a free quote</h2>
                        <div class="image">
                            <img class="roundrect" src="{{asset('public/frontend/img/contact.jpg')}}" alt="image">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="tab quote-list-tab">
                        <ul class="tabs">
                            <li><a>Call Me Back</a></li>


                        </ul>
                        <div class="tab_content">
                            <div class="tabs_item">

                                <p>Complete the form below and one of our friendly consultants will call you back.</p>
                                <form class="p-3" method="POST" action="{{ url('call_back') }}">
                                    @csrf
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="name"
                                               placeholder="enter your Name" required>
                                    </div>

                                    <div class="form-group">
                                        <input type="text" class="form-control" name="phoneNumber"
                                               placeholder="enter your Phone" required>
                                    </div>

                                    <button type="submit" class="default-btn">CALL ME BACK <span></span></button>
                                </form>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
