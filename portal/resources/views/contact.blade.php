@extends("layouts.master")
@section("content")


    <div class="page-title-area page-title-bg3">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2>Contact</h2>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>Contact</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section class="contact-area ptb-100">
        <div class="container">
            <div class="section-title">

                <h3>Do you have any questions or queries,<br> feel free to email us or visit us.</h3>
{{--                <p>If you have an idea, we would love to hear about it.</p>--}}
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="contact-form">
                        <form class="p-3" method="POST" action="{{ url('contactUs') }}" >
                            @csrf
                            <div class="row">
                                <div class="col-lg-12 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="name"  class="form-control" required data-error="Please enter your name" placeholder="enter your Name">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-6">
                                    <div class="form-group">
                                        <input type="email" name="email"  class="form-control" required data-error="Please enter your email" placeholder="enter your Email">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <input type="text" name="phoneNumber"  required data-error="Please enter your number" class="form-control" placeholder="enter your Phone">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <textarea name="message" class="form-control"  cols="30" rows="6" required data-error="Write your message" placeholder="enter your  Message"></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <button type="submit" class="default-btn">Send Message <span></span></button>
                                    <div id="msgSubmit" class="h3 text-center hidden"></div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div id="map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3311.276799876115!2d18.507971314770597!3d-33.90827488064531!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1dcc5c6e1965d615%3A0x44606906a458a696!2s16%20Factreton%20Ave%2C%20Kensington%2C%20Cape%20Town%2C%207405!5e0!3m2!1sen!2sza!4v1651219312411!5m2!1sen!2sza"></iframe>
                </div>
            </div>

        </div>
        <div class="bg-map"><img src="{{asset('public/frontend/img/bg-map.png')}}" alt="image"></div>
        </div>
    </section>

@endsection
