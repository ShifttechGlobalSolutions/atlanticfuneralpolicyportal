@extends('layouts.portalMaster')

@section('content')
    <div class="page-wrapper">
        <div class="content ">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Tutorials</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Agents</a></li>
                            <li class="breadcrumb-item active">Tutorials</li>
                        </ul>
                    </div>
                </div>
            </div>


            <div class="az-content-body pd-lg-l-40 d-flex flex-column">
                <div class="page-header">
                    <div class="row">
                        <div class="col">
                            <h3 class="page-title">Get started with video tutorials</h3>
                        </div><hr>
                        <div class="col-auto text-right">
                            <a class="btn btn-light" href="{{ url('clear-search') }}">
                                <i class="fa fa-times">&nbsp;&nbsp;Clear Filter</i>
                            </a>
                            <a class="btn btn-outline-primary" href="" id="filter_search" data-target="#searchModal" data-toggle="modal">
                                <i class="fas fa-filter">&nbsp;&nbsp;Filter</i>
                            </a>
                        </div>
                        </div>
                    </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-hover table-center mb-0 datatable">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Tutorial Name</th>
                                            <th>Description</th>

                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($Items as $items)
                                            <tr>
                                                <td>{{ ++$i }}</td>

                                                <td>{{$items->title }}</td>
                                                <td>{{ $items->description }}</td>



                                                <td>
                                                    <a href="{{ asset('public/storage/videos/'.$items->file_path) }}" class="btn btn-sm bg-info-light">
                                                        <i class="far fa-edit mr-1"></i> Play Tutorial
                                                    </a>
                                                </td>
                                            </tr>


                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




{{--                @foreach($Items as $items)--}}
{{--                <div class="container">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col">--}}
{{--                                <h5>{{ $items->title }}</h5>--}}
{{--                                <video  width="280" height="200" controls controlsList="nodownload">--}}
{{--                                    <source src="{{ asset('public/storage/videos/'.$items->file_path) }}" type="video/mp4">--}}
{{--                                    <source src="#" type="video/mp4">--}}
{{--                                </video>--}}
{{--                                <p>{{ $items->description }}</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                @endforeach--}}
{{--            {!! $Items->links() !!}--}}
        </div>
    </div>


    {{-- modal Form --}}
    <div id="searchModal" class="modal fade" data-backdrop="static" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header"style="background-color: #0C0457;color:white">
                    <h5 class="modal-title">Search for Video</h5><br>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <form class="needs-validation" method="POST" action="{{ url('search-tutorials') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="row" style="text-align: left">
                            <div class="col">
                                <div class="input-group">
                                    <input type="text" name="searchItem" class="form-control" placeholder="Video title...">
                                    <span class="input-group-btn">
                                          <button type="submit" class="btn btn-primary" type="button" style="height:40px;width:40px"><i class="fa fa-search"></i></button>
                                        </span>
                                </div><!-- input-group -->
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection
