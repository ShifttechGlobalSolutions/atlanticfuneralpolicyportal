@extends('layouts.portalMaster')

@section('content')
    <div class="page-wrapper">

        <div class="content container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Policies</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href=". url('/home') .">Agent</a></li>
                            <li class="breadcrumb-item active">Policies</li>
                        </ul>
                    </div>
                    <div class="col-auto text-right">
                        <a class="btn btn-light filter-btn" href="{{ url('clear-search') }}" data-toggle="tooltip" title="Clear Search">
                            <i class="fas fa-times"></i>
                        </a>
                        <a class="btn btn-white filter-btn" href="javascript:void(0);" id="filter_search" data-toggle="tooltip" title="Search Policy">
                            <i class="fas fa-filter"></i>
                        </a>
                        <a href="{{  url('agents/create_policy') }} " class="btn btn-primary add-button ml-3" data-toggle="tooltip" title="Create New Policy">
                            <i class="fas fa-plus"></i>
                        </a>
                    </div>

                </div>
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{Session::get('success')}}
                    </div>
                @endif
            </div>

            <div class="card filter-card" id="filter_inputs">
                <div class="card-body pb-0">
                    <form method="POST" action="{{route('agent-search') }}"  enctype="multipart/form-data"/>
                    @CSRF
                    <div class="row filter-row">
                        <div class="col-sm-6 col-md-6">
                            <div class="form-group">
                                <label class="col-form-label">Search for an Agent(s)</label>
                                <input class="form-control" name="agents" type="text">
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-3">

                            <div class="form-group">
                                <button class="btn btn-primary btn-block" type="submit">Submit</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>

            <ul class="nav nav-tabs menu-tabs">
                <li class="nav-item active">
                    <a class="nav-link" data-toggle="tab" href="#all_agents">All Policies <span class="badge badge-primary">{{ $allPolicies}}</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#active_agents">Active Policies <span class="badge badge-primary">{{ $countActivePolicies}}</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#inactive_agents">InActive Policies <span class="badge badge-primary">{{ $countInactivePolicies}}</span></a>
                </li>


            </ul>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content profile-tab-cont">
                                <div class="table-responsive tab-pane show active" id="all_agents">
                                    <table class="table table-hover table-center mb-0 datatable">
                                        <thead>
                                        <tr>
                                            <th>#</th>

                                            <th>Fullname</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($members as $policies)
                                            <tr>
                                                <td>{{ $policies->id }}</td>

                                                <td>{{ $policies->client_firstname }}</td>
                                                <td>{{ $policies->email }}</td>
                                                <td>{{ $policies->cell}}</td>
                                                <td class="text">{{ $policies->status }}

                                                </td>
                                                <td>
                                                    <a href="{{ url('agents/policy_details/'.$policies->policy_no) }}" class="btn btn-info ">
                                                        <i class="far fa-eye mr-1"></i> View
                                                    </a>
{{--                                                    <a href="{{ url('admin/delete-agent/'.$policies->id) }}" class="btn btn-danger">--}}
{{--                                                        <i class="far fa-trash-alt"></i> Delete--}}
{{--                                                    </a>--}}
                                                </td>
                                            </tr>
                                        @endforeach
{{--                                        {!! $policies->links() !!}--}}

                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-responsive tab-pane fade" id="active_agents">
                                    <table class="table table-hover table-center mb-0 datatable">
                                        <thead>
                                        <tr>
                                            <th>#</th>

                                            <th>Fullname</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($activePolicies as $active_policies)
                                            <tr>
                                                <td>{{ $active_policies->id }}</td>
                                                <td>
                                                    <a href="service-details.html">
                                                        <img class="rounded service-img mr-1" src="asset('storage/app/public/images/avatars/'.$activeagents1->image" alt="">
                                                    </a>
                                                </td>
                                                <td>{{ $active_policies->client_firstname }}</td>
                                                <td>{{ $active_policies->email }}</td>
                                                <td>{{ $active_policies->cell}}</td>
                                                <td class="text">{{ $active_policies->status }}

                                                </td>
                                                <td>
                                                    <a href="{{ url('agents/policy_details/'.$active_policies->policy_no) }}" class="btn btn-info ">
                                                        <i class="far fa-eye mr-1"></i> View
                                                    </a>
{{--                                                    <a href="{{ url('admin/claims/view-claims/'.$claims->id) }}" class="btn btn-danger">--}}
{{--                                                        <i class="far fa-trash-alt"></i> Delete--}}
{{--                                                    </a>--}}
                                                </td>
                                            </tr>
                                        @endforeach
{{--                                        {!! $active_policies->links() !!}--}}

                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-responsive tab-pane fade" id="inactive_agents">
                                    <table class="table table-hover table-center mb-0 datatable">
                                        <thead>
                                        <tr>
                                            <th>#</th>

                                            <th>Fullname</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($inactivePolicies as $inactive_policies)
                                            <tr>
                                                <td>{{ $inactive_policies->id }}</td>
                                                <td>
                                                    <a href="service-details.html">
                                                        <img class="rounded service-img mr-1" src="asset('storage/app/public/images/avatars/'.$inactiveagents1->image" alt="">
                                                    </a>
                                                </td>
                                                <td>{{ $inactive_policies->client_firstname }}</td>
                                                <td>{{ $inactive_policies->email }}</td>
                                                <td>{{ $inactive_policies->cell}}</td>
                                                <td class="text">{{ $inactive_policies->status }}

                                                </td>
                                                <td>
                                                    <a href="{{ url('agents/policy_details/'.$inactive_policies->policy_no) }}" class="btn btn-info ">
                                                        <i class="far fa-eye mr-1"></i> View
                                                    </a>
{{--                                                    <a href="{{ url('admin/claims/view-claims/'.$claims->id) }}" class="btn btn-danger">--}}
{{--                                                        <i class="far fa-trash-alt"></i> Delete--}}
{{--                                                    </a>--}}
                                                </td>
                                            </tr>
                                        @endforeach
{{--                                        {!! $inactive_policies->links() !!}--}}

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
