@extends('layouts.portalMaster')

@section('content')


    <div class="page-wrapper">
        <div class="content container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Commissions</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Agent</a></li>
                            <li class="breadcrumb-item active">Commissions</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">

                    <div class="profile-header">
                        <div class="row align-items-center">
                            <div class="col-auto profile-image">
                                <a href="#">
                                    <img class="rounded-circle" alt="User Image" src="{{asset('public/frontend/img/log.png')}}">
                                </a>
                            </div>
                            <div class="col ml-md-n2 profile-user-info">

                                <h4 class="user-name mb-0">No Commissions yet</h4>


                            </div>

                        </div>
                    </div>






                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
