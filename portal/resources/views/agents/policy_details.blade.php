@extends('layouts.portalMaster')


@section('content')
    <div class="page-wrapper">
        <div class="content container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Policy Details</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{url('admin/policies')}}">Policy</a></li>
                            <li class="breadcrumb-item active">Policy Details</li>
                        </ul>
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="col-md-12">

                    <div class="profile-menu">
                        <ul class="nav nav-tabs nav-tabs-solid">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#policy">Policy Holder Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#package">Package Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#spouse">Spouse Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#dependant">Dependant Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#beneficiary">Benefifiary Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#account">Account Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#documents">Documents</a>
                            </li>


                        </ul>
                    </div>
                    <form  enctype="multipart/form-data" id="upload-file" >
                        @CSRF
                        <div class="tab-content profile-tab-cont">
                            @foreach ($member as $policy)
                            @endforeach
                            <div id="policy" class="tab-pane fade show active">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Policy Holder Details</h5><br>
                                        <div class="row">
                                            <div class="col-md-4 ">

                                                <div class="form-group">
                                                    <label>Policy No</label>
                                                    <input type="text" name="policyHolderId" class="form-control" value="{{$policy->policy_no}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Surname</label>
                                                    <input type="text" name="policyHolderSurname" class="form-control" value="{{$policy->client_lastname}}"readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>First Name</label>
                                                    <input type="text" name="policyHolderFullName" class="form-control" value="{{$policy->client_firstname}}"readonly>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-md-4">

                                                <div class="form-group">
                                                    <label>Date of Birth</label>
                                                    <input type="text" name="policyHolderDateOfBirth"
                                                           class="form-control" value="{{$policy->client_dob}}"readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>ID Number</label>
                                                    <input type="text" name="policyHolderEmail" class="form-control" value="{{$policy->client_id_number}}"readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Gender</label>
                                                    <input type="text" name="policyHolderIdNumber" class="form-control" value="{{$policy->gender}}"readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Street Address</label>
                                                    <input type="text" name="policyHolderIdNumber" class="form-control" value="{{$policy->strName}}"readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Suburb</label>
                                                    <input type="text" name="policyHolderPostalAddress"
                                                           class="form-control" value="{{$policy->surburb}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Postal Code</label>
                                                    <input type="text" name="policyHolderResidentialAddress"
                                                           class="form-control" value="{{$policy->postalCode}}" readonly>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Home Tel</label>
                                                    <input type="text" name="policyHolderIdNumber" class="form-control" value="{{$policy->home}}"readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Work Tel</label>
                                                    <input type="text" name="policyHolderPostalAddress"
                                                           class="form-control" value="{{$policy->work}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Cellphone</label>
                                                    <input type="text" name="policyHolderPostalAddress"
                                                           class="form-control" value="{{$policy->cell}}" readonly>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="row">
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Email Address</label>
                                                    <input type="text" name="policyHolderResidentialAddress"
                                                           class="form-control" value="{{$policy->email}}" readonly>
                                                </div>

                                            </div>



                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div id="package" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Package Details</h5><br>
                                        <div class="row">


                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Package</label>
                                                    <input type="text" name="deceasedSurname" class="form-control" value="{{$policy->package}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Cover Amount</label>
                                                    <input type="text" name="deceasedFullName" class="form-control" value="R{{$policy->coverAmount}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Age Group</label>
                                                    <input type="tel" name="deceasedContactNumber" class="form-control" value="{{$policy->ageGroup}}" readonly>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">


                                            <div class="col-md-4">

                                                <div class="form-group">
                                                    <label>Marital Status</label>
                                                    <input type="text" name="deceasedLastAddress" class="form-control" value="{{$policy->marital}}" readonly>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="spouse" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Spouse Details</h5><br>
                                        <div class="row">
                                            <div class="col-md-4 ">

                                                <div class="form-group">
                                                    <label>Surname</label>
                                                    <input type="text" name="claimantSurname" class="form-control" value="{{$policy->spouse_lastname}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" name="claimantFullName" class="form-control" value="{{$policy->spouse_lastname}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Date of Birth</label>
                                                    <input type="text" name="claimantDateOfBirth" class="form-control" value="{{$policy->spouse_dob}}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>ID Passport Number </label>
                                                    <input type="text" name="claimantContactNumber"
                                                           class="form-control" value="{{$policy->spouse_idNumber}}" readonly>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div id="dependant" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Dependant Details</h5><br>
                                        <div class="row">
                                            <div class="form-group col-md-4 ">
                                                <label>Surname</label>
                                                <input type="text" name="funeralParlourName" class="form-control" value="{{$policy->dep_lname}}" readonly>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>Full Name</label>
                                                <input type="text" name="funeralParlourPhone" class="form-control" value="{{$policy->dep_fname}}" readonly>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>Date of Birth</label>
                                                <input name="beneficiaryName" type="text" class="form-control" value="{{$policy->dep_dob}}">
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div id="beneficiary" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Beneficiary Details</h5><br>
                                        <div class="row">
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Surname</label>
                                                    <input type="text" name="deceasedSurname" class="form-control" value="{{$policy->ben_lastname}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" name="deceasedFullName" class="form-control" value="{{$policy->ben_firstname}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>ID/ Passport Number</label>
                                                    <input type="tel" name="deceasedContactNumber" class="form-control" value="{{$policy->ben_idNumber}}" readonly>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Date Of Birth</label>
                                                    <input type="text" name="deceasedSurname" class="form-control" value="{{$policy->ben_dob}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Cellphone</label>
                                                    <input type="text" name="deceasedFullName" class="form-control" value="{{$policy->ben_contact_number}}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div id="account" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Account Details</h5>
                                        <div class="row">
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Bank Namek</label>
                                                    <input type="text" name="deceasedSurname" class="form-control" value="{{$policy->bankName}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Branch Name</label>
                                                    <input type="text" name="deceasedFullName" class="form-control" value="{{$policy->branchName}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Branch Code</label>
                                                    <input type="tel" name="deceasedContactNumber" class="form-control" value="{{$policy->branchCode}}" readonly>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Account Number</label>
                                                    <input type="tel" name="deceasedContactNumber" class="form-control" value="{{$policy->accNumber}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label>Account Type</label>
                                                    <input type="text" name="deceasedSurname" class="form-control" value="{{$policy->accType}}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div id="documents" class="tab-pane fade">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-3 ">
                                                    <div class="form-group"><br>
                                                        <p><a href="{{asset('storage/documents/members/'.$policy->client_idCopy) }}" >
                                                                <i class="fa fa-download"></i>
                                                            </a>ID Number/ Passport Copy
                                                        </p>
                                                    </div>
                                                </div>

{{--                                                    <div class="form-group col-md-4 ">--}}
{{--                                                        <br><p><a href="{{asset('storage/app/public/images/members/'.$policy->claimant_idCopy )}}" >--}}
{{--                                                                <i class="fa fa-download"></i>--}}
{{--                                                            </a>Claimant Id Copy--}}

{{--                                                        </p>--}}
{{--                                                    </div>documents/dependents--}}

                                                <div class="form-group col-md-4 ">
                                                    <br><p><a href="{{asset('storage/documents/dependents/'.$policy->dep_copiesofid )}}" >
                                                            <i class="fa fa-download"></i>
                                                        </a>Dependent Id Copy

                                                    </p>
                                                </div>


                                            </div>


                                        </div>


                                    </div>
                                </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>


@endsection
