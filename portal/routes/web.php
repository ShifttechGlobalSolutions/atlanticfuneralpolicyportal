<?php

use App\Http\Controllers\Admin\AdminAgentsController;
use App\Http\Controllers\Admin\AdminClaimsController;
use App\Http\Controllers\Admin\AdminDashboardController;
use App\Http\Controllers\Admin\AdminNotificationsController;
use App\Http\Controllers\Admin\AdminPoliciesController;
use App\Http\Controllers\Admin\AdminReportsController;
use App\Http\Controllers\Agent\AgentCommisionsController;
use App\Http\Controllers\Agent\AgentDashboardController;
use App\Http\Controllers\Agent\AgentPoliciesController;
// use App\Http\Controllers\Agent\videoTutsController;
use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ClaimController;
use App\Http\Controllers\Clients\BeneficieryController;
use App\Http\Controllers\Clients\ClaimsController;
use App\Http\Controllers\Clients\PaymentController;
use App\Http\Controllers\Clients\PoliciesController;
use App\Http\Controllers\Clients\ProfileController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PolicyController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PackagesController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Agent\VideoTutsController;


// use App\Http\Controllers\Agent\AgentCommisionsController;
// use App\Http\Controllers\Agent\AgentDashboardController;
// use App\Http\Controllers\Agent\AgentPoliciesController;
use App\Http\Controllers\Agent\AgentProfileController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;

// use App\Http\Controllers\Admin\AdminPoliciesController;
// use App\Http\Controllers\Admin\AdminAgentsController;
// use App\Http\Controllers\Admin\AdminClaimsController;
// use App\Http\Controllers\Admin\AdminDashboardController;
// use App\Http\Controllers\Admin\AdminNotificationsController;
// use App\Http\Controllers\Admin\AdminReportsController;
use App\Http\Controllers\Admin\AdminProfileController;
use App\Http\Controllers\Admin\PolicyPlanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');
});
//website_routes
Route::get('portal',[PortalController::class, 'index'])->name('portal');
Route::get('claims',[ClaimController::class, 'index'])->name('claims');
Route::get('myPolicy',[PolicyController::class, 'index'])->name('myPolicy');
Route::get('contact',[ContactController::class, 'index'])->name('contact');
Route::post('contactUs',[ContactController::class, 'sendMessage'])->name('contactUs');
Route::post('call_back',[ContactController::class, 'calls'])->name('call_back');
Route::get('applications',[ApplicationController::class, 'index'])->name('applications');
Route::get('stepper_application',[ApplicationController::class, 'application'])->name('stepper_application');
Route::get('policy_products/cash_funerals',[PackagesController::class, 'cashFunerals'])->name('policy_products/cash_funerals');
Route::get('policy_products/cremations',[PackagesController::class, 'cremations'])->name('policy_products/cremations');
Route::get('policy_products/funeral_arrangements',[PackagesController::class, 'funeralArrangements'])->name('policy_products/funeral_arrangements');



Route::get('policy_products/funeral_policies',[PackagesController::class, 'funeralPolicies'])->name('policy_products/funeral_policies');
Route::post('filterCover',[PackagesController::class, 'funeralPolicies'])->name('filterCover');

Route::get('policy_products/tombstones',[PackagesController::class, 'tombstones'])->name('policy_products/tombstones');

//portal_routes
Route::get('admin/policy-stepper',[AdminPoliciesController::class, 'application'])->name('admin/policy-stepper');
Route::get('stepper-application',[ApplicationController::class, 'adminMail'])->name('stepper-application');
Route::post('admin-mail',[ApplicationController::class, 'adminMail'])->name('admin-mail');
Route::post('agent-mail',[AgentPoliciesController::class, 'agentMail'])->name('agent-mail');
Route::get('agent-policy',[AgentPoliciesController::class, 'agentPolicy'])->name('agent-policy');
Route::post('upload-video',[VideoTutsController::class, 'store'])->name('upload-video');

Auth::routes(['verify' => true]);
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('products', ProductController::class);
});


//client_routes
Route::get('clients/policy',[PoliciesController::class, 'index'])->name('clients/policy');
Route::post('clients/add-beneficiary',[PoliciesController::class, 'new_benefiary'])->name('clients/add-beneficiary');
Route::post('clients/update-beneficiary',[PoliciesController::class, 'beneficiary_update'])->name('clients/update-beneficiary');
Route::post('clients/add-spouse',[PoliciesController::class, 'new_spouse'])->name('clients/add-spouse');
Route::get('clients/beneficiary_details/{id}',[PoliciesController::class, 'beneficiary_details'])->name('clients/beneficiary_details');
Route::get('clients/delete_beneficiary/{id}',[PoliciesController::class, 'delete_beneficiary'])->name('clients/delete_beneficiary');
Route::get('clients/beneficiaries',[BeneficieryController::class, 'index'])->name('clients/beneficiaries');
Route::get('clients/spouse_details/{id}',[PoliciesController::class, 'spouse_details'])->name('clients/spouse_details');
Route::post('clients/update-spouse',[PoliciesController::class, 'spouse_update'])->name('clients/update-spouse');
Route::get('clients/delete_spouse/{id}',[PoliciesController::class, 'delete_spouse'])->name('clients/delete_spouse');
Route::post('clients/update_holder',[PoliciesController::class, 'holder_update'])->name('clients/update_holder');
Route::get('clients/payments',[PaymentController::class, 'index'])->name('clients/payments');
Route::get('clients/profile',[ProfileController::class, 'index'])->name('clients/profile');
Route::get('clients/claims',[ClaimsController::class, 'index'])->name('clients/claims');
Route::get('clients/claim_details/{id}',[ClaimsController::class, 'getClaimDetails'])->name('clients/claim_details');
Route::get('clients/claimForm',[ClaimsController::class, 'getClaimForm'])->name('clients/claimForm');
Route::post('clients/claimForm',[ClaimsController::class, 'captureClaims'])->name('claims.store');

//agent_routes
Route::get('agents/dashboard',[AgentDashboardController::class, 'index'])->name('agents/dashboard');
Route::get('agents/policy',[AgentPoliciesController::class, 'index'])->name('agents/policy');
Route::get('agents/create_policy',[AgentPoliciesController::class, 'createPolicy'])->name('agents/create_policy');
Route::get('agents/policy_details/{id}',[AgentPoliciesController::class, 'showPolicy'])->name('agents/policy_details');
Route::get('agents/commissions',[AgentCommisionsController::class, 'index'])->name('agents/commissions');
Route::get('agents/profile',[AgentProfileController::class, 'index'])->name('agents/profile');
//demo
Route::get('agents/tutorials',[videoTutsController::class, 'tuts'])->name('agents/tutorials');
Route::post('search-tutorials',[videoTutsController::class, 'tutSearch'])->name('search-tutorials');
Route::get('clear-search',[videoTutsController::class, 'clrSearch'])->name('clear-search');



Route::get('register',[RegisterController::class, 'index'])->name('register');
Route::get('login',[LoginController::class, 'index'])->name('login');

//admin_routes
Route::get('admin/dashboard',[AdminDashboardController::class, 'index'])->name('admin/dashboard');
//policy plans
Route::get('admin/policy_plans',[PolicyPlanController::class, 'index'])->name('admin/policy_plans');
Route::post('capturePlans',[PolicyPlanController::class, 'capturePlans'])->name('capturePlans');
Route::get('admin/delete_plan/{id}',[PolicyPlanController::class, 'deletePlan'])->name('admin/delete_plan');
Route::post('admin/update_plan',[PolicyPlanController::class, 'updatePlan'])->name('admin/update_plan');

Route::get('admin/policies',[AdminPoliciesController::class, 'index'])->name('admin/policies');
Route::get('admin/agents',[AdminAgentsController::class, 'index'])->name('admin/agents');
Route::get('admin/claims',[AdminClaimsController::class, 'index'])->name('admin/claims');
Route::get('admin/claims/view-claims/{id}',[AdminClaimsController::class, 'viewClaim'])->name('admin/claims/view-claims');
Route::post('claim-filter',[AdminClaimsController::class, 'filterByDate'])->name('claim-filter');
Route::get('admin/delete-agent/{id}',[AdminAgentsController::class, 'deleteAgent'])->name('admin/delete-agent');
Route::get('admin/view-agent/{id}',[AdminAgentsController::class, 'viewAgent'])->name('admin/view-agent');

Route::post('agent-search',[AdminAgentsController::class, 'filterByAgent'])->name('agent-search');
Route::get('admin/reports',[AdminReportsController::class, 'index'])->name('admin/reports');
Route::get('admin/notifications',[AdminNotificationsController::class, 'index'])->name('admin/notifications');
Route::get('admin/profile',[AdminProfileController::class, 'index'])->name('admin/profile');
Route::post('policy-filter',[AdminPoliciesController::class, 'filterByDate'])->name('policy-filter');
Route::get('clear-policy-search',[AdminPoliciesController::class, 'clrSearch'])->name('clear-policy-search');
Route::get('clear-search',[AdminAgentsController::class, 'clrSearch'])->name('clear-search');
Route::get('clear-claim-search',[AdminClaimsController::class, 'clrSearch'])->name('clear-claim-search');
Route::get('admin/delete/{id}',[AdminPoliciesController::class, 'destroy'])->name('admin/delete');
Route::get('admin/show/{id}',[AdminPoliciesController::class, 'show'])->name('admin/show');
Route::get('admin/edit/{id}',[AdminPoliciesController::class, 'show_stepper'])->name('admin/edit');
Route::post('admin/add-video',[VideoTutsController::class, 'store'])->name('admin/add-video');
Route::get('admin/addvideo',[VideoTutsController::class, 'addVid'])->name('admin/addvideo');
Route::post('admin/update-video',[VideoTutsController::class, 'update'])->name('admin/update-video');
Route::post('admin/search-video',[VideoTutsController::class, 'tutSearch'])->name('admin/search-video');
// Route::get('admin/clear-search',[VideoTutsController::class, 'clearSearch'])->name('admin/clear-search');
Route::get('admin/tutorials',[VideoTutsController::class, 'videoTuts'])->name('admin/tutorials');
Route::get('admin/delete-video/{id}',[VideoTutsController::class, 'deleteVideo'])->name('admin/delete-video');
Route::get('admin/show-video/{id}',[VideoTutsController::class, 'showVideo'])->name('admin/show-video');

// Route::get('admin/profile',[AgentProfileController::class, 'index'])->name('admin/profile');
// Route::get('admin/upload_image',[AdminProfileController::class, 'imageUpload'])->name('admin/upload_image');
Route::get('admin/profile_update',[AgentProfileController::class, 'update'])->name('admin/profile_update');
Route::post('admin/profile_updated',[AgentProfileController::class, 'updatedRecord'])->name('admin/profile_updated');
Route::post('admin/password_update',[AgentProfileController::class, 'updatePassword'])->name('admin/password_update');
Route::post('admin/upload_image',[AdminProfileController::class, 'imageUpload'])->name('admin/upload_image');
Route::post('admin/summary_update',[AgentProfileController::class, 'summaryUpdate'])->name('admin/summary_update');


