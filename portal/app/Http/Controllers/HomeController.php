<?php

namespace App\Http\Controllers;

use App\Models\BeneficiaryInfo;
use App\Models\Claims;
use App\Models\agent_info;
use Illuminate\Http\Request;
use App\Models\HolderInfo;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware('auth');
       $this->middleware('verified');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      // $agent=Auth()->User()->email;

        $policy_no=  HolderInfo::where('email', Auth()->User()->email)->value('policy_no');
        $policy_details=  HolderInfo::where('policy_no', $policy_no)->get();
       // dd($policy_details);

       $allPolicies_count = HolderInfo::count();
       $allAgents_count = agent_info::count();


        $recentPolicies= DB::table('holder_infos')->selectRaw('*')->paginate();
        $gentPolicies= DB::table('holder_infos')->where('agent',Auth()->User()->email)->selectRaw('*')->paginate();
        $countAgentPolicies= DB::table('holder_infos')->where('agent',Auth()->User()->email)->count();
        $videosCount = DB::table('video_tutorials')->selectRaw('*')->count();
       // dd($videosCount);
          $allClaims = Claims::count();

        $recentClaims= DB::table('claims')->selectRaw('*')->paginate();


      $allBeneficiaries =BeneficiaryInfo::where('policy_no', $policy_no)->count();
//
//       // $myPremiums = DB::table('payments')->where('policy_no', $policy_no)->sum('premiumPaid');
        $Beneficiaries =BeneficiaryInfo::where('policy_no', $policy_no)->get();
//       // dd($allBeneficiaries);

    //    $allPol = DB::table('holder_infos')->select('*')
    //    ->whereYear('created_at', date('Y'))
    //    ->groupBy(DB::raw('Month(created_at)'))
    //    ->count();

//    // All Policies
//        $all_Pol = HolderInfo::select(DB::raw("COUNT(*) AS count"))
//       ->whereYear('created_at', date('Y'))
//       ->groupBy(DB::raw('Month(created_at)'))
//       ->pluck('count');
//
//       $polMonth =  HolderInfo::select(DB::raw("Month(created_at) as month"))
//       ->whereYear('created_at', date('Y'))
//       ->groupBy(DB::raw('Month(created_at)'))
//       ->pluck('month');
//
//       $allPol = array(0,0,0,0,0,0,0,0,0,0,0,0);
//       foreach($polMonth as $index => $polMonths){
//            $allPol[$polMonths] = $all_Pol[$index];
//       }
//
//       // Approved Policies
//       $apr = "Active";
//       $apr_Pol = HolderInfo::select(DB::raw("COUNT(*) AS count"))
//       ->whereYear('created_at', date('Y'))
//       ->where('status', $apr)
//       ->groupBy(DB::raw('Month(created_at)'))
//       ->pluck('count');
//
//       $aprMonth =  HolderInfo::select(DB::raw("Month(created_at) as month"))
//       ->whereYear('created_at', date('Y'))
//       ->where('status', $apr)
//       ->groupBy(DB::raw('Month(created_at)'))
//       ->pluck('month');
//
//       $aprPol = array(0,0,0,0,0,0,0,0,0,0,0,0);
//       foreach($aprMonth as $index => $aprMonths){
//            $aprPol[$aprMonths] = $apr_Pol[$index];
//       }
//
//       // Declined Policies
//       $decl = "InActive";
//       $decl_Pol = HolderInfo::select(DB::raw("COUNT(*) AS count"))
//       ->whereYear('created_at', date('Y'))
//       ->where('status', $decl)
//       ->groupBy(DB::raw('Month(created_at)'))
//       ->pluck('count');
//
//       $declMonth =  HolderInfo::select(DB::raw("Month(created_at) as month"))
//       ->whereYear('created_at', date('Y'))
//       ->where('status', $decl)
//       ->groupBy(DB::raw('Month(created_at)'))
//       ->pluck('month');
//
//       $declPol = array(0,0,0,0,0,0,0,0,0,0,0,0);
//       foreach($declMonth as $index => $declMonths){
//            $declPol[$declMonths] = $decl_Pol[$index];
//       }
//
//
//       // All claims
//       $all_Cl = Claims::select(DB::raw("COUNT(*) AS count"))
//       ->whereYear('created_at', date('Y'))
//       ->groupBy(DB::raw('Month(created_at)'))
//       ->pluck('count');
//
//       $clMonth =  Claims::select(DB::raw("Month(created_at) as month"))
//       ->whereYear('created_at', date('Y'))
//       ->groupBy(DB::raw('Month(created_at)'))
//       ->pluck('month');
//
//       $allCl = array(0,0,0,0,0,0,0,0,0,0,0,0);
//       foreach($clMonth as $index => $clMonths){
//            $allCl[$clMonths] = $all_Cl[$index];
//       }
//
//       // Approved Claims
//       $apr = "Approved";
//       $apr_Cl = Claims::select(DB::raw("COUNT(*) AS count"))
//       ->whereYear('created_at', date('Y'))
//       ->where('status', $apr)
//       ->groupBy(DB::raw('Month(created_at)'))
//       ->pluck('count');
//
//       $aprClMonth =  Claims::select(DB::raw("Month(created_at) as month"))
//       ->whereYear('created_at', date('Y'))
//       ->where('status', $apr)
//       ->groupBy(DB::raw('Month(created_at)'))
//       ->pluck('month');
//
//       $aprCl = array(0,0,0,0,0,0,0,0,0,0,0,0);
//       foreach($aprClMonth as $index => $aprClMonths){
//            $aprCl[$aprClMonths] = $apr_Cl[$index];
//       }
//
//       // Declined Policies
//       $decl = "Declined";
//       $decl_Cl = Claims::select(DB::raw("COUNT(*) AS count"))
//       ->whereYear('created_at', date('Y'))
//       ->where('status', $decl)
//       ->groupBy(DB::raw('Month(created_at)'))
//       ->pluck('count');
//
//       $declClMonth =  Claims::select(DB::raw("Month(created_at) as month"))
//       ->whereYear('created_at', date('Y'))
//       ->where('status', $decl)
//       ->groupBy(DB::raw('Month(created_at)'))
//       ->pluck('month');
//
//       $declCl = array(0,0,0,0,0,0,0,0,0,0,0,0);
//       foreach($declClMonth as $index => $declClMonths){
//            $declCl[$declClMonths] = $decl_Cl[$index];
//       }


        return view('home')
            ->with('count_beneficiaries',$allBeneficiaries)
           ->with('beneficiaries',$Beneficiaries)
            ->with('claims',$allClaims)
            ->with('recent_claims',$recentClaims)
            ->with('policy_details',$policy_details)
            ->with('all_policies_count',$allPolicies_count)
            ->with('allAgents_count',$allAgents_count)
            ->with('recent_policies',$recentPolicies)
            ->with('countAgentPolicies',$countAgentPolicies)
            ->with('agentPolicies',$gentPolicies)
            ->with('videosCount', $videosCount);
//            ->with('aprPol', $aprPol)
//            ->with('declPol', $declPol)
//            ->with('allCl', $allCl)
//            ->with('aprCl', $aprCl)
//            ->with('declCl', $declCl);

    }
}
