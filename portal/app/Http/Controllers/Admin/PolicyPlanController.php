<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Policy_plan;
use Illuminate\Support\Facades\DB;

class PolicyPlanController extends Controller
{
    //
    public function index()
    {

        $total_standard_plans = DB::table('policy_plans')->select('*')->where('planType', 'Standard Policy')->count();
        $total_societal_plans = DB::table('policy_plans')->select('*')->where('planType', 'Societal Policy')->count();
        $standard_plans = DB::table('policy_plans')->select('*')->where('planType', 'Standard Policy')->paginate(5);
        $societal_plans = DB::table('policy_plans')->select('*')->where('planType', 'Societal Policy')->paginate(5);
       // dd($societal_plans);
        return view('admin/policy_plans', compact('total_societal_plans','total_standard_plans','standard_plans','societal_plans'));
    }

    public function capturePlans(Request $request)
    {

        $Policy_plan = new policy_plan([
            'planType' => $request->planType,
            'maritalPlanStatus'=> $request->maritalPlanStatus,
            'planName' => $request->planName,
            'coverAmount' => $request->coverAmount,
            'ageRange' => $request->ageRange,
            'groupSizeRange' => $request->groupSizeRange,
            'premium' => $request->premium,
            'entryFee' => $request->entryFee,

        ]);
        //dd($Policy_plan);
       $Policy_plan->save();

            return back() ->with('message','Plan Created Successfully');





         // ->with('success','Plan created ');
    }

    public function deletePlan($id){
        $planInfo = DB::table('policy_plans')->select('*')->where('id', $id);
       // dd($planInfo);
        $planInfo->delete();
        return back() ->with('message','Plan Deleted Successfully');
    }

    public function updatePlan(Request $request){
      // dd($request);
        $planInfo = DB::table('policy_plans')
            ->where('id', $request->get('id'))
            ->update(['planType' => $request->get('planType'),
                'maritalPlanStatus' => $request->get('maritalPlanStatus'),
                'planName' => $request->get('planName'),
                'coverAmount' => $request->get('coverAmount'),
                'ageRange' => $request->get('ageRange'),
                'groupSizeRange' => $request->get('groupSizeRange'),
                'entryFee' => $request->get('entryFee'),
                'premium' => $request->get('premium'),
                ]);





        //dd($planInfo);
      //  $planInfo->save();
        return back() ->with('message','Plan Updated Successfully');
    }
}
