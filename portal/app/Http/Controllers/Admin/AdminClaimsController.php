<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminClaimsController extends Controller
{
    public function index()
    {
        $status = 'Approved';
        $status1 = 'Declined';
        $allClaimsCount = DB::table('claims')->selectRaw('policy_no')->count();
        $active_claims = DB::table('claims')->select('*')->where('status', $status)->count();
        $active_claims1 = DB::table('claims')->select('*')->where('status', $status)->paginate(10);
        $dormant_claims = DB::table('claims')->select('*')->where('status', $status1)->count();
        $dormant_claims1 = DB::table('claims')->select('*')->where('status', $status1)->paginate(10);
        $allClaims = DB::table('claims')->get();
        return view('admin/claims', compact('allClaims', 'allClaimsCount', 'active_claims', 'active_claims1', 'dormant_claims', 'dormant_claims1'));
    }

//    public function getAllClaims(){
//      $allClaims = DB::table('claims')->get();
//    }


    public function viewClaim($id){
        $claimInfo = DB::table('claims')->select('*')->where('id', $id)->paginate(10);
       // dd($claimInfo);
        return view('admin/claim_details', compact('claimInfo'));
    }
}
