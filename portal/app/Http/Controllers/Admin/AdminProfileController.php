<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\agent_info;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class AdminProfileController extends Controller
{
    //
    public function index(Request $request){
        $email = Auth::user()->email;
        // dd($email);
        $polNum = DB::table('holder_infos')->where('agent', Auth::user()->name)->count();
        $admin = DB::table('users')->select('*')->where('email', $email)->get();
       //  dd($admin);
        return view('admin/profile', compact('admin','polNum'));
    }

    public function update(Request $request){
        $email = Auth::user()->email;
        $polNum = DB::table('holder_infos')->where('agent', Auth::user()->name)->count();
        $agent = DB::table('agent_infos')->select('*')->where('email', $email)->get();
        return view('admin/edit_profile', compact('agent','polNum'));
    }

    public function updatedRecord(Request $request){
        $email = Auth::user()->email;
            DB::table('users')
                ->where('email', $email)
                ->update(['name' => $request->name]);

                DB::table('agent_infos')
                ->where('email', $email)
                ->update(['name' => $request->name,'dob' => $request->dob,'email' => $request->email, 'mobile' => $request->mobile,'address' => $request->address,'suburb' => $request->suburb,'city' => $request->city,'postalCode' => $request->postalCode,'country' => $request->country]); //'image' => $request->avatarPic->hashName(), 'description' => $request->description
                $polNum = DB::table('holder_infos')->where('agent', Auth::user()->name)->count();
            $agent = DB::table('agent_infos')->select('*')->where('email', $email)->get();
            // DB::table('agent_infos')->select('*')->where()
        return view('admin/profile', compact('agent','polNum'));

    }
    public function imageUpload(Request $request){
        $email = Auth::user()->email;
        if($request->hasFile(('avatarPic'))){
            $avatar = $request->file('avatarPic');
            $request->avatarPic->store('public/images/avatars');
            DB::table('agent_infos')
            ->where('email', $email)
            ->update(['image' => $request->avatarPic->hashName()]);
        $polNum = DB::table('holder_infos')->where('agent', Auth::user()->name)->count();
        $agent = DB::table('agent_infos')->select('*')->where('email', $email)->get();
        return view('admin/profile', compact('agent','polNum'));
        }
    }

    public function summaryUpdate(Request $request){
        // dd($request->description);
        $email = Auth::user()->email;
        DB::table('agent_infos')
            ->where('email', $email)
            ->update(['description' => $request->description]);
        $agent = DB::table('agent_infos')->select('*')->where('email', $email)->get();
        $polNum = DB::table('holder_infos')->where('agent', Auth::user()->name)->count();
        return view('admin/profile', compact('agent','polNum'));
    }

    public function updatePassword(Request $request){
        $email = Auth::user()->email;

        $pword = DB::table('users')->where('email', $email)->select('*')->first() ;

        if(Hash::check($request->oldpass, $pword->password)){
            if($request->newpass == $request->conpass){
                DB::table('users')->where('email', $email)
                ->update(['password'=>$request->newpass->hashName()]);
            }
            else{
                dd("bad");
            }
        }
        else{
            dd("dofo");
        }

        $agent = DB::table('agent_infos')->select('*')->where('email', $email)->get();
        $polNum = DB::table('holder_infos')->where('agent', Auth::user()->name)->count();
        return view('admin/profile', compact('agent','polNum'));

    }


}
