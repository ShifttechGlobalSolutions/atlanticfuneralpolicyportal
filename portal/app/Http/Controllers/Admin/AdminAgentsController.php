<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminAgentsController extends Controller
{
    public function index()
    {
        $active = "Active";
        $inactive = "Pending";
        $agents = DB::table('agent_infos')->selectRaw('*')->paginate(10);
        $allAgents = DB::table('agent_infos')->selectRaw('*')->count();
        $activeAgents = DB::table('agent_infos')->selectRaw('*')->where('status', $active)->count();
        $activeAgents1 = DB::table('agent_infos')->selectRaw('*')->where('status', $active)->paginate(10);
        $inactiveAgents = DB::table('agent_infos')->selectRaw('*')->where('status', $inactive)->paginate(10);
        $inactiveAgents1 = DB::table('agent_infos')->selectRaw('*')->where('status', $inactive)->count();
        $allagents1 = DB::table('agent_infos')->selectRaw('*')->count();
        // dd($allClaims);
        return view('admin/agents',compact(
            'agents',
            'allAgents',
            'activeAgents',
            'inactiveAgents',
            'activeAgents1',
            'inactiveAgents1',
            'allagents1',
        ));
    }

    public function filterByAgent(Request $request){
        $agent = $request->agents;
        $active = "Active";
        $inactive = "Pending";
        $agents = DB::table('agent_infos')->selectRaw('*')->where('name','LIKE', "%{$agent}%")->paginate(10);
        $allAgents = DB::table('agent_infos')->selectRaw('*')->count();
        $activeAgents = DB::table('agent_infos')->selectRaw('*')->where('status', $active)->count();
        $activeAgents1 = DB::table('agent_infos')->selectRaw('*')->where('status', $active)->where('name','LIKE', "%{$agent}%")->paginate(10);
        $inactiveAgents = DB::table('agent_infos')->selectRaw('*')->where('status', $inactive)->where('name','LIKE', "%{$agent}%")->paginate(10);
        $inactiveAgents1 = DB::table('agent_infos')->selectRaw('*')->where('status', $inactive)->count();
        $allagents1 = DB::table('agent_infos')->selectRaw('*')->count();
        // dd($allClaims);
        return view('admin/agents',compact(
            'agents',
            'allAgents',
            'activeAgents',
            'inactiveAgents',
            'activeAgents1',
            'inactiveAgents1',
            'allagents1',
        ));
    }

    public function clrSearch(Request $request){
        $active = "Active";
        $inactive = "Pending";
        $agents = DB::table('agent_infos')->selectRaw('*')->paginate(10);
        $allAgents = DB::table('agent_infos')->selectRaw('*')->count();
        $activeAgents = DB::table('agent_infos')->selectRaw('*')->where('status', $active)->count();
        $activeAgents1 = DB::table('agent_infos')->selectRaw('*')->where('status', $active)->paginate(10);
        $inactiveAgents = DB::table('agent_infos')->selectRaw('*')->where('status', $inactive)->paginate(10);
        $inactiveAgents1 = DB::table('agent_infos')->selectRaw('*')->where('status', $inactive)->count();
        $allagents1 = DB::table('agent_infos')->selectRaw('*')->count();
        // dd($allClaims);
        return view('admin/agents',compact(
            'agents',
            'allAgents',
            'activeAgents',
            'inactiveAgents',
            'activeAgents1',
            'inactiveAgents1',
            'allagents1',
        ));
    }

    public function deleteAgent($id){
        $email = DB::table('agent_infos')->select('email')->where('id', $id)->get();
        foreach ($email as $e) {
            foreach($e as $i){
                // dd($i);
            }
        }

        DB::table('users')->select('*')->where('email', $i)->delete();
        DB::table('agent_infos')->select('*')->where('id', $id)->delete();
        return redirect()->back();
    }

    public function viewAgent($id){
       // dd($id);
        $agentInfo = DB::table('agent_infos')->select('*')->where('id', $id)->get();
        $agentName = DB::table('agent_infos')->select('name')->where('id', $id)->get();
        foreach($agentName as $ag){
            foreach($ag as $a){
            }
        }
        $appl = DB::table('holder_infos')->select('*')->where('agent', $a)->count();

        $currentMonth = date('m');
        $applCur = DB::table("holder_infos")->whereRaw('agent', $a)->whereRaw('MONTH(created_at) = ?',[$currentMonth])->count();

        $cl = DB::table('claims')->select('*')->where('agent', $a)->count();
        $clCur = DB::table("claims")->whereRaw('agent', $a)->whereRaw('MONTH(created_at) = ?',[$currentMonth])->count();

        return view('admin/agent_details', compact('agentInfo', 'appl', 'applCur', 'cl', 'clCur'));
    }
}
