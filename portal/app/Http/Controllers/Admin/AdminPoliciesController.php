<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HolderInfo;
use Illuminate\Support\Facades\DB;

class AdminPoliciesController extends Controller
{
    public function index()
    {
        $status = 'Active';
        $status1 = 'InActive';
        $policies = DB::table('holder_infos')->selectRaw('policy_no')->count();
        $active_policies = DB::table('holder_infos')->select('*')->where('status', $status)->count();
        $active_policies1 = DB::table('holder_infos')->select('*')->where('status', $status)->paginate(10);
        $dormant_policies = DB::table('holder_infos')->select('*')->where('status', $status1)->count();
        $policy_holders= DB::table('holder_infos')->selectRaw('*')->paginate();
        $dormant_policies1 = DB::table('holder_infos')->select('*')->where('status', $status1)->paginate(10);
//        dd($dormant_policies1);
        return view('admin/policies', compact('policies','active_policies', 'dormant_policies', 'policy_holders', 'active_policies1', 'dormant_policies1'));
    }


    public function filterByDate(Request $request){
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $status = 'Active';
        $status1 = 'InActive';
        $policies = DB::table('holder_infos')->selectRaw('policy_no')->count();
        $active_policies = DB::table('holder_infos')->select('*')->where('status', $status)->count();
        $dormant_policies = DB::table('holder_infos')->select('*')->where('status', $status1)->count();
        $active_policies1 = DB::table('holder_infos')->select('*')->where('status', $status)->paginate();
        $dormant_policies1 = DB::table('holder_infos')->select('*')->where('status', $status1)->paginate();
        $policy_holders= DB::table('holder_infos')->selectRaw('*')->whereBetween('created_at', [$startDate, $endDate])->paginate();
        return view('admin/policies', compact('policies','active_policies', 'dormant_policies', 'policy_holders','active_policies1', 'dormant_policies1'));
    }

    public function clrSearch(Request $request){
        $status = 'Active';
        $status1 = 'InActive';
        $policies = DB::table('holder_infos')->selectRaw('policy_no')->count();
        $active_policies = DB::table('holder_infos')->select('*')->where('status', $status)->count();
        $dormant_policies = DB::table('holder_infos')->select('*')->where('status', $status1)->count();
        $policy_holders= DB::table('holder_infos')->selectRaw('*')->paginate();
        $active_policies1 = DB::table('holder_infos')->select('*')->where('status', $status)->paginate();
        $dormant_policies1 = DB::table('holder_infos')->select('*')->where('status', $status1)->paginate();
        return view('admin/policies', compact('policies','active_policies', 'dormant_policies', 'policy_holders','active_policies1', 'dormant_policies1'));
    }

    public function destroy($id){
        $holderInfo = DB::table('holder_infos')->select('*')->where('policy_no', $id);
        $spouseInfo = DB::table('spouse_infos')->select('*')->where('policy_no', $id);
        $beneficiaryInfo = DB::table('beneficiary_infos')->select('*')->where('policy_no', $id);
        $bankInfo = DB::table('bank_account_infos')->select('*')->where('policy_no', $id);
        $dependentInfo = DB::table('dependent_infos')->select('*')->where('policy_no', $id);
        $holderInfo->delete();
        $spouseInfo->delete();
        $beneficiaryInfo->delete();
        $bankInfo->delete();
        $dependentInfo->delete();
        return back();
    }

    public function show($id){
        $member = DB::table('holder_infos')
            ->leftJoin('spouse_infos', 'holder_infos.policy_no', '=', 'spouse_infos.policy_no')
            ->leftJoin('beneficiary_infos', 'holder_infos.policy_no', '=', 'beneficiary_infos.policy_no')
            ->leftJoin('bank_account_infos', 'holder_infos.policy_no', '=', 'bank_account_infos.policy_no')
            ->leftJoin('dependent_infos', 'holder_infos.policy_no', '=', 'dependent_infos.policy_no')
            ->select('*')->where('holder_infos.policy_no', $id)->get();
        return view('admin/show', compact('member'));
    }

    public function show_stepper($id){
        $member = DB::table('holder_infos')
            ->leftJoin('spouse_infos', 'holder_infos.policy_no', '=', 'spouse_infos.policy_no')
            ->leftJoin('beneficiary_infos', 'holder_infos.policy_no', '=', 'beneficiary_infos.policy_no')
            ->leftJoin('bank_account_infos', 'holder_infos.policy_no', '=', 'bank_account_infos.policy_no')
            ->leftJoin('dependent_infos', 'holder_infos.policy_no', '=', 'dependent_infos.policy_no')
            ->select('*')->where('holder_infos.policy_no', $id)->get();
    //    dd($member);



        return view('admin/edit', compact('member'));
    }

    // function downloadFile($file_name){
    //     $file = Storage::disk('public')->get($file_name);

    //     return (new Response($file, 200))
    //           ->header('Content-Type', 'image/jpeg');
    // }

    public function updateHolder(Request $request){
            $holderInfo = DB::table('holder_infos')
                ->where('policy_no', $request->get('policy_no'))
                ->update(['client_firstname' => $request->get('fname')])
                ->update(['client_lastname' => $request->get('lname')])
                ->update(['client_dob' => $request->get('dob1')])
                ->update(['client_id_number' => $request->get('memID')])
                ->update(['strName' => $request->get('strName')])
                ->update(['surburb' => $request->get('surburb')])
                ->update(['postalCode' => $request->get('pcode')])
                ->update(['home' => $request->get('homeContact')])
                ->update(['work' => $request->get('workContact')])
                ->update(['cell' => $request->get('cellContact')])
                ->update(['email' => $request->get('emailContact')])
                ->update(['gender' => $request->get('genderRadio')])
                ->update(['premium' => $request->get('premium')]);
            dd($holderInfo);
            $holderInfo->save();
        }

    public function application()
    {
        return view('admin/policy_stepper');
    }

    public function getAllPolices(){
        //$allpolicies = DB::table('policies')->get();
    }

    public function updatePolicy(Request $request){

    }
}
