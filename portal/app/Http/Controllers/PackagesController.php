<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Policy_plan;
use Illuminate\Support\Facades\DB;

class PackagesController extends Controller
{
    //
    public function cashFunerals()
    {
        return view('policy_products/cash_funerals');
    }

    public function cremations()
    {
        return view('policy_products/cremations');
    }

    public function funeralArrangements()
    {
        return view('policy_products/funeral_arrangements');
    }


    public function funeralPolicies(Request $request)
    {

         $coverAmount= $request->coverAmount ?? "";

        if($coverAmount !=""){
            $single_standard_plans = DB::table('policy_plans')->select('*')
                ->where('planType', 'Standard Policy')
                ->where('maritalPlanStatus', 'Single')
                ->where('coverAmount', $coverAmount)

                ->paginate(6);

        }else{
            $single_standard_plans = DB::table('policy_plans')->select('*')
                ->where('planType', 'Standard Policy')
                ->where('maritalPlanStatus', 'Single')
                ->where('coverAmount', '5000')
                ->paginate(6);
        }

        if($coverAmount !="") {
            $married_standard_plans = DB::table('policy_plans')->select('*')
                ->where('planType', 'Standard Policy')
                ->where('maritalPlanStatus', 'Married')
                ->where('coverAmount', $coverAmount)
                ->paginate(66);
        }else{
            $married_standard_plans = DB::table('policy_plans')->select('*')
                ->where('planType', 'Standard Policy')
                ->where('maritalPlanStatus', 'Married')
                ->where('coverAmount', '5000')
                ->paginate(6);
        }

              return view('policy_products/funeral_policies', compact('single_standard_plans', 'married_standard_plans'));

    }

    public function tombstones()
    {
        return view('policy_products/tombstones');
    }
}
