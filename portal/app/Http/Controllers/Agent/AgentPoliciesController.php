<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\HolderInfo;
use App\Models\SpouseInfo;
use App\Models\BankAccountInfo;
use App\Models\BeneficiaryInfo;
use App\Models\DependentInfo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AgentPoliciesController extends Controller
{
    //
    public function index()
    {
        $agent= Auth()->User()->email;

        $active = "Active";
        $inactive = "Pending";
        $members =  DB::table('holder_infos')->selectRaw('*')->where('agent',$agent)->paginate(10);
        $allPolicies = DB::table('holder_infos')->selectRaw('*')->where('agent',$agent)->count();
        $countActivePolicies = DB::table('holder_infos')->selectRaw('*')->where('status', $active)->where('agent',$agent)->count();
//        dd($allPolicies);
        $activePolicies = DB::table('holder_infos')->selectRaw('*')->where('status', $active)->where('agent',$agent)->paginate(10);
        $inactivePolicies = DB::table('holder_infos')->selectRaw('*')->where('status', $inactive)->where('agent',$agent)->paginate(10);
        $countInactivePolicies = DB::table('holder_infos')->selectRaw('*')->where('status', $inactive)->where('agent',$agent)->count();

        return view('agents/policy', compact('members','allPolicies','countActivePolicies','activePolicies','inactivePolicies','countInactivePolicies'));
    }
    public function showPolicy($policy_no){
       // dd($id);

             $member = DB::table('holder_infos')
                 ->leftJoin('spouse_infos', 'holder_infos.policy_no', '=', 'spouse_infos.policy_no')
                 ->leftJoin('beneficiary_infos', 'holder_infos.policy_no', '=', 'beneficiary_infos.policy_no')
                 ->leftJoin('bank_account_infos', 'holder_infos.policy_no', '=', 'bank_account_infos.policy_no')
                 ->leftJoin('dependent_infos', 'holder_infos.policy_no', '=', 'dependent_infos.policy_no')
                 ->select('*')->where('holder_infos.policy_no', $policy_no)->get();
      // dd($member);
             return view('agents/policy_details', compact('member'));

}

public function createPolicy(){
        return view('agents/create_policy');
}
    public function agentMail(Request $request){
       // dd($request);
        $email = Auth()->User()->email;
        $image = $request->file('idCopy');


        if ($request->hasFile('idCopy')) {
            $count = DB::table('holder_infos')->select('id')->count();
            $num = strval( $count );
            $newNum = 1000 + $num;
            $policy_No = 'AT' . $newNum;

            $request->validate([
                'image' => 'mimes:jpeg,bmp,png,jpg,pdf' // Only allow .jpg, .bmp and .png file types.
            ]);

            // Save the file locally in the storage/public/ folder under a new folder named /productImage
            $request->idCopy->store('/documents/members');

            $holderInfo = new HolderInfo([
                "policy_no"=>$policy_No,
                "client_firstname"=>$request->get('fname'),
                "client_lastname"=>$request->get('lname'),
                "client_dob"=>$request->get('dob1'),
                "client_id_number"=>$request->get('memID'),
                "client_idCopy" => $request->idCopy->hashName(),
                "strName"=>$request->get('strName'),
                "surburb"=>$request->get('surburb'),
                "postalCode"=>$request->get('pcode'),
                "home"=>$request->get('homeContact'),
                "work"=>$request->get('workContact'),
                "cell"=>$request->get('cellContact'),
                "email"=>$request->get('emailContact'),
                "gender"=>$request->get('genderRadio'),
                "premium"=>$request->get('premium'),
                "status"=>'InActive',
                "agent"=>$email,
                "premium"=>$request->get('premium'),
               //"agent"=>$request->get('agent'),
                "package"=>$request->get('package'),
                "ageGroup"=>$request->get('ageGroup'),
                "marital"=>$request->get('marital'),
                "coverAmount"=>$request->get('coverAmount')

            ]);
            //dd($holderInfo);



            //Spouse
            $spouseInfo = new SpouseInfo([
                "policy_no"=>$policy_No,
                "spouse_firstname"=>$request->get('sfname'),
                "spouse_lastname"=>$request->get('slname'),
                "spouse_idNumber"=>$request->get('sId'),
                "spouse_dob"=>$request->get('sdob'),
            ]);


            $request->depIdCody->store('/documents/dependents');
            $dependentInfo = new DependentInfo([
                "policy_no"=>$policy_No,
                "dep_fname"=>$request->get('dep_fname'),
                "dep_lname"=>$request->get('dep_lname'),
                "dep_dob"=>$request->get('dep_dob'),
                "dep_copiesofid" => $request->depIdCody->hashName(),
            ]);



            $beneficiaryInfo = new BeneficiaryInfo([
                "policy_no"=>$policy_No,
                "ben_firstname"=>$request->get('benfname'),
                "ben_lastname"=>$request->get('benlname'),
                "ben_idNumber"=>$request->get('benId'),
                "ben_dob"=>$request->get('bdob'),
                "ben_contact_number"=>$request->get('benconNumber'),
            ]);



            $bankAccountInfo = new BankAccountInfo([
                "policy_no"=>$policy_No,
                "bankName"=>$request->get('bankName'),
                "branchName"=>$request->get('branchName'),
                "accNumber"=>$request->get('accNumber'),
                "branchCode"=>$request->get('branchCode'),
                "accType"=>$request->get('accType'),
            ]);
           // dd($bankAccountInfo);


            // create user account
            //take email,
            $user= User::create([
                'name' => $request->get('fname'),
                'email' => $request->get('emailContact'),
                'password' => Hash::make($request->get('password')),

            ]);

            $user->assignRole(['id'=>2]);

            // dd($user);
            $dependentInfo->save();
            $beneficiaryInfo->save();
            $bankAccountInfo->save();
            $spouseInfo->save();
            $holderInfo->save();
            //$user->save(); // Finally, save the record.
        }
      //  return back()->with('success', 'We have received your application and your account has been created, go ahead and login to track your application thank you.');
        return redirect()->route('agents/policy')
            ->with('success', 'We have received your application and your account has been created, go ahead and login to track your application thank you.');

        //s return redirect()->back()->with('flash_success', 'Your document has been uploaded.');
    }
}
