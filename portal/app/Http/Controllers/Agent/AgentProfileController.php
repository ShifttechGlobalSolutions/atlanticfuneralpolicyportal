<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use App\Models\agent_info;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class AgentProfileController extends Controller
{
    //
    public function index(Request $request){
        $email = Auth()->User()->email;
        $polNum = DB::table('holder_infos')->where('agent', Auth::user()->name)->count();
        $agent = DB::table('agent_infos')->select('*')->where('email', $email)->get();
       // dd($agent);
        return view('agents/profile', compact('agent','polNum'));
    }

    public function update(Request $request){
        $email = Session::get('key');
        $polNum = DB::table('holder_infos')->where('agent', Auth::user()->name)->count();
        $agent = DB::table('agent_infos')->select('*')->where('email', $email)->get();
        return view('agents/edit_profile', compact('agent','polNum'));
    }

    public function updatedRecord(Request $request){

        $email = Session::get('key');
            DB::table('users')
                ->where('email', $email)
                ->update(['name' => $request->name]);

                DB::table('agent_infos')
                ->where('email', $email)
                ->update(['name' => $request->name,'dob' => $request->dob,'email' => $request->email, 'mobile' => $request->mobile,'address' => $request->address,'suburb' => $request->suburb,'city' => $request->city,'postalCode' => $request->postalCode,'country' => $request->country]); //'image' => $request->avatarPic->hashName(), 'description' => $request->description
                $polNum = DB::table('holder_infos')->where('agent', Auth::user()->name)->count();
            $agent = DB::table('agent_infos')->select('*')->where('email', $email)->get();
            // DB::table('agent_infos')->select('*')->where()
        return view('agents/profile', compact('agent','polNum'));

    }
    public function imageUpload(Request $request){
        $email = Session::get('key');
        if($request->hasFile(('avatarPic'))){
            $avatar = $request->file('avatarPic');
            $request->avatarPic->store('public/images/avatars');
            DB::table('agent_infos')
            ->where('email', $email)
            ->update(['image' => $request->avatarPic->hashName()]);
        $polNum = DB::table('holder_infos')->where('agent', Auth::user()->name)->count();
        $agent = DB::table('agent_infos')->select('*')->where('email', $email)->get();
        return view('agents/profile', compact('agent','polNum'));
    }
    }

    public function summaryUpdate(Request $request){
        // dd($request->description);
        $email = Session::get('key');
        DB::table('agent_infos')
            ->where('email', $email)
            ->update(['description' => $request->description]);
        $agent = DB::table('agent_infos')->select('*')->where('email', $email)->get();
        $polNum = DB::table('holder_infos')->where('agent', Auth::user()->name)->count();
        return view('agents/profile', compact('agent','polNum'));
    }

    public function updatePassword(Request $request){
        $email = Session::get('key');

        $pword = DB::table('users')->where('email', $email)->select('*')->first() ;

        if(Hash::check($request->oldpass, $pword->password)){
            if($request->newpass == $request->conpass){
                DB::table('users')->where('email', $email)
                ->update(['password'=>$request->newpass->hashName()]);
            }
            else{
                dd("bad");
            }
        }
        else{
            dd("dofo");
        }

        $agent = DB::table('agent_infos')->select('*')->where('email', $email)->get();
        $polNum = DB::table('holder_infos')->where('agent', Auth::user()->name)->count();
        return view('agents/profile', compact('agent','polNum'));

    }


}
