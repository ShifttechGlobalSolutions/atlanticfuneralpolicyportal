<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use App\Models\VideoTutorial;
use Illuminate\Http\Request;
use function back;
use Illuminate\Support\Facades\DB;

class VideoTutsController extends Controller
{
    public function store(Request $request){
        // ensure the request has a file before we attempt anything else.
        if ($request->hasFile('file')) {

            $request->validate([
                'video' => 'mimes:wmv,mpeg,mpv,ivr,flv,video,mpeg4,movie.mpg,mjpg,smv,m4v,mov,avi' // Only allow .jpg, .bmp and .png file types.
            ]);

            // Save the file locally in the storage/public/ folder under a new folder named /productImage
            $request->file->store('public/videos');

            $videoTuts = new VideoTutorial([
                "title"=>$request->get('vidTitle'),
                "description"=>$request->get('vidDescript'),
                "file_path" => $request->file->hashName(),
            ]);
            // dd($catalogue);
            $videoTuts->save(); // Finally, save the record.
        }
        return back()->with('success', 'Video uploaded successfully');
    }

    //    demo1
    public function tuts()
    {
        $Items = DB::table('video_tutorials')->selectRaw('*')->paginate(10);
        return view('agents.tutorials', ['Items'=>$Items]);
    }

    public function tutSearch(Request $request){
        $item = $request->get('searchItem');
        $Items = DB::table('video_tutorials')->whereRaw('title like ?',['%'.$item.'%'])->selectRaw('*')->paginate(2);
        return view('agents.tutorials', ['Items'=>$Items]);
    }

    public function clrSearch(Request $request){
        $Items = DB::table('video_tutorials')->selectRaw('*')->paginate(2);
        return view('agents.tutorials', ['Items'=>$Items]);
    }
//    endDemo1
}
