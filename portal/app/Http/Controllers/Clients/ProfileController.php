<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use App\Models\HolderInfo;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    //
    public function index()
    {
        $policy_no=  HolderInfo::where('email', Auth()->User()->email)->value('policy_no');
        $policy_details=  HolderInfo::where('policy_no', $policy_no)->get();
        //dd($policy_details);
        return view('clients/profile') ->with('policy_details',$policy_details);
    }
}
