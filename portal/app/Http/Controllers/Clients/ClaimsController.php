<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use App\Models\Claims;

use App\Models\HolderInfo;
use Illuminate\Http\Request;

class ClaimsController extends Controller
{
    //
    public function index()
    {
        $claims = Claims::latest()->paginate(10);

        //dd($claims);
        return view('clients/claims',compact('claims'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function getClaimForm(){
        $policy_no=  HolderInfo::where('email', Auth()->User()->email)->value('policy_no');
        $policy_details=  HolderInfo::where('policy_no', $policy_no)->get();
    dd($policy_no);
        return view('clients/claimForm')->with('policy_details', $policy_details);
    }

    public function getClaimDetails($id){
        $policy_no=  HolderInfo::where('email', Auth()->User()->email)->value('policy_no');
        //$policy_details=  HolderInfo::where('policy_no', $policy_no)->get();
        $claim_details=  Claims::where('policyHolderId', $policy_no)->where('id',$id)->get();
         //dd($claim_details);
        return view('clients/claim_details')->with('claim_details', $claim_details);
    }

    public function captureClaims(Request $request){

        $request->validate([
            'deceasedIdCopy' => 'required|mimes:pdf,xlx,png,csv|max:2048',
            'claimantIdCopy' => 'required|mimes:pdf,xlx,png,csv|max:2048',
            'bankStatementCopy' => 'required|mimes:pdf,xlx,png,csv|max:2048',
            'policeReportCopy' => 'required|mimes:pdf,xlx,png,csv|max:2048',
            'certifiedBICopy' => 'required|mimes:pdf,xlx,png,csv|max:2048',
        ]);
        $deceasedIdCopy = $request->file('deceasedIdCopy')->getClientOriginalName();
        $claimantIdCopy = $request->file('claimantIdCopy')->getClientOriginalName();
        $bankStatementCopy = $request->file('bankStatementCopy')->getClientOriginalName();
        $policeReportCopy = $request->file('policeReportCopy')->getClientOriginalName();
        $certifiedBICopy = $request->file('certifiedBICopy')->getClientOriginalName();

        $status ="Pending";

        if ($request->hasFile('deceasedIdCopy','claimantIdCopy','bankStatementCopy','policeReportCopy','certifiedBICopy')!=null) {


            $deceasedIdCopyPath = $request->file('deceasedIdCopy')->store('public/documents/claims');
            $claimantIdCopyPath = $request->file('claimantIdCopy')->store('public/documents/claims');
            $bankStatementCopyPath = $request->file('bankStatementCopy')->store('public/documents/claims');
            $policeReportCopyPath = $request->file('policeReportCopy')->store('public/documents/claims');
            $certifiedBICopyPath = $request->file('certifiedBICopy')->store('public/documents/claims');



            $claimData = new Claims([
                "policyHolderId" => $request->get('policyHolderId'),
                "policyHolderSurname" => $request->get('policyHolderSurname'),
                "policyHolderFullName" => $request->get('policyHolderFullName'),
                "policyHolderDateOfBirth" => $request->get('policyHolderDateOfBirth'),
                "policyHolderEmail" => $request->get('policyHolderEmail'),
                "policyHolderContactNumber" => $request->get('policyHolderContactNumber'),
                "policyHolderIdNumber" => $request->get('policyHolderIdNumber'),
                "policyHolderPostalAddress" => $request->get('policyHolderPostalAddress'),
                "policyHolderResidentialAddress" => $request->get('policyHolderResidentialAddress'),
                //deceasedDetails
                "deceasedSurname" => $request->get('deceasedSurname'),
                "deceasedFullName" => $request->get('deceasedFullName'),
                "deceasedContactNumber" => $request->get('deceasedContactNumber'),
                "deceasedDateOfBirth" => $request->get('deceasedDateOfBirth'),
                "deceasedLastAddress" => $request->get('deceasedLastAddress'),
                "deceasedIdCopy" => $deceasedIdCopy,
                "deceasedIdCopyPath" => $deceasedIdCopyPath,
                "certifiedBICopy" => $certifiedBICopy,
                "certifiedBICopyPath" => $certifiedBICopyPath,
                "deceasedCauseOfDeath" => $request->get('deceasedCauseOfDeath'),
                "deceasedResidentialAddress" => $request->get('deceasedResidentialAddress'),
                "deceasedBiSerialNo" => $request->get('deceasedBiSerialNo'),
                "deceasedDeathCertificateSerialNo" => $request->get('deceasedDeathCertificateSerialNo'),
                //claimantDetails
                "claimantSurname" => $request->get('claimantSurname'),
                "claimantFullName" => $request->get('claimantFullName'),
                "claimantContactNumber" => $request->get('claimantContactNumber'),
                "claimantEmail" => $request->get('claimantEmail'),
                "claimantIdNumber" => $request->get('claimantIdNumber'),
                "claimantRelationship" => $request->get('claimantRelationship'),
                "claimantPostalAddress" => $request->get('claimantPostalAddress'),
                "claimantResidentialAddress" => $request->get('claimantResidentialAddress'),
                "claimantIdCopy" => $claimantIdCopy,
                "claimantIdCopyPath" => $claimantIdCopyPath,
                "policeReportCopy" => $policeReportCopy,
                "policeReportCopyPath" => $policeReportCopyPath,
                //benefit payment options
                "funeralParlourName" => $request->get('funeralParlourName'),
                "funeralParlourPhone" => $request->get('funeralParlourPhone'),
                //nominated beneficiary details
                "beneficiaryName" => $request->get('beneficiaryName'),
                "beneficiaryIdNumber" => $request->get('beneficiaryIdNumber'),
                "beneficiaryBankName" => $request->get('beneficiaryBankName'),
                "beneficiaryBranch" => $request->get('beneficiaryBranch'),
                "beneficiaryAccountNumber" => $request->get('beneficiaryAccountNumber'),
                "beneficiaryBranchCode" => $request->get('beneficiaryBranchCode'),
                "beneficiaryAccountType" => $request->get('beneficiaryAccountType'),
                "bankStatementCopy" => $bankStatementCopy,
                "bankStatementCopyPath" => $bankStatementCopyPath,


                "status" => $status,
                //  "agent"=>$agent,

            ]);

            $claimData->save();
        }
        return redirect()->route('clients/claims')
            ->with('success','Claim successfully submitted. We will get back to you soon. Thank you');

    }
}
