<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;

use App\Models\BeneficiaryInfo;
use App\Models\HolderInfo;
use App\Models\SpouseInfo;
use App\Models\Policy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PoliciesController extends Controller
{
    //
    public function index()
    {
        $ben_counter = 1;
        $spouse_counter = 1;
        $policy_no=  HolderInfo::where('email', Auth()->User()->email)->value('policy_no');
        // $policy_no = "AT1000";
        // dd($policy_no);
        // $policy_details=  HolderInfo::where('policy_no', $policy_no)->get();
        $policy_details= DB::table('holder_infos')->select("*")->where("policy_no",$policy_no)->get();
    //  dd($policy_details);
        $user= Auth::user()->email;

        $beneficiary = BeneficiaryInfo::latest()->where('policy_no',$policy_no)->paginate(10);
        $spouse = SpouseInfo::latest()->where('policy_no',$policy_no)->paginate(10);

        return view('clients/policy')
            ->with('policy_details',$policy_details)
            ->with('beneficiaries',$beneficiary)
            ->with('spouses',$spouse)
            ->with('ben_counter', $ben_counter)
            ->with('spouse_counter', $spouse_counter)
            ->with('policy_no', $policy_no) ;
    }

    public function new_benefiary(Request $request){
        $beneficiaryInfo = new BeneficiaryInfo([
            "policy_no"=>$request->get('policy_no'),
            "ben_firstname"=>$request->get('ben_firstname'),
            "ben_lastname"=>$request->get('ben_lastname'),
            "ben_idNumber"=>$request->get('ben_id_number'),
            "ben_dob"=>$request->get('ben_dob'),
            "ben_contact_number"=>$request->get('ben_mobile'),
        ]);

        $beneficiaryInfo->save();

        return back();
    }

    public function new_spouse(Request $request){
        $spouseInfo = new SpouseInfo([
            "policy_no"=>$request->get('policy_no'),
            "spouse_firstname"=>$request->get('spouse_firstname'),
            "spouse_lastname"=>$request->get('spouse_lastname'),
            "spouse_idNumber"=>$request->get('spouse_id_number'),
            "spouse_dob"=>$request->get('spouse_dob'),
        ]);
        $spouseInfo->save();
        return back();
    }

    public function beneficiary_details($id){
        $beneficiary = DB::table('beneficiary_infos')->select('*')->where('id',$id)->get();
        return view('clients/beneficiary_details', compact('beneficiary'));
    }

    public function beneficiary_update(Request $request){
        DB::table('beneficiary_infos')->where('id',$request->get('id'))->update([
            "policy_no"=>$request->get('policy_no'),
            "ben_firstname"=>$request->get('ben_firstname'),
            "ben_lastname"=>$request->get('ben_lastname'),
            "ben_idNumber"=>$request->get('ben_id_number'),
            "ben_dob"=>$request->get('ben_dob'),
            "ben_contact_number"=>$request->get('ben_contact_number'),
            ]);

        return back();
    }

    public function delete_beneficiary($id){
        DB::table('beneficiary_infos')->select('*')->where('id',$id)->delete();
        return back();
    }

    public function spouse_details($id){
        $spouse = DB::table('spouse_infos')->select('*')->where('id',$id)->get();
        return view('clients/spouse_details', compact('spouse'));
    }

    public function spouse_update(Request $request){
        DB::table('spouse_infos')->where('id',$request->get('id'))->update([
            "policy_no"=>$request->get('policy_no'),
            "spouse_firstname"=>$request->get('spouse_firstname'),
            "spouse_lastname"=>$request->get('spouse_lastname'),
            "spouse_idNumber"=>$request->get('spouse_id_number'),
            "spouse_dob"=>$request->get('spouse_dob'),
            ]);

        return back();
    }

    public function delete_spouse($id){
        DB::table('spouse_infos')->select('*')->where('id',$id)->delete();
        return back();
    }

    public function holder_update(Request $request){
        DB::table('holder_infos')->where('id',$request->get('id'))->update([
            "client_firstname"=>$request->get('client_firstname'),
            "client_lastname"=>$request->get('client_lastname'),
            "client_dob"=>$request->get('client_dob'),
            "email"=>$request->get('email'),
            "cell"=>$request->get('cell'),
            "strName"=>$request->get('strName'),
            "surburb"=>$request->get('surburb'),
            "postalCode"=>$request->get('postalCode'),
            ]);

        return back();
    }

}
