<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\HolderInfo;
use App\Models\SpouseInfo;
use App\Models\BeneficiaryInfo;
use App\Models\BankAccountInfo;
use App\Models\DependentInfo;
use App\Models\plan_cover;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class ApplicationController extends Controller
{
    //
    public function index()
    {
        return view('application.applications');
    }

    public function application()
    {
        return view('application.stepper_application');
    }

    public function adminMail(Request $request){

        $image = $request->file('idCopy');
        $data = [
            'memberFirstName'=>$request->fname,
            'memberLastName'=>$request->lname,
            'memberDOB'=>$request->dob1,
            'memberIDNum'=>$request->memID,
            'idCopy'=>$image,
            'memberStrName'=>$request->strName,
            'memberSurburb'=>$request->surburb,
            'memberPcode'=>$request->pcode,
            'memberHomeContact'=>$request->homeContact,
            'memberWorkContact'=>$request->workContact,
            'memberCellContact'=>$request->cellContact,
            'memberEmailContact'=>$request->emailContact,
            'memberGenderRadio'=>$request->genderRadio,
            'spouseFname'=>$request->sfname,
            'spouseLname'=>$request->slname,
            'spouseId'=>$request->sId,
            'spouseDOB'=>$request->sdob,
            'dep_fname'=>$request->dep_fname,
            'dep_lname'=>$request->dep_lname,
            'dep_dob'=>$request->dep_dob,
            'depCopiesofid'=>$request->copiesofid,
            'benFname'=>$request->benfname,
            'benLname'=>$request->benlname,
            'benConNumber'=>$request->benconNumber,
            'bankName'=>$request->bankName,
            'branchName'=>$request->branchName,
            'accNumber'=>$request->accNumber,
            'branchCode'=>$request->branchCode,
            'accType'=>$request->accType,
        ];


        if ($request->hasFile('idCopy')) {
            $count = DB::table('holder_infos')->select('id')->count();
            $num = strval( $count );
            $newNum = 1000 + $num;
            $policy_No = 'AT' . $newNum;

            //pay@_nos
            $incrementing_payAt_no = 7000000000 + $num;
            $payAt_no = '1191' . $incrementing_payAt_no;
            // dd($payAt_no);

            $request->validate([
                'image' => 'mimes:pdf' // Only allow .pdf file types.
            ]);

            // Save the file locally in the storage/public/ folder under a new folder named /productImage
            $request->idCopy->store('public/documents/members');

            $holderInfo = new HolderInfo([
                "policy_no"=>$policy_No,
                "client_firstname"=>$request->get('fname'),
                "client_lastname"=>$request->get('lname'),
                "client_dob"=>$request->get('dob1'),
                "client_id_number"=>$request->get('memID'),
                "client_idCopy" => $request->idCopy->hashName(),
                "strName"=>$request->get('strName'),
                "surburb"=>$request->get('surburb'),
                "postalCode"=>$request->get('pcode'),
                "home"=>$request->get('homeContact'),
                "work"=>$request->get('workContact'),
                "cell"=>$request->get('cellContact'),
                "email"=>$request->get('emailContact'),
                "gender"=>$request->get('genderRadio'),
                "premium"=>$request->get('premium'),
                "status"=>'InActive',
                "premium"=>$request->get('premium'),
                "agent"=>$request->get('agent'),
                "package"=>$request->get('package'),
                "ageGroup"=>$request->get('ageGroup'),
                "marital"=>$request->get('marital'),
                "coverAmount"=>$request->get('coverAmount'),
                "payAt_no"=>$payAt_no

            ]);

            //Spouse
            $spouseInfo = new SpouseInfo([
                "policy_no"=>$policy_No,
                "spouse_firstname"=>$request->get('sfname'),
                "spouse_lastname"=>$request->get('slname'),
                "spouse_idNumber"=>$request->get('sId'),
                "spouse_dob"=>$request->get('sdob'),
            ]);

            $request->depIdCody->store('public/documents/dependents');
            $dependentInfo = new DependentInfo([
                "policy_no"=>$policy_No,
                "dep_fname"=>$request->get('dep_fname'),
                "dep_lname"=>$request->get('dep_lname'),
                "dep_dob"=>$request->get('dep_dob'),
                "dep_copiesofid" => $request->depIdCody->hashName(),
            ]);

            $beneficiaryInfo = new BeneficiaryInfo([
                "policy_no"=>$policy_No,
                "ben_firstname"=>$request->get('benfname'),
                "ben_lastname"=>$request->get('benlname'),
                "ben_idNumber"=>$request->get('benId'),
                "ben_dob"=>$request->get('bdob'),
                "ben_contact_number"=>$request->get('benconNumber'),
            ]);

            $bankAccountInfo = new BankAccountInfo([
                "policy_no"=>$policy_No,
                "bankName"=>$request->get('bankName'),
                "branchName"=>$request->get('branchName'),
                "accNumber"=>$request->get('accNumber'),
                "branchCode"=>$request->get('branchCode'),
                "accType"=>$request->get('accType'),
            ]);

            $cover_plan = new plan_cover([
                "policy_no"=>$policy_No,
                "packageName"=>$request->get('package'),
                "description"=>$request->get('package'),
                "coverAmount"=>$request->get('coverAmount'),
                "memberStatus"=>$request->get('marital'),
                "memberAge"=>$request->get('ageGroup'),
                "premium"=>$request->get('premium1'),
                "entryFee"=>$request->get('entryFee')
            ]);

            $user = User::where('email', '=', $request->get('emailContact'))->first();
            if ($user === null) {
                // User does not exist
                $user= User::create([
                    'name' => $request->get('fname'),
                    'email' => $request->get('emailContact'),
                    'password' => Hash::make($request->get('password')),

                ]);
                $user->assignRole(['id'=>2]);
                $dependentInfo->save();
                $beneficiaryInfo->save();
                $bankAccountInfo->save();
                $spouseInfo->save();
                $holderInfo->save();
                $cover_plan->save();
                //send mail
                $to_Client = $data['memberEmailContact'];
                Mail::to($to_Client)->send(new \App\Mail\clientMail($data));
                return back()->with('success', 'Application sent successfully, Verify your email.');
            } else {
                // User exits
                return back()->with('error', 'Failed! Email is taken');
            }



        }


    }

}
