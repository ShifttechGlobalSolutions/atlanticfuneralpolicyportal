<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{

    public function index()
    {
        return view('contact');
    }

    public function calls(Request $request){
        $data = [
            'name'=>$request->name,
            'phoneNumber'=>$request->phoneNumber,
           // 'purpose'=>$request->purpose,

            ];
        //dd($data);
        $to_email = "info@atlantic-funerals.co.za";

        Mail::to($to_email)->send(new \App\Mail\callBackMail($data));
        return back() ->with('message','Call back sent Successfully');
//            ->with('success','Call back sent ');
    }

    public function sendMessage(Request $request){
        $data = [
            'name'=>$request->name,
            'phoneNumber'=>$request->phoneNumber,
            'email'=>$request->email,
            'message'=>$request->message,


        ];
        //dd($data);
        $to_email = "info@atlantic-funerals.co.za";
        Mail::to($to_email)->send(new \App\Mail\contactMail($data));
        return back() ->with('message','Message sent Successfully');
//            ->with('success','Call back sent ');
    }

}
