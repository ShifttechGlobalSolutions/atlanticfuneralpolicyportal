<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\video;
use Illuminate\Support\Facades\DB;

class VideoTutsController extends Controller
{
    //
    public function index(){
        $video = DB::table('videos')->select('*')->paginate(3);
//        dd($video);
        return view('agents/video', compact('video'));
    }

    public function addVid(){
        return view('admin/addVideo');
    }

    public function videoTuts(){
        $video = DB::table('videos')->select('*')->paginate(5);
//        dd($video);
        return view('admin/video', compact('video'));
    }

    public function deleteVideo($id){
        DB::table('videos')->select('*')->where('id', $id)->delete();
        return redirect()->back();
    }

    public function showVideo($id){
        $video = DB::table('videos')->select('*')->where('id', $id)->get();
        return view('admin/showVidDetails', compact('video'));
    }

    public function update(Request $request)
    {
        $video = $request->file('file')->hashName();
        if ($request->hasFile('file')) {
            // $request->validate([
            //     'file' => 'mimes:mp4,vlc,wmv'
            // ]);
            $request->file->store('public/video');
            DB::table('videos')
                ->where('id', $request->id)
                ->update(['title' => $request->title, 'video' => $request->file->hashName(), 'description' => $request->description,]);

            return back();
        }
    }

    public function tutSearch(Request $request){
        $item = $request->get('searchItem');
        // dd($item);
        $video = DB::table('videos')->whereRaw('title like ?',['%'.$item.'%'])->selectRaw('*')->paginate(5);
        return view('admin/video', compact('video'));
    }

    public function clearSearch(){
        $video = DB::table('videos')->selectRaw('*')->paginate(5);
        return view('admin/video', compact('video'));
    }

    public function store(Request $request)
    {
        // dd($request->file);
        $video = $request->file('file')->hashName();
        if ($request->hasFile('file')) {
            // $request->validate([
            //     'file' => 'mimes:mp4,vlc,wmv'
            // ]);
            $request->file->store('public/video');
            $Video = new video([
                "title"=>$request->title,
                'video'=>$request->file->hashName(),
                'description' =>$request->description
                ]);
            $Video->save();
        }
        return back();
    }

}
