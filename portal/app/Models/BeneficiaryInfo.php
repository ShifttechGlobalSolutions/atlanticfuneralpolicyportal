<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BeneficiaryInfo extends Model
{
    use HasFactory;

    protected $fillable = [
        "policy_no",
        'ben_firstname',
        'ben_lastname',
        'ben_idNumber',
        'ben_dob',
        'ben_contact_number',
    ];
}
