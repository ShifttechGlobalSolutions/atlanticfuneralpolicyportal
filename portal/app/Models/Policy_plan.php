<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Policy_plan extends Model
{
    use HasFactory;
    protected $fillable = [

        'planType', //standard or societal
        'maritalPlanStatus', //single or married
        //societal
        'planName',//Plan A,B,C
        'coverAmount',
        'ageRange',
        'groupSizeRange',
        'premium',
        'entryFee',
    ];
}
