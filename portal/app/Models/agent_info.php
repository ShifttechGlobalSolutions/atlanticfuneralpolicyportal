<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class agent_info extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'dob',
        'title',
        'email',
        'mobile',
        'address',
        'suburb',
        'city',
        'postalCode',
        'country',
        'image',
        'description'

    ];

}
