<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HolderInfo extends Model
{
    use HasFactory;

    protected $fillable = [
        'policy_no',
        'client_firstname',
        'client_lastname',
        'status',
        'client_dob',
        'client_id_number',
        'client_idCopy',
        'strName',
        'surburb',
        'postalCode',
        'home',
        'work',
        'cell',
        'email',
        'gender',
        'status',
        'package',
        'ageGroup',
        'marital',
        'coverAmount',
        'agent',
        'payAt_no'
        ];
}
