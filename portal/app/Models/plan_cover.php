<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class plan_cover extends Model
{
    use HasFactory;

    protected $fillable = [
        'policy_no',
        'packageName',
        'description',
        'coverAmount',
        'memberStatus',
        'memberAge',
        'premium',
        'entryFee',
        ];
}
