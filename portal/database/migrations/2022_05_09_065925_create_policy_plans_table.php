<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePolicyPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policy_plans', function (Blueprint $table) {

            $table->id();
            $table->string('planType');
            $table->string('maritalPlanStatus');
            $table->string('planName')->nullable();
            $table->decimal('coverAmount',65,2);
            $table->string('ageRange');
            $table->string('groupSizeRange')->nullable();
            $table->decimal('premium',65,2);
            $table->decimal('entryFee',65,2);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('policy_plans');
    }
}
