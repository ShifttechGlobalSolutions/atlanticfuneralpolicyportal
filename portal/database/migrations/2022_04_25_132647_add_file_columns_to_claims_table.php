<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFileColumnsToClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claims', function (Blueprint $table) {
            //
            $table->string('deceasedIdCopy');
            $table->string('claimantIdCopy');
            $table->string('bankStatementCopy');
            $table->string('policeReportCopy');
            $table->string('certifiedBICopy');
            $table->string('deceasedIdCopyPath');
            $table->string('claimantIdCopyPath');
            $table->string('bankStatementCopyPath');
            $table->string('policeReportCopyPath');
            $table->string('certifiedBICopyPath');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claims', function (Blueprint $table) {
            //
        });
    }
}
