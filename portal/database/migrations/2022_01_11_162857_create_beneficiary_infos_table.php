<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeneficiaryInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beneficiary_infos', function (Blueprint $table) {
            $table->id();
            $table->string('policy_no');
            $table->string('ben_firstname');
            $table->string('ben_lastname');
            $table->string('ben_idNumber');
            $table->date('ben_dob');
            $table->string('ben_contact_number');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beneficiary_infos');
    }
}
