<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanCoversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_covers', function (Blueprint $table) {
            $table->id();
            $table->string('policy_no');
            $table->string('packageName');
            $table->string('description');
            $table->decimal('coverAmount',65,2);
            $table->string('memberStatus');
            $table->string('memberAge');
            $table->decimal('premium',65,2);
            $table->decimal('entryFee',65,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_covers');
    }
}
