<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDependentInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dependent_infos', function (Blueprint $table) {
            $table->id();
            $table->string('policy_no');
            $table->string('dep_fname');
            $table->string('dep_lname');
            $table->datetime('dep_dob');
            $table->text('dep_copiesofid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dependent_infos');
    }
}
