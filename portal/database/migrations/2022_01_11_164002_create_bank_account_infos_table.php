<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankAccountInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_account_infos', function (Blueprint $table) {
            $table->id();
            $table->string('policy_no');
            $table->string('bankName');
            $table->string('branchName');
            $table->string('accNumber');
            $table->string('branchCode');
            $table->string('accType');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_account_infos');
    }
}
